package org.firstinspires.ftc.teamcode.tests;

public class Distance {
    private double distance;
    private double position;

    public Distance (double dis, double pos) {
        distance = dis;
        position = pos;
    }

    public double getDistance() {
        return distance;
    }

    public double getPosition() {
        return position;
    }
}
