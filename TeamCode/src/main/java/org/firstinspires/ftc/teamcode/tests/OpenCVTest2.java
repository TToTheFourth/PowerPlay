package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;
import org.firstinspires.ftc.teamcode.RepBot;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion3;
import org.opencv.imgproc.Imgproc;

@Disabled
@Autonomous
public class OpenCVTest2 extends LinearOpMode {
    int position;

    @Override
    public void runOpMode() {
        OpenCVVersion3 cam = new OpenCVVersion3(this, true, false);
        ForWhatWeHaveNow rep = new ForWhatWeHaveNow(this);

        rep.startGyro();
        waitForStart();

        while (opModeIsActive()) {
           // position = cam.iconPos();

            telemetry.addData("Icon Position Number:", position);
            telemetry.update();
        }


//        if (position < 0) {
//            rep.turnForever('l');
//        } else if (position > 0) {
//            rep.turnForever('r');
//        } else {
//            rep.stopMotor();
//        }
    }
}
