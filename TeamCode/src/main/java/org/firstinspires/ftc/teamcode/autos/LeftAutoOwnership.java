package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.TurtleBotCoreV3;
import org.firstinspires.ftc.teamcode.roadrunner.drive.DriveConstants;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;


@Autonomous
public class LeftAutoOwnership extends LinearOpMode {

    enum Phase {
        SCAN,
        TO_POLE,
        DRIVE1,
        DRIVE2,
        SCORE,
        TO_STACK,
        INTAKE,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int cycles = 3;
    double SLOW_MAX_ACCEL = 9;
    double SLOW_MAX_VELOCITY = 18;

    double SLOWER_MAX_ACCEL = 7;
    double SLOWER_MAX_VELOCITY = 9;

    double SLOWEST_MAX_ACCEL = 3;
    double SLOWEST_MAX_VELOCITY = 5;
    ElapsedTime timer = new ElapsedTime();


    Pose2d startPose = new Pose2d(-36, -65, Math.toRadians(-90));



    @Override
    public void runOpMode() throws InterruptedException {



        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCoreV3 bot = new TurtleBotCoreV3(this);
        AprilTagsVersion1 tag = new AprilTagsVersion1(this);


        drive.setPoseEstimate(startPose);

        Trajectory toPoleCloseLowPole1 = drive.trajectoryBuilder(startPose,true)
                .lineToLinearHeading(new Pose2d(-35,-53,Math.toRadians(180)))
                .addTemporalMarker(0.01, () -> bot.extendArmAndSlide(1))
                .addTemporalMarker(0.01, () -> bot.setStackAugmentFive())
                .build();

        Trajectory toPoleCloseLowPole2 = drive.trajectoryBuilder(toPoleCloseLowPole1.end(),true)
                .lineToLinearHeading(new Pose2d(-35,-47,Math.toRadians(180)))
                .build();

        Trajectory toStackCloseLow1 = drive.trajectoryBuilder(toPoleCloseLowPole2.end(),true)
                .lineToLinearHeading(new Pose2d(-36,-16,Math.toRadians(180)))
                .addTemporalMarker(0.4, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.4, () -> bot.setDunk(0))
                .build();

        Trajectory toStackCloseLow2 = drive.trajectoryBuilder(toStackCloseLow1.end(),false)
                .lineToLinearHeading(new Pose2d(-61.5,-16,Math.toRadians(180)))
                .build();

        Trajectory toPoleFarLowPole = drive.trajectoryBuilder(toStackCloseLow2.end(),true)
                .lineToLinearHeading(new Pose2d(-58,-16,Math.toRadians(145)),SampleMecanumDrive.getVelocityConstraint(SLOWEST_MAX_VELOCITY,SLOWEST_MAX_ACCEL,DriveConstants.TRACK_WIDTH), SampleMecanumDrive.getAccelerationConstraint(SLOWEST_MAX_ACCEL))
                .addTemporalMarker(0.1, () -> bot.extendArmAndSlide(1))

                .build();

        Trajectory toStackFarLow = drive.trajectoryBuilder(toPoleFarLowPole.end(),false)
                .lineToLinearHeading(new Pose2d(-61.5,-16,Math.toRadians(180)))
                .addTemporalMarker(0.8, () -> bot.setDunk(0))
                .build();

        Trajectory toPoleMidPole = drive.trajectoryBuilder(toStackFarLow.end(),true)
                .splineToSplineHeading(new Pose2d(-47,-16,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-34,-23.5,Math.toRadians(145)),  Math.toRadians(-30))
                .addTemporalMarker(1, () -> bot.extendArmAndSlide(2))

                .build();

        Trajectory toStackMidPole = drive.trajectoryBuilder(toPoleMidPole.end(),false)
                .splineToSplineHeading(new Pose2d(-48,-16,Math.toRadians(180)),Math.toRadians(180),SampleMecanumDrive.getVelocityConstraint(SLOW_MAX_VELOCITY,SLOW_MAX_ACCEL,DriveConstants.TRACK_WIDTH), SampleMecanumDrive.getAccelerationConstraint(SLOW_MAX_ACCEL))
                .splineToSplineHeading(new Pose2d(-61.5,-16,Math.toRadians(180)),Math.toRadians(180),SampleMecanumDrive.getVelocityConstraint(SLOW_MAX_VELOCITY,SLOW_MAX_ACCEL,DriveConstants.TRACK_WIDTH), SampleMecanumDrive.getAccelerationConstraint(SLOW_MAX_ACCEL))
                .addTemporalMarker(0.8, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.8, () -> bot.setDunk(0))
                .build();

        Trajectory toPoleTeamHighPole = drive.trajectoryBuilder(toStackCloseLow2.end(),true)
                .splineToSplineHeading(new Pose2d(-26,-16,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-10,-23.5,Math.toRadians(145)),  Math.toRadians(-30))
                .addTemporalMarker(1, () -> bot.extendArmAndSlide(3))

                .build();

        Trajectory toStackTeamHigh = drive.trajectoryBuilder(toPoleTeamHighPole.end(),false)
                .splineToSplineHeading(new Pose2d(-48,-16,Math.toRadians(180)),Math.toRadians(180),SampleMecanumDrive.getVelocityConstraint(SLOW_MAX_VELOCITY,SLOW_MAX_ACCEL,DriveConstants.TRACK_WIDTH), SampleMecanumDrive.getAccelerationConstraint(SLOW_MAX_ACCEL))
                .splineToSplineHeading(new Pose2d(-61.5,-16,Math.toRadians(180)),Math.toRadians(180),SampleMecanumDrive.getVelocityConstraint(SLOW_MAX_VELOCITY,SLOW_MAX_ACCEL,DriveConstants.TRACK_WIDTH), SampleMecanumDrive.getAccelerationConstraint(SLOW_MAX_ACCEL))
                .addTemporalMarker(0.8, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.8, () -> bot.setDunk(0))
                .build();

        Trajectory park1 = drive.trajectoryBuilder(toPoleMidPole.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-16,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-60,-16,Math.toRadians(180)),Math.toRadians(180))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.5, () -> bot.setDunk(0))

                .build();

        Trajectory park2 = drive.trajectoryBuilder(toPoleMidPole.end(),false)
                .lineToLinearHeading(new Pose2d(-36, -16, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.5, () -> bot.setDunk(0))
                .build();

        Trajectory park3 = drive.trajectoryBuilder(toPoleTeamHighPole.end(),false)
                .lineToLinearHeading(new Pose2d(-12, -18, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.5, () -> bot.setDunk(0))
                .build();



       // int preSensed = bot.scanAprilTagTime(tag, 1);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        bot.closeClaw();

        waitForStart();

       // int sensed = bot.scanAprilTagTime(tag, 1);

        int sensed = Constants.APRILTAG_ID_THREE;

        bot.closeClaw();


        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMechAutomated(1,1);




            switch (currentPhase) {

                case SCAN:

                    telemetry.addData("signal state", sensed);
                    telemetry.update();

                    currentPhase = Phase.DRIVE1;
                    drive.followTrajectoryAsync(toPoleCloseLowPole1);

                    break;


                case TO_POLE:

                    if (!drive.isBusy() && !bot.mechIsBusy()) {
                        currentPhase = Phase.SCORE;
                        bot.setDunk(400);
                    }

                    break;

                case SCORE:

                    scored++;
                    if(scored > 1) {
                        bot.lowerStackAugmentOneLevel();
                    }
                    bot.openClaw();


                    if(scored == 1) {
                        drive.followTrajectoryAsync(toStackCloseLow1);
                        currentPhase = Phase.DRIVE2;
                    }

                    if(scored == 2) {
                        drive.followTrajectoryAsync(toStackFarLow);
                        currentPhase = Phase.TO_STACK;
                    }

                    if(scored == 3 && sensed == Constants.APRILTAG_ID_THREE) {
                        drive.followTrajectoryAsync(toStackMidPole);
                        currentPhase = Phase.TO_STACK;
                    }
                    if(scored == 3 && sensed != Constants.APRILTAG_ID_THREE) {
                        drive.followTrajectoryAsync(toStackTeamHigh);
                        currentPhase = Phase.TO_STACK;
                    }

                    if(scored == 4 && sensed == Constants.APRILTAG_ID_ONE) {
                        drive.followTrajectoryAsync(park1);
                        currentPhase = Phase.PARK;
                    }

                    if(scored == 4 && sensed == Constants.APRILTAG_ID_TWO) {
                        drive.followTrajectoryAsync(park2);
                        currentPhase = Phase.PARK;
                    }

                    if(scored == 4 && sensed == Constants.APRILTAG_ID_THREE) {
                        drive.followTrajectoryAsync(park3);
                        currentPhase = Phase.PARK;
                    }





                    break;

                case DRIVE1:

                    if(!drive.isBusy()) {
                        drive.followTrajectoryAsync(toPoleCloseLowPole2);
                        currentPhase = Phase.TO_POLE;
                    }

                    break;

                case DRIVE2:

                    if(!drive.isBusy()) {
                        drive.followTrajectoryAsync(toStackCloseLow2);
                        currentPhase = Phase.TO_STACK;
                    }

                    break;


                case TO_STACK:

                    if(!drive.isBusy() && !bot.mechIsBusy()) {
                        currentPhase = Phase.INTAKE;
                        timer.reset();
                    }

                    break;

                case INTAKE:

                    bot.closeClaw();

                    if(timer.seconds() > 0.6) {
                       bot.clearAugmentUp();
                    }

                    if(timer.seconds() > 0.75) {
                        currentPhase = Phase.TO_POLE;

                        if(scored == 1) {
                            drive.followTrajectoryAsync(toPoleFarLowPole);
                        }

                        if(scored == 2) {
                            if(sensed == Constants.APRILTAG_ID_THREE) {
                                drive.followTrajectoryAsync(toPoleMidPole);
                            }
                            else {
                                drive.followTrajectoryAsync(toPoleTeamHighPole);
                            }

                        }

                        if(scored == 3) {
                            if(sensed == Constants.APRILTAG_ID_THREE) {
                                drive.followTrajectoryAsync(toPoleTeamHighPole);
                            }
                            else {
                                drive.followTrajectoryAsync(toPoleMidPole);
                            }
                        }

                    }




                    break;


                case PARK:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                    }

                    break;

                case IDLE:

                    bot.setStackAugmentZero();
                    bot.armBrakeOff();
                    telemetry.addData("all done",":)");
                    telemetry.update();

                    break;

            }
        }
    }
}