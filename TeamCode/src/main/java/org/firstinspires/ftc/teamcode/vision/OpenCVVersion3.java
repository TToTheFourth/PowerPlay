package org.firstinspires.ftc.teamcode.vision;


import static org.firstinspires.ftc.teamcode.Constants.BLUE;
import static org.firstinspires.ftc.teamcode.Constants.CAMERA_HEIGHT;
import static org.firstinspires.ftc.teamcode.Constants.CAMERA_WIDTH;
import static org.firstinspires.ftc.teamcode.Constants.GREEN;
import static org.firstinspires.ftc.teamcode.Constants.RED;
import static org.firstinspires.ftc.teamcode.Constants.YELLOW_HIGH;
import static org.firstinspires.ftc.teamcode.Constants.YELLOW_LOW;
import static org.opencv.imgproc.Imgproc.drawContours;
import static org.opencv.imgproc.Imgproc.minAreaRect;


import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.java_websocket.framing.PongFrame;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvPipeline;
import org.openftc.easyopencv.OpenCvWebcam;

import java.util.ArrayList;
import java.util.List;

public class OpenCVVersion3 {
    OpenCvWebcam webcam;
    IconPipeline pipeline;
    static final int WIDTH = CAMERA_WIDTH;
    static final int HEIGHT = CAMERA_HEIGHT;
    static boolean drawSquares;
    static boolean drawContours;
    static RotatedRect biggestRect = new RotatedRect();
    static RotatedRect smallestRect = new RotatedRect();


    /**
     * OpenCV that draws squares rather than detect them through points on the screen
     *
     * @param om LinearOpMode.
     * @param squares Will draw squares if set to true.
     * @param contours Will draw contours if set to true.
     */
    public OpenCVVersion3(LinearOpMode om, boolean squares, boolean contours) {
        drawSquares = squares;
        drawContours = contours;

        int cameraMonitorViewId = om.hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", om.hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam(om.hardwareMap.get(WebcamName.class, "Webcam 1"), cameraMonitorViewId);
        pipeline = new IconPipeline();
        webcam.setPipeline(pipeline);

        // We set the viewport policy to optimized view so the preview doesn't appear 90 deg
        // out when the RC activity is in portrait. We do our actual image processing assuming
        // landscape orientation, though.
        webcam.setViewportRenderingPolicy(OpenCvCamera.ViewportRenderingPolicy.OPTIMIZE_VIEW);

        webcam.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener()
        {
            @Override
            public void onOpened()
            {
                webcam.startStreaming(WIDTH, HEIGHT, OpenCvCameraRotation.UPRIGHT);
            }

            @Override
            public void onError(int errorCode)
            {
                /*
                 * This will be called if the camera could not be opened
                 */
            }
        });
            // Don't burn CPU cycles busy-looping in this sample
        om.sleep(50);
    }

    public static class IconPipeline extends OpenCvPipeline {
        /*
         * An enum to define the skystone position
         */
        public enum IconPosition
        {
            LEFT,
            MIDDLE_LEFT,
            CENTER,
            MIDDLE_RIGHT,
            RIGHT
        }

        Scalar blue = BLUE;
        Scalar green = GREEN;

        /*
         * The core values which define the location and size of the sample regions
         */
        static final Point REGION1_TOPLEFT_ANCHOR_POINT = new Point(0,0);
        static final Point REGION2_TOPLEFT_ANCHOR_POINT = new Point(WIDTH/5,0);
        static final Point REGION3_TOPLEFT_ANCHOR_POINT = new Point((2*WIDTH)/5,0);
        static final Point REGION4_TOPLEFT_ANCHOR_POINT = new Point((3*WIDTH)/5,0);
        static final Point REGION5_TOPLEFT_ANCHOR_POINT = new Point((4*WIDTH)/5,0);
        static final int REGION_WIDTH = WIDTH/5;
        static final int REGION_HEIGHT = HEIGHT;

        /*
         * Points which actually define the sample region rectangles, derived from above values
         *
         * Example of how points A and B work to define a rectangle
         *
         *   ------------------------------------
         *   | (0,0) Point A                    |
         *   |                                  |
         *   |                                  |
         *   |                                  |
         *   |                                  |
         *   |                                  |
         *   |                                  |
         *   |                  Point B (70,50) |
         *   ------------------------------------
         *
         */
        Point region1_pointA = new Point(
                REGION1_TOPLEFT_ANCHOR_POINT.x,
                REGION1_TOPLEFT_ANCHOR_POINT.y);
        Point region1_pointB = new Point(
                REGION1_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION1_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
        Point region2_pointA = new Point(
                REGION2_TOPLEFT_ANCHOR_POINT.x,
                REGION2_TOPLEFT_ANCHOR_POINT.y);
        Point region2_pointB = new Point(
                REGION2_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION2_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
        Point region3_pointA = new Point(
                REGION3_TOPLEFT_ANCHOR_POINT.x,
                REGION3_TOPLEFT_ANCHOR_POINT.y);
        Point region3_pointB = new Point(
                REGION3_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION3_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
        Point region4_pointA = new Point(
                REGION4_TOPLEFT_ANCHOR_POINT.x,
                REGION4_TOPLEFT_ANCHOR_POINT.y);
        Point region4_pointB = new Point(
                REGION4_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION4_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
        Point region5_pointA = new Point(
                REGION5_TOPLEFT_ANCHOR_POINT.x,
                REGION5_TOPLEFT_ANCHOR_POINT.y);
        Point region5_pointB = new Point(
                REGION5_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION5_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);

        /*
         * Working variables
         */
        Mat region1_Cb, region2_Cb, region3_Cb, region4_Cb, region5_Cb;
        Mat hsv = new Mat();
        //Mat Cb = new Mat();
        Mat thresh = new Mat();
        int avg1, avg2, avg3, avg4, avg5;

        // Volatile since accessed by OpMode thread w/o synchronization
        private volatile IconPosition position = IconPosition.LEFT;

        /*
         * This function takes the RGB frame, converts to YCrCb,
         * and extracts the Cb channel to the 'Cb' variable
         */
        void inputToHSV(Mat input)
        {
            Imgproc.cvtColor(input, hsv, Imgproc.COLOR_RGB2HSV);
            //Core.extractChannel(hsv, Cb, 2);
        }

        @Override
        public void init(Mat firstFrame)
        {
            /*
             * We need to call this in order to make sure the 'Cb'
             * object is initialized, so that the submats we make
             * will still be linked to it on subsequent frames. (If
             * the object were to only be initialized in processFrame,
             * then the submats would become delinked because the backing
             * buffer would be re-allocated the first time a real frame
             * was crunched)
             */
            inputToHSV(firstFrame);

            //TODO: Input
            Core.inRange(hsv, YELLOW_LOW, YELLOW_HIGH, thresh);

            /*
             * Submats are a persistent reference to a region of the parent
             * buffer. Any changes to the child affect the parent, and the
             * reverse also holds true.
             */
            region1_Cb = thresh.submat(new Rect(region1_pointA, region1_pointB));
            region2_Cb = thresh.submat(new Rect(region2_pointA, region2_pointB));
            region3_Cb = thresh.submat(new Rect(region3_pointA, region3_pointB));
            region4_Cb = thresh.submat(new Rect(region4_pointA, region4_pointB));
            region5_Cb = thresh.submat(new Rect(region5_pointA, region5_pointB));
        }

        @Override
        public Mat processFrame(Mat input)
        {
            /*
             * Get the Cb channel of the input frame after conversion to YCrCb
             */
            inputToHSV(input);

            //threshold hsv matrix so we only see from golden brown to microsoft yellow

            Core.inRange(hsv, YELLOW_LOW, YELLOW_HIGH, thresh);

            /*
             * Create the contours, and if enabled, draw them.
             *
             */
            ArrayList<MatOfPoint> contours = new ArrayList<>();
            Mat hiearchy = new Mat();
            Imgproc.findContours(thresh, contours, hiearchy, Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE);
            if (drawContours)
                Imgproc.drawContours(input, contours, -1 , BLUE);

            Rect r = new Rect(new Point(10,10), new Point(30,30));
            Mat points = new Mat();

            for (MatOfPoint contour : contours) {
                MatOfPoint2f newMat = new MatOfPoint2f(contour.toArray());

                RotatedRect rec = minAreaRect(newMat); //This isn't actually displayed. This gives us the output
                Point[] verts = new Point[4]; // Vertices of the square
                rec.points(verts);

                List<MatOfPoint> boxContours = new ArrayList<>();
                boxContours.add(new MatOfPoint(verts)); // Makes contour points

                if (rec.size.area() >= biggestRect.size.area()) { biggestRect = rec; }


                if (rec.size.area() <= smallestRect.size.area()) { smallestRect = rec; }


                if (drawSquares) {
                    if (rec.size.area() >= 500) {
                        Imgproc.drawContours(input, boxContours, 0, GREEN, -1); //Displays the minimum area rectangle.
                        Imgproc.circle(input, new Point(rec.center.x, rec.center.y), 5, RED, -1); // Draws the center of the square
                    }
                }
            }

            return input;
        }


    }

    public RotatedRect getBiggestRect() {
        return biggestRect;
    }

    public RotatedRect getSmallestRect() {
        return smallestRect;
    }

    public double getBiggestArea() {
        return biggestRect.size.area();
    }
}
