package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import java.util.ArrayList;

/**
 * A ridiculous project I made in my own time. Probably broken lol
 */
@Disabled
public class TracerRecall extends LinearOpMode {
    DriveTrainTestTeleOp dt = new DriveTrainTestTeleOp();
    private static int MAX_LIST = 200;
    private ArrayList<Double> lRecallx = new ArrayList<Double>();
    private ArrayList<Double> lRecally = new ArrayList<Double>();
    private ArrayList<Double> rRecallx = new ArrayList<Double>();
    boolean runningInputs = false;

    @Override
    public void runOpMode(){
        //Motors
        DcMotor bl = dt.getBackLeftMotor();
        DcMotor br = dt.getBackRightMotor();
        DcMotor fl = dt.getFrontLeftMotor();
        DcMotor fr = dt.getFrontRightMotor();

        //Motor Directions
        double rightX_G1;
        double rightY_G1;
        double leftX_G1;

        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {
                double linputy = gamepad1.left_stick_y;
                double linputx = gamepad1.left_stick_x;
                double rinputx = gamepad1.right_stick_x;


                //Start recording
                if (gamepad1.left_bumper) {
                    if (!runningInputs)
                        addInputs(linputy, linputx, rinputx);
                }

                if (gamepad1.right_bumper) {
                    runningInputs = true;

                    for (int i = lRecallx.size()-1; i >= 0; i--) {
                        rightY_G1 = lRecally.get(i);
                        rightX_G1 = lRecallx.get(i);
                        leftX_G1 = rRecallx.get(i);

                        double frontLeft = (rightX_G1 + rightY_G1 - leftX_G1);
                        double backLeft = (rightX_G1 + rightY_G1 + leftX_G1);
                        double backRight = (rightX_G1 - rightY_G1 + leftX_G1);
                        double frontRight = (rightX_G1 - rightY_G1 - leftX_G1);

                        fl.setPower(frontLeft);
                        bl.setPower(backLeft);
                        br.setPower(backRight);
                        fr.setPower(frontRight);
                    }

                    runningInputs = false;
                }
            }
        }
    }

    public void addInputs(double ly, double lx, double rx) {
        if (lRecally.size() >= MAX_LIST)
            lRecally.remove(lRecally.size()-1);
        lRecally.add(ly);

        if (lRecallx.size() >= MAX_LIST)
            lRecallx.remove(lRecallx.size()-1);
        lRecallx.add(lx);

        if (rRecallx.size() >= MAX_LIST)
            rRecallx.remove(rRecallx.size()-1);
        rRecallx.add(rx);
    }
}
