package org.firstinspires.ftc.teamcode.tests;

public class LidarController {

    LiadarTestCH lidar;

    public LidarController(LiadarTestCH lidar) {
        this.lidar = lidar;
    }

    public void startSweep(double high, double low) {
        // start sweep using the initial high and low bounds
    }

    public void stopSweep() {
        // stop the sweep
    }

    public void refineBounds() {
        // find the edges of the sweep
        // if edge found then set the high and low bounds to just larger than edges
        // if no edge is found then increase the bounds
    }

    public double getDistance() {
        // get the minimum distance using the angle and trig
        return 0.0;
    }
}
