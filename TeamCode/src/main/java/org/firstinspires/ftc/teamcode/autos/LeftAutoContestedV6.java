package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.TurtleBotCoreV3;
import org.firstinspires.ftc.teamcode.roadrunner.drive.DriveConstants;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;


@Autonomous
public class LeftAutoContestedV6 extends LinearOpMode {

    enum Phase {
        SCAN,
        TO_POLE,
        SCORE,
        TO_STACK,
        INTAKE,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int cycles = 3;
    double SLOW_MAX_ACCEL = 9;
    double SLOW_MAX_VELOCITY = 18;
    ElapsedTime timer = new ElapsedTime();


    Pose2d startPose = new Pose2d(-36, -64.5, Math.toRadians(-90));



    @Override
    public void runOpMode() throws InterruptedException {



        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCoreV3 bot = new TurtleBotCoreV3(this);
        AprilTagsVersion1 tag = new AprilTagsVersion1(this);


        drive.setPoseEstimate(startPose);

        Trajectory toPoleFromStart = drive.trajectoryBuilder(startPose,true)
                .splineToSplineHeading(new Pose2d(-36,-20,Math.toRadians(-90)),Math.toRadians(90))
                .splineToSplineHeading(new Pose2d(-32.5,-11,Math.toRadians(-145)),  Math.toRadians(40))
                .addTemporalMarker(1, () -> bot.extendArmAndSlide(3))
                .addTemporalMarker(1, () -> bot.setStackAugmentFive())
                .addSpatialMarker(new Vector2d(-32.6,-11.1), () -> bot.setDunk(400))

                .build();

        Trajectory toStack = drive.trajectoryBuilder(toPoleFromStart.end(),false)
                .splineToSplineHeading(new Pose2d(-48,-16,Math.toRadians(180)),Math.toRadians(180),SampleMecanumDrive.getVelocityConstraint(SLOW_MAX_VELOCITY,SLOW_MAX_ACCEL,DriveConstants.TRACK_WIDTH), SampleMecanumDrive.getAccelerationConstraint(SLOW_MAX_ACCEL))
                .splineToSplineHeading(new Pose2d(-61.5,-16,Math.toRadians(180)),Math.toRadians(180),SampleMecanumDrive.getVelocityConstraint(SLOW_MAX_VELOCITY,SLOW_MAX_ACCEL,DriveConstants.TRACK_WIDTH), SampleMecanumDrive.getAccelerationConstraint(SLOW_MAX_ACCEL))
                .addTemporalMarker(0.8, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.8, () -> bot.setDunk(0))
                .build();

        Trajectory toPoleNormal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-45,-16,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-32.5,-11,Math.toRadians(-145)),  Math.toRadians(30))
                .addTemporalMarker(0.1, () -> bot.extendArmAndSlide(3))
                .addTemporalMarker(0.5, ()-> bot.clearAugmentDown())
                .addSpatialMarker(new Vector2d(-32.6,-11.1), () -> bot.setDunk(400))
                .build();

        Trajectory toPoleFinal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-21,-18,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-8.5,-23.5,Math.toRadians(145)),  Math.toRadians(-30))
                .addTemporalMarker(0.5, () -> bot.extendArmAndSlide(3))
                .addTemporalMarker(0.9, ()-> bot.clearAugmentDown())
                .addSpatialMarker(new Vector2d(-8.6,-23.4), () -> bot.setDunk(400))
                .build();

        Trajectory park1 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-16,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-60,-16,Math.toRadians(180)),Math.toRadians(180))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.5, () -> bot.setDunk(0))

                .build();

        Trajectory park2 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .lineToLinearHeading(new Pose2d(-36, -16, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.5, () -> bot.setDunk(0))
                .build();

        Trajectory park3 = drive.trajectoryBuilder(toPoleFinal.end(),false)
                .lineToLinearHeading(new Pose2d(-12, -18, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.5, () -> bot.setDunk(0))
                .build();



       // int preSensed = bot.scanAprilTagTime(tag, 1);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        bot.closeClaw();

        waitForStart();

       // int sensed = bot.scanAprilTagTime(tag, 1);

        int sensed = Constants.APRILTAG_ID_THREE;

        bot.closeClaw();


        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMechAutomated(1,1);




            switch (currentPhase) {

                case SCAN:

                    telemetry.addData("signal state", sensed);
                    telemetry.update();

                    currentPhase = Phase.TO_POLE;
                    drive.followTrajectoryAsync(toPoleFromStart);
                    bot.setDunk(-400);

                    break;


                case TO_POLE:

                    if (!drive.isBusy()) {
                        currentPhase = Phase.SCORE;
                    }

                    break;

                case SCORE:

                    bot.setDunk(-400);
                    scored++;
                    if(scored > 1) {
                        bot.lowerStackAugmentOneLevel();
                    }
                    bot.openClaw();


                    if(scored == cycles + 1) {
                        currentPhase = Phase.PARK;
                        if(sensed == Constants.APRILTAG_ID_ONE) {
                            drive.followTrajectoryAsync(park1);
                        }

                        if(sensed == Constants.APRILTAG_ID_TWO) {
                            drive.followTrajectoryAsync(park2);
                        }

                        if(sensed == Constants.APRILTAG_ID_THREE) {
                            drive.followTrajectoryAsync(park3);
                        }

                    }

                    if(scored < cycles + 1) {
                        currentPhase = Phase.TO_STACK;
                        drive.followTrajectoryAsync(toStack);
                    }





                    break;


                case TO_STACK:

                    if(!drive.isBusy()) {
                        currentPhase = Phase.INTAKE;
                        timer.reset();
                    }

                    break;

                case INTAKE:

                    bot.closeClaw();

                    if(timer.seconds() > 0.6) {
                        bot.clearAugmentUp();
                    }

                    if(timer.seconds() > 0.75) {
                        currentPhase = Phase.TO_POLE;

                        if(scored == cycles && sensed == Constants.APRILTAG_ID_THREE) {
                            drive.followTrajectoryAsync(toPoleFinal);
                            bot.setDunk(-400);
                        }
                        else {
                            drive.followTrajectoryAsync(toPoleNormal);
                            bot.setDunk(-400);
                        }
                    }




                    break;


                case PARK:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                        bot.retractArmAndSlide();
                    }

                    break;

                case IDLE:

                    bot.setStackAugmentZero();
                    bot.armBrakeOff();
                    telemetry.addData("all done",":)");
                    telemetry.update();

                    break;

            }
        }
    }
}