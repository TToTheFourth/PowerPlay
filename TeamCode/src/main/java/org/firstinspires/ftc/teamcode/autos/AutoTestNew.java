package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled
@Autonomous
public class AutoTestNew extends LinearOpMode {

    enum Phase {
        One,
        Two,
        Three,
        Four,
        Five,
        IDLE
    }

    Phase currentPhase = Phase.One;

    ElapsedTime timer = new ElapsedTime();

    int scored = 0;
    int cycles = 3;
    int stackAugment = 0; // Augments the resting base position to reach cones on top of the stack
    int signalState = 1; // Uh I don't know what numbers are mapped to which signal and parking spot so please change

    Pose2d startPose = new Pose2d(35, -60, Math.toRadians(90));


    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCore bot = new TurtleBotCore(this);
        OpenCVVersion4 cv = new OpenCVVersion4(this);
        // TODO: Uh I don't know what numbers are mapped to which signal and parking spot so please change
        int signalState = bot.coneColor(cv);

        Trajectory reposition1 = drive.trajectoryBuilder(startPose,false)
                .lineToLinearHeading(new Pose2d(12,-60,Math.toRadians(90)))
                .build();

        Trajectory toFirstPole = drive.trajectoryBuilder(reposition1.end(),false)
                .lineToLinearHeading(new Pose2d(12,-11,Math.toRadians(45)))
                .build();

        Trajectory repositionToStack = drive.trajectoryBuilder(toFirstPole.end(),false)
                .lineToLinearHeading(new Pose2d(57,-12,Math.toRadians(0)))
                .build();

        Trajectory toSecondPole = drive.trajectoryBuilder(repositionToStack.end(), true)
                .lineToLinearHeading(new Pose2d(40,-12,Math.toRadians(-40)))
                .build();

        Trajectory toStack = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(58, -12, 0))
                .build();

        Trajectory park1A = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(10, -12, Math.toRadians(0)))
                .build();
        /**
        Trajectory park2A = drive.trajectoryBuilder(park1A.end(),false)
                .strafeRight(24)
                .build();
        */
        Trajectory park1B = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(34, -12, Math.toRadians(0)))
                .build();
        /**
        Trajectory park2B = drive.trajectoryBuilder(park1B.end(),false)
                .strafeRight(24)
                .build();
         */
        Trajectory park1C = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(58, -12, Math.toRadians(0)))
                .build();
        /**
        Trajectory park2C = drive.trajectoryBuilder(park1C.end(),false)
                .strafeRight(24)
                .build();
         */


        waitForStart();
        timer.reset();
        bot.extendArmAndSlide();

        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMech(0,0);



            switch (currentPhase) {


                case One:

                    if(timer.seconds() > 2) {
                        currentPhase = Phase.Two;
                        bot.outtake();
                        timer.reset();
                    }

                    break;

                case Two:

                    if(timer.seconds() > 2) {
                        currentPhase = Phase.IDLE;
                        bot.retractArmAndSlide();
                        timer.reset();
                    }

                    break;


                case IDLE:

                    break;

            }
        }











    }
}


