package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion3;
import org.opencv.core.Point;

@Disabled
@TeleOp
public class SensingStateTest extends LinearOpMode {

    public static final int CAMERA_WIDTH = 640;
    public static final int CAMERA_HEIGHT = 480;

    @Override
    public void runOpMode() throws InterruptedException {
        OpenCVVersion3 cv = new OpenCVVersion3(this, true, true);
        ForWhatWeHaveNow b = new ForWhatWeHaveNow(this);
        Servo liadarServo = hardwareMap.get(Servo.class, "liadarServo");
        DistanceSensor distance = hardwareMap.get(DistanceSensor.class, "distance");
        LiadarTestCH l  = new LiadarTestCH(liadarServo, distance);

        Thread thread = new Thread(l);
        thread.start();

        waitForStart();

        while (opModeIsActive()) {
            Point rxPoint = cv.getBiggestRect().center;
            double rx = rxPoint.x;
            double mx = CAMERA_WIDTH / 2.;
            double d = l.getDistanceLowest();
            double tgt = 8.;
            if (rx > mx) {
                b.slide(0.2, rx-mx);
            } else if (mx > rx) {
                b.slide(-0.2, mx-rx);
            } else if (Math.abs(rx - mx) < 2.5) {
                if (d > tgt) {
                    b.goForward(0.2, d-tgt);
                } else if (d < tgt) {
                    b.goForward(-0.2, tgt-d);
                } else if (Math.abs(d-tgt) < 2.5) {
                    b.openClaw();
                }
            }
        }
    }
}
