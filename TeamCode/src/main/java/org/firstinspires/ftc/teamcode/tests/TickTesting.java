package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@Disabled
@TeleOp()
public class TickTesting extends LinearOpMode {
    private DcMotor leftLinear;
    private DcMotor rightLinear;
    private DcMotor arm;

    @Override
    public void runOpMode() throws InterruptedException {
        leftLinear = hardwareMap.get(DcMotor.class, "leftLinear");
        rightLinear = hardwareMap.get(DcMotor.class, "rightLinear");

        leftLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        arm = hardwareMap.get(DcMotor.class, "arm");
        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {
                float leftStick = gamepad1.left_stick_y * -1; //Up is 1.0 and down is -1.0

                int ticks = leftLinear.getCurrentPosition(); //Lowest 0

                /*
                 * Data Table values:
                 * 1.) -3925
                 * 2.) -3911
                 * 3.) -3938
                 *
                 * Avg: -3925
                 *
                 * High Pole: -3215
                 *
                 * Middle Pole: -1601
                 *
                 * Low Pole: 0
                 */

//                arm.setPower(leftStick * -1);
                leftLinear.setPower(leftStick*-1);
                rightLinear.setPower(leftStick);
                telemetry.addData("Ticks", ticks);
                telemetry.addData("Stick: ", leftStick);
                telemetry.update();

                //-990 - 5
                //-816 - 4
                //-620 - 3
                //-458 - 2
                //-135 - 1
            }
        }
    }
}
