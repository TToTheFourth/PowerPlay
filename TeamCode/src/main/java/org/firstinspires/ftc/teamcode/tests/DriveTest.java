package org.firstinspires.ftc.teamcode.tests;

import static org.firstinspires.ftc.teamcode.Constants.MAXIMUM_EXTENSION;
import static org.firstinspires.ftc.teamcode.Constants.MINIMUM_RETRACTION;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Constants;

@Disabled
@TeleOp
public class DriveTest extends LinearOpMode {
    private DcMotor leftLinearSlide;
    private DcMotor rightLinearSlide;
    private DcMotor arm;

    private DcMotor backLeftMotor;
    private DcMotor frontLeftMotor;
    private DcMotor frontRightMotor;
    private DcMotor backRightMotor;

    private Servo grabber1;
    private Servo grabber2;

    private double integralSum;
    private double kP = 1.0;
    private double kI = 0;
    private double kD = 0;

    private double lastError;
    ElapsedTime timer = new ElapsedTime();

    @Override
    public void runOpMode() throws InterruptedException {
        //Linear Slides and arm
        leftLinearSlide = hardwareMap.get(DcMotor.class, "leftLinear");
        rightLinearSlide = hardwareMap.get(DcMotor.class, "rightLinear");
        arm = hardwareMap.get(DcMotor.class, "arm");

        // Drive train init
        backLeftMotor = hardwareMap.get(DcMotor.class, "motor0");
        frontLeftMotor = hardwareMap.get(DcMotor.class, "motor1");
        frontRightMotor = hardwareMap.get(DcMotor.class, "motor2");
        backRightMotor = hardwareMap.get(DcMotor.class, "motor3");


        arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftLinearSlide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinearSlide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftLinearSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightLinearSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftLinearSlide.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightLinearSlide.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);


        //Grabber init. Grabber 1 is the left and 2 on the right from intake side.
        grabber1 = hardwareMap.get(Servo.class, "grabber1");
        grabber2 = hardwareMap.get(Servo.class, "grabber2");

        double rightX_G1;
        double rightY_G1;
        double leftX_G1;

        Servo liadarServo = hardwareMap.get(Servo.class, "liadarServo");
        DistanceSensor distance = hardwareMap.get(DistanceSensor.class, "distance");
        LiadarTestCH lidar = new LiadarTestCH(liadarServo, distance);

        waitForStart();

        Thread th = new Thread(lidar);
        th.start();

        // initialize to closed position
        grabber1.setPosition(0.75);
        grabber2.setPosition(0.80);

        boolean slidetop = false;
        boolean slidebottom = false;
        if (opModeIsActive()) {
            while (opModeIsActive()) {

                rightY_G1 = -gamepad1.left_stick_y;
                rightX_G1 = -gamepad1.right_stick_x;
                leftX_G1 = -gamepad1.left_stick_x;

                double frontLeft = (rightX_G1 + rightY_G1 - leftX_G1);
                double backLeft = (rightX_G1 + rightY_G1 + leftX_G1);
                double backRight = (rightX_G1 - rightY_G1 + leftX_G1);
                double frontRight = (rightX_G1 - rightY_G1 - leftX_G1);

                frontLeftMotor.setPower(frontLeft);
                backLeftMotor.setPower(backLeft);
                backRightMotor.setPower(backRight);
                frontRightMotor.setPower(frontRight);

                if(Math.abs(leftLinearSlide.getCurrentPosition()) >= MAXIMUM_EXTENSION - 200) {
                    slidetop = true;
                } else {
                    slidetop = false;
                }

                if(Math.abs(leftLinearSlide.getCurrentPosition()) <= 100) {
                    slidebottom = true;
                } else {
                    slidebottom = false;
                }

                if (gamepad1.dpad_up) {
                    if(slidetop) {
                        leftLinearSlide.setPower(0.0);
                        rightLinearSlide.setPower(0.0);
                    } else {
                        leftLinearSlide.setPower(ticRamp(3000,leftLinearSlide.getCurrentPosition(),-1.0));
                        rightLinearSlide.setPower(ticRamp(3000,leftLinearSlide.getCurrentPosition(),1.0));
                    }
                } else if (gamepad1.dpad_down) {
                    if(slidebottom) {
                        leftLinearSlide.setPower(0.0);
                        rightLinearSlide.setPower(0.0);
                    } else {
                        leftLinearSlide.setPower(0.5);
                        rightLinearSlide.setPower(-0.5);
                    }
                } else {
                    leftLinearSlide.setPower(0);
                    rightLinearSlide.setPower(0.0);
                }

                if (gamepad1.left_bumper) {
                    // open
                    grabber1.setPosition(0.83);
                    grabber2.setPosition(0.58);
                } else if (gamepad1.right_bumper) {
                    // close
                    grabber1.setPosition(0.75);
                    grabber2.setPosition(0.80);
                }
                telemetry.addData("Forward Pod: ", frontLeftMotor.getCurrentPosition());
                telemetry.addData("Slide Pod: ", frontRightMotor.getCurrentPosition());
                telemetry.addData("Linear: ", leftLinearSlide.getCurrentPosition());
                telemetry.addData("lowestAngle: ", lidar.getAngleLowest());
                telemetry.update();

            }
        }

        lidar.stop();
        frontLeftMotor.setPower(0);
        backLeftMotor.setPower(0);
        frontRightMotor.setPower(0);
        backRightMotor.setPower(0);
    }

    private double ticRamp(int goal, int cur, double power) {
        double val = 0.0;

        if(Math.abs(goal) - Math.abs(cur) <= 300) {
            val = power * (Math.abs(goal) - Math.abs(cur)) / 300;
        } else {
            val = power;
        }

        return val;
    }

    public double timeRamp(double power, long startTime) {
        // ramp for 0.75 seconds
        long t = System.currentTimeMillis() - startTime;
        if (t >= 750) {
            return power;
        } else {
            return power / 750 * t;
        }
    }

}
