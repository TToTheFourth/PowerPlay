package org.firstinspires.ftc.teamcode.vision;

import static android.os.SystemClock.sleep;
import static org.firstinspires.ftc.teamcode.Constants.LIDAR_DELAY;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

public class LiadarDetector {
    //Classes
    private Servo liadar;
    private DistanceSensor distanceDetect;

    private double angle;
    private double distance;
    private double position;
    private final int DELAY = LIDAR_DELAY;
    private double robotTriangleX = 0;
    private double robotTriangleY = 0;
    private double robotThetaToObject = 0;
    private double LIDAR_LEFT_OFSET = 15; // cm
    private double LIDAR_FRONT_OFSET = 13; // cm

    /**
     * Constructor of the Lidar sensor
     * @param om LinearOpMode
     */
    public LiadarDetector (LinearOpMode om) {
        liadar = om.hardwareMap.get(Servo.class, "liadarServo");
        distanceDetect = om.hardwareMap.get(DistanceSensor.class, "distance");
    }

    /**
     * Method to begin object detection
     * @param cycles The number of times it repeats.
     * @param startPosition The starting Servo position.
     */
    public void detectObjects(int cycles, int startPosition) {
        double dcm1;
        double positionLowest = Double.MAX_VALUE;
        double angleLowest = Double.MAX_VALUE;
        double distanceLowest = Double.MAX_VALUE;

        if (startPosition == 1) {
            liadar.setPosition(1);
        } else {
            liadar.setPosition(0);
        }

        //TODO: Fix this bruh this is broken
        //FACTS

        if (cycles != 0) {
            for (int x = 0; x < cycles; x++) {
                if (liadar.getPosition() == 0) {
                    for (double i = 0; i >= 1; i += 0.02) {
                        liadar.setPosition(i);
                        sleep(DELAY);
                        dcm1 = distanceDetect.getDistance(DistanceUnit.CM);

                        if (dcm1 < distanceLowest) {
                            distanceLowest = dcm1;
                            angleLowest = i * 270 - 135;
                            positionLowest = i;
                        }
                    }
                } else {
                    for (double i = 1; i >= 0; i += -0.02) {
                        liadar.setPosition(i);
                        sleep(DELAY);
                        dcm1 = distanceDetect.getDistance(DistanceUnit.CM);

                        if (dcm1 < distanceLowest) {
                            distanceLowest = dcm1;
                            angleLowest = i * 270 - 135;
                            positionLowest = i;
                        }
                    }
                }


                // Set position to closest element
                liadar.setPosition(positionLowest);
                angle = angleLowest;
                distance = distanceLowest;
                position = positionLowest;

                // TODO: Incorporate function returning the angle of the object relative to the robot
                /**
                 *  Something sorta like:
                 *  robotThetaToObject =
                 *
                 *  robotTriangleX =  distance * cos(lidarThetaToObject) - LIDAR_LEFT_OFSET
                 *  robotTriangleY = distance * sin(lidarThetaToObject) + LIDAR_FRONT_OFSET
                 *
                 *  robotThetaToObject: arctan(robotTriangleY / robotTriangleX)
                 *
                 *
                 */

                robotTriangleX =  distance * Math.cos(angle) - LIDAR_LEFT_OFSET;
                robotTriangleY = distance * Math.sin(angle) + LIDAR_FRONT_OFSET;

                robotThetaToObject = Math.atan(robotTriangleY / robotTriangleX);


            }
        }
    }

    /**
     * Getter for lowest angle
     * @return Lowest angle
     */
    public double getAngle() {
        return angle;
    }

    /**
     * Getter for lowest distance
     * @return Lowest distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Getter for the Servo position where it found the closest object
     * @return Closest object position
     */
    public double getPosition() {
        return position;
    }

    public double getCurrentDistance() {
        return distanceDetect.getDistance(DistanceUnit.CM);
    }

    public double getRobotThetaToObject() {
        return getRobotThetaToObject();
    }
}

//TODO:
/*
 * Optimize the sweeping radius of the lidar sensor to make it quicker
 *
 * Change the sweeping so that it only needs to sweep once to locate an object
 *
 * Create a class for using the lidar sensor so it is easily accessible through a common class
 *
 * Maybe utilize trigonometry to figure out the distance from the center of the screen to the object in pixels?
 */