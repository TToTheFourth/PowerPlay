package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.subsystems.Arm;
import org.firstinspires.ftc.teamcode.vision.LiadarDetector;

@Disabled
@TeleOp
public class LidarTest extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        LiadarDetector lid = new LiadarDetector(this);
        Arm arm = new Arm(this);

        waitForStart();
        if (opModeIsActive()) {
            while(opModeIsActive()){
                if (gamepad1.right_bumper)
                    lid.detectObjects(1, 0);
                else if (gamepad1.left_bumper)
                    lid.detectObjects(1, 1);
//                arm.runLinearSlides(1, Arm.Direction.UP);
//                sleep(1000);
//                arm.stopLinearSlides();
//                sleep(1000);
//                arm.runLinearSlides(1, Arm.Direction.DOWN);
//                sleep(5000);
            }
        }

    }
}
