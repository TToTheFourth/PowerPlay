package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;
import org.firstinspires.ftc.teamcode.RepBot;

@Disabled
public class RepBotTesting extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        ForWhatWeHaveNow rep = new ForWhatWeHaveNow(this);

        waitForStart();

        rep.goForward(1, 12);
        sleep(5000);
        rep.goForward(-1, 12);
        sleep(5000);
    }
}
