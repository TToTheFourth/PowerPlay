package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled
@Autonomous
public class RightAutoRRV3 extends LinearOpMode {

    enum Phase {
        SCAN,
        REPOSITION1,
        REPOSITION2,
        TO_POLE,
        SCORE,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int signalState = 3; // Uh I don't know what numbers are mapped to which signal and parking spot so please change

    int AUGMENT_DOWN = 0;

    boolean raising = false;
    boolean waiting = false;
    ElapsedTime timer = new ElapsedTime();

    Pose2d startPose = new Pose2d(35, -64, Math.toRadians(90));



    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCore bot = new TurtleBotCore(this);
        OpenCVVersion4 cv = new OpenCVVersion4(this);
        // TODO: Uh I don't know what numbers are mapped to which signal and parking spot so please change


        drive.setPoseEstimate(startPose);

        Trajectory reposition1 = drive.trajectoryBuilder(startPose,false)
                .lineToLinearHeading(new Pose2d(12,-60,Math.toRadians(90)))
                .build();

        Trajectory reposition2 = drive.trajectoryBuilder(reposition1.end(),false)
                .lineToLinearHeading(new Pose2d(12,-15,Math.toRadians(90)))
                .build();


        Trajectory toFirstPole = drive.trajectoryBuilder(reposition2.end().plus(new Pose2d(0,0,Math.toRadians(-45))),false)
                .lineToLinearHeading(new Pose2d(11,-15,Math.toRadians(45)))
                .build();


        Trajectory park1A = drive.trajectoryBuilder(toFirstPole.end(),false)
                .lineToLinearHeading(new Pose2d(12, -16, Math.toRadians(0)))
                .addTemporalMarker(1.5, () -> bot.retractArmAndSlide())
                .build();


/**
        Trajectory park2A = drive.trajectoryBuilder(park1A.end(),false)
                .strafeLeft(24)
                .build();
*/
        Trajectory park1B = drive.trajectoryBuilder(toFirstPole.end(),false)
                .lineToLinearHeading(new Pose2d(36, -16, Math.toRadians(0)))
                .addTemporalMarker(1.5, () -> bot.retractArmAndSlide())
                .build();
/**
        Trajectory park2B = drive.trajectoryBuilder(park1B.end(),false)
                .strafeLeft(24)
                .build();
*/
        Trajectory park1C = drive.trajectoryBuilder(toFirstPole.end(),false)
                .lineToLinearHeading(new Pose2d(58, -16, Math.toRadians(0)))
                .addTemporalMarker(1.5, () -> bot.retractArmAndSlide())
                .build();
/**
        Trajectory park2C = drive.trajectoryBuilder(park1C.end(),false)
                .strafeLeft(24)
                .build();
*/

        bot.claw(true);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        waitForStart();

        //signalState = cv.iconPos(); //bot.coneColor()
        signalState = bot.coneColorTimeBased(cv);
        //signalState = 3;

        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMech(100,0);

            telemetry.addData("signal state", signalState);
            telemetry.update();


            switch (currentPhase) {

                case SCAN:

                    currentPhase = Phase.REPOSITION1;
                    drive.followTrajectoryAsync(reposition1);

                    break;

                case REPOSITION1:

                    if(!drive.isBusy()) {
                        currentPhase = Phase.REPOSITION2;
                        drive.followTrajectoryAsync(reposition2);
                    }

                    break;


                case REPOSITION2:

                    if (!drive.isBusy()) {
                        drive.turnAsync(Math.toRadians(-45));
                        currentPhase = Phase.TO_POLE;
                        bot.extendArmAndSlide();
                    }
                    break;



                case TO_POLE:
                    if (!drive.isBusy() && !bot.shouldExtend && !waiting) {
                        waiting = true;
                        drive.followTrajectoryAsync(toFirstPole);
                    }

                    if(waiting && !drive.isBusy()) {
                        waiting = false;
                        currentPhase = Phase.SCORE;
                    }

                    break;

                case SCORE:
                    if(!waiting) {
                        AUGMENT_DOWN = -100;
                        bot.outtake();
                        waiting = true;
                        timer.reset();
                        //drive.turnAsync(Math.toRadians(45));
                    }

                    else if (waiting && !drive.isBusy() && timer.seconds() > 0.1) {
                        AUGMENT_DOWN = 0;
                        waiting = false;
                        currentPhase = Phase.PARK;
                        if(signalState == 1) {
                            drive.followTrajectoryAsync(park1A);
                        }
                        else if(signalState == 2) {
                            drive.followTrajectoryAsync(park1B);
                        }
                        if(signalState == 3) {
                            drive.followTrajectoryAsync(park1C);
                        }
                        //bot.retractArmAndSlide();
                    }

                    break;




                case PARK:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                        /**
                        if(signalState == 1) {
                            drive.followTrajectoryAsync(park2A);
                        }
                        else if(signalState == 2) {
                            drive.followTrajectoryAsync(park2B);
                        }
                        if(signalState == 3) {
                            drive.followTrajectoryAsync(park2C);
                        } */
                    }

                    break;
/**
                case PARK2:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                    }
                    break;
*/
                case IDLE:

                    break;

            }
        }











    }
}


