package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Constants.ARM_HIGH;
import static org.firstinspires.ftc.teamcode.Constants.ARM_LOW;
import static org.firstinspires.ftc.teamcode.Constants.HIGH_POLE;
import static org.firstinspires.ftc.teamcode.Constants.LOW_POLE;
import static org.firstinspires.ftc.teamcode.Constants.MID_POLE;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Open;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Open;

import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.tests.LiadarTestCH;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;
import org.openftc.apriltag.AprilTagDetection;

import java.util.ArrayList;

public class TurtleBotCore {

    private LinearOpMode opMode;


    private DcMotor intake;
    private DcMotor rightLinear;
    private DcMotor leftLinear;
    private DcMotor arm;

    private Servo grabber1;
    private Servo grabber2;
    private Servo flag;

    private RevColorSensorV3 color;

    private ElapsedTime clawTime;
    private ElapsedTime timer;
    private ElapsedTime aprilTagScannerTime;

    int level = 0; // 0 is all the way down, 1 is low pole, 2 is mid pole, 3 is high pole
    int armPosBozo = -1; // -1 is resting/intaking position, 0 is middle, and 1 is extended out the back
    boolean retracting = false;
    boolean inputReceived = true;
    boolean lowering = false;
    public static boolean shouldRetract = false;
    public static boolean shouldExtend = false;
    boolean raising = false;
    boolean extending = false;
    boolean intaking = false;
    public static boolean coneLoaded = true;
    double powerSend = 0;

    boolean slidebottom = true;







    public TurtleBotCore(LinearOpMode om) {
        this.opMode = om;

        timer = new ElapsedTime();

        color = om.hardwareMap.get(RevColorSensorV3.class, "color");

        intake = om.hardwareMap.get(DcMotor.class, "intake");
        rightLinear = om.hardwareMap.get(DcMotor.class, "rightLinear");
        leftLinear = om.hardwareMap.get(DcMotor.class, "leftLinear");
        arm = om.hardwareMap.get(DcMotor.class, "arm");

        //Grabber init. Grabber 1 is the left and 2 on the right from intake side.
        grabber1 = om.hardwareMap.get(Servo.class, "grabber1");
        grabber2 = om.hardwareMap.get(Servo.class, "grabber2");
        flag = om.hardwareMap.get(Servo.class, "flag");

        Servo liadarServo = om.hardwareMap.get(Servo.class, "liadarServo");
        DistanceSensor distance = om.hardwareMap.get(DistanceSensor.class, "distance");
        LiadarTestCH lidar = new LiadarTestCH(liadarServo, distance);

        clawTime = new ElapsedTime();

        leftLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        intake.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        aprilTagScannerTime = new ElapsedTime();

        //telemetry.addData("Status", "Initialized");
        //telemetry.update();

    }

    public int getLevel() {
        return  level;
    }

    public void outtake() {
        claw(false);
        coneLoaded = false;
    }

    public void retractArmAndSlide() {
        shouldRetract = true;

    }

    public void extendArmAndSlide() {
        shouldExtend = true;
    }

    public void updateScoringMech(int retractLevelAugment, int dunk) {

        opMode.telemetry.addData("level",level);

        int pos = leftLinear.getCurrentPosition();
        int armPos = arm.getCurrentPosition();

        if(Math.abs(leftLinear.getCurrentPosition()) <= 250) {
            slidebottom = true;
        } else {
            slidebottom = false;
        }


        if (color.getRawLightDetected() > 300 && !intaking) {
            claw(true);
            intaking = true;
            clawTime.reset();
        }

        if (intaking && clawTime.seconds() > 0.5) {
            intaking = false;
            coneLoaded = true;
        }




        if (shouldRetract) {
            if (!retracting && !lowering) {
                retracting = true;
                clawTime.reset();
            }

            if (retracting && clawTime.seconds() > 0.5) {
                claw(true);
                armPosBozo = -1;
                level = 0;
                retracting = false;
                lowering = true;
                opMode.telemetry.addData("retracting","hi");
            }

            if (lowering && slidebottom) {
                claw(false);
                lowering = false;
                shouldRetract = false;
            }

        }

        if (shouldExtend) {
            if (!raising && !extending) {
                claw(true);
                raising = true;
                clawTime.reset();
            }

            if (raising && clawTime.seconds() > 0.5) {
                level = 3;
            }

            if (raising && clawTime.seconds() > 1.2) {
                armPosBozo = 1;
                raising = false;
                extending = true;
            }

            if (extending && clawTime.seconds() > 1.7) {
                extending = false;
                shouldExtend = false;
            }

        }

        // Update Slide Power
        if (level ==1) {
            double power = slideTicRamp(LOW_POLE, pos, 1);
            powerSend = power;
            leftLinear.setPower(-power);
            rightLinear.setPower(power);
        }
        else if (level == 2){
            double power = slideTicRamp(MID_POLE, pos, 1);
            powerSend = power;
            leftLinear.setPower(-power);
            rightLinear.setPower(power);
        }
        else if (level == 3) {
            double power = slideTicRamp(HIGH_POLE - dunk, pos, 1);
            powerSend = power;
            leftLinear.setPower(-power);
            rightLinear.setPower(power);
        }
        else if (level == 0) {
            double power = slideTicRamp(LOW_POLE + retractLevelAugment, pos, 1);
            powerSend = power;
            leftLinear.setPower(-power);
            rightLinear.setPower(power);
        }
        else {
            powerSend = 0;
            leftLinear.setPower(0);
            rightLinear.setPower(0);
        }

        // Update Arm Power
        if (armPosBozo == 1 && (level > 1 || armPos > 500)) {
            double armPower = armTicRamp(ARM_HIGH, armPos, 1);
            arm.setPower(armPower);
        } else if (armPosBozo == -1) {
            double armPower = armTicRamp((ARM_LOW), armPos, 1);
            arm.setPower(armPower);
        } else {
            arm.setPower(0);
        }


        opMode.telemetry.update();
    }

    public double getSlidePower() {
        return powerSend;
    }

    private double slideTicRamp(int goal, int cur, double power) {
        double val = 0.0;

        if(Math.abs(Math.abs(goal) - Math.abs(cur)) <= 300) {


            val = power * (Math.abs(goal) - Math.abs(cur)) / 300;
        } else if (Math.abs(goal) > Math.abs(cur)) {
            val = power;
        }
        else if (Math.abs(goal) <= Math.abs(cur)) {
            val = -power;
        }

        return val;
    }

    private double armTicRamp(int goal, int cur, double power) {
        double val = 0.0;

        if(Math.abs(goal - cur) <= 600) {


            val = power * (goal - cur) / 600;
        } else if (Math.abs(goal) > Math.abs(cur)) {
            val = power;
        }
        else if (Math.abs(goal) <= Math.abs(cur)) {
            val = -power;
        }

        return val;
    }

    public void claw (boolean closed) {
        if (closed) {
            grabber1.setPosition(grabber1Closed);
            grabber2.setPosition(grabber2Closed);

        }

        else {
            grabber1.setPosition(grabber1Open);
            grabber2.setPosition(grabber2Open);
            flag.setPosition(0.8);
        }

    }

    public int coneColorTimeBased(OpenCVVersion4 v) {
        int position;
        int orange = 0;
        int pink = 0;
        int green = 0;

        timer.reset();
        while(timer.seconds() < 5) {
            position = v.iconPos();

            if (position == 1) {
                green++;
                //green
            } else if (position == 2) {
                pink++;
                //pink
            } else if (position == 3) {
                orange++;
                //orange
            }
        }

        if (orange > green && orange > pink) {
            return 3;
        } else if (green > orange && green > pink) {
            return 1;
        } else if (pink > orange && pink > green) {
            return 2;
        } else {
            return 2;
        }
    }

    public int coneColor(OpenCVVersion4 v) {
        int position;
        int orange = 0;
        int pink = 0;
        int green = 0;

        for (int i = 0; i < 10; i++) {
            position = v.iconPos();

            if (position == 1) {
                green++;
                //green
            } else if (position == 2) {
                pink++;
                //pink
            } else if (position == 3) {
                orange++;
                //orange
            }
        }

        if (orange > green && orange > pink) {
            return 3;
        } else if (green > orange && green > pink) {
            return 1;
        } else if (pink > orange && pink > green) {
            return 2;
        } else {
            return 2;
        }
    }



    /**
     * Scans a specified amount of times using AprilTag
     * @param aprilTagDetector
     * @return The ID of the detected Apriltag
     */
    public int scanAprilTag(AprilTagsVersion1 aprilTagDetector) {
        ArrayList<AprilTagDetection> detections;
        int detectionTypeOne = 0;
        int detectionTypeTwo = 0;
        int detectionTypeThree = 0;
        int incorrectDetections = 0;
        int noDetections = 0;

        detections = aprilTagDetector.getDetectionsUpdate();

        if (detections != null) {
            for (int i = 0; i < 100; i++) { // May need to change based off of time
                if (detections.size() > 0) {
                    for (AprilTagDetection detect : detections) {
                        int detectID = detect.id;

                        if (detectID == Constants.APRILTAG_ID_ONE) {
                            detectionTypeOne++;
                        } else if (detectID == Constants.APRILTAG_ID_TWO) {
                            detectionTypeTwo++;
                        } else if (detectID == Constants.APRILTAG_ID_THREE) {
                            detectionTypeThree++;
                        } else {
                            incorrectDetections++;
                        }
                    }
                } else {
                    noDetections++; //
                }
            }

            // Code then decides which one to return
            int returningDetection;
            int totalDetects = detectionTypeOne + detectionTypeTwo + detectionTypeThree + incorrectDetections;
            double certainty;

            if (detectionTypeOne > detectionTypeTwo && detectionTypeOne > detectionTypeThree) {
                returningDetection = Constants.APRILTAG_ID_ONE;
                certainty = Math.round((double) detectionTypeOne / totalDetects) * 100;
            } else if (detectionTypeTwo > detectionTypeOne && detectionTypeTwo > detectionTypeThree) {
                returningDetection = Constants.APRILTAG_ID_TWO;
                certainty = Math.round((double) detectionTypeTwo / totalDetects) * 100;
            } else {
                returningDetection = Constants.APRILTAG_ID_THREE;
                certainty = Math.round((double) detectionTypeThree / totalDetects) * 100;
            }

            double error = Math.round((double) incorrectDetections / totalDetects) * 100;

            // Not needed. Like to have for testing
            opMode.telemetry.addLine(String.format("Detection: %d", returningDetection));
            opMode.telemetry.addLine(String.format("Certainty: %f", certainty));
            opMode.telemetry.addLine(String.format("Error: %f", error));
            opMode.telemetry.addLine(String.format("No scans: %d", noDetections));
            opMode.telemetry.update();

            return returningDetection;
        } else {
            return -1;
        }
    }

    /**
     * Scans for a specified amount of time in seconds
     * @param aprilTagDetector
     * @return The ID of the detected Apriltag
     */
    public int scanAprilTagTime(AprilTagsVersion1 aprilTagDetector, int time) {
        ArrayList<AprilTagDetection> detections;
        int detectionTypeOne = 0;
        int detectionTypeTwo = 0;
        int detectionTypeThree = 0;
        int incorrectDetections = 0;
        int noDetections = 0;


        aprilTagScannerTime.reset();

        while (opMode.opModeIsActive() && aprilTagScannerTime.seconds() < time) { // May need to change based off of time
            detections = aprilTagDetector.getDetectionsUpdate();
            if (detections != null) {
                if (detections.size() > 0) {
                    for (AprilTagDetection detect : detections) {
                        int detectID = detect.id;

                        if (detectID == Constants.APRILTAG_ID_ONE) {
                            detectionTypeOne++;
                        } else if (detectID == Constants.APRILTAG_ID_TWO) {
                            detectionTypeTwo++;
                        } else if (detectID == Constants.APRILTAG_ID_THREE) {
                            detectionTypeThree++;
                        } else {
                            incorrectDetections++;
                        }
                    }
                } else {
                    noDetections++; //
                }
            }
        }

        // Code then decides which one to return
        int returningDetection;
        int totalDetects = detectionTypeOne + detectionTypeTwo + detectionTypeThree + incorrectDetections;
        double certainty;

        if (detectionTypeOne > detectionTypeTwo && detectionTypeOne > detectionTypeThree) {
            returningDetection = Constants.APRILTAG_ID_ONE;
            certainty = Math.round((double) detectionTypeOne / totalDetects) * 100;
        } else if (detectionTypeTwo > detectionTypeOne && detectionTypeTwo > detectionTypeThree) {
            returningDetection = Constants.APRILTAG_ID_TWO;
            certainty = Math.round((double) detectionTypeTwo / totalDetects) * 100;
        } else {
            returningDetection = Constants.APRILTAG_ID_THREE;
            certainty = Math.round((double) detectionTypeThree / totalDetects) * 100;
        }

        double error = Math.round((double) incorrectDetections / totalDetects) * 100;

        // Not needed. Like to have for testing
//            opMode.telemetry.addLine(String.format("Detection: %d", returningDetection));
//            opMode.telemetry.addLine(String.format("Certainty: %f", certainty));
//            opMode.telemetry.addLine(String.format("Error: %f", error));
//            opMode.telemetry.addLine(String.format("No scans: %d", noDetections));
//            opMode.telemetry.update();
        opMode.telemetry.addLine("Detection: " + returningDetection);
        opMode.telemetry.addLine("Certainty: " + certainty + "%");
        opMode.telemetry.addLine("Error: " + error + "%");
        opMode.telemetry.addLine("No scans: " + noDetections);
        opMode.telemetry.update();

        return returningDetection;
    }
}