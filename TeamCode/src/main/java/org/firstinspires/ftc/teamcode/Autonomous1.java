package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

@Disabled
@Autonomous
public class Autonomous1  extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        ForWhatWeHaveNow rep = new ForWhatWeHaveNow(this);
//        rep.startGyro();

        waitForStart();

        //Identify target

        rep.goForward(1, 12);
        rep.slide(1, 12);
        rep.goForward(-1, 12);
        rep.slide(-1, 12);
        //Slide right
        //Move forward
        //Slide left
        // Slide right
    }
}
