package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled

@TeleOp
public class ConeSleeveTest2 extends LinearOpMode {
    int position;
    int orange = 0;
    int pink = 0;
    int green = 0;
    int error = 0;

    @Override
    public void runOpMode() {
        OpenCVVersion4 cv = new OpenCVVersion4(this);
        TurtleBotCore bot = new TurtleBotCore(this);

        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {
                int pos = cv.iconPos();
                telemetry.addData("icon pos",pos);
                int pos2 = bot.coneColor(cv);
                int pos3 = bot.coneColorTimeBased(cv);
                telemetry.addData("cone color pos",pos2);
                telemetry.addData("time based pos",pos3);
                telemetry.update();

            }
        }
    }
}
