package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@Disabled
@TeleOp(name= "Lidar")
public class Liadar extends LinearOpMode {

    @Override
    public void runOpMode() throws InterruptedException {
        Servo liadar = hardwareMap.get(Servo.class, "liadarServo");
        DistanceSensor distance = hardwareMap.get(DistanceSensor.class, "distance");
        final int DELAY = 100;

        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {
                double dcm1;
                double positionLowest = Double.MAX_VALUE;
                double angleLowest = Double.MAX_VALUE;
                double distanceLowest = Double.MAX_VALUE;
//                liadar.setPosition(0);
//                sleep(10000);
//                liadar.setPosition(1);
//                sleep(10000);

                for (double i = 1; i >= 0; i += -0.02) {
                    liadar.setPosition(i);
                    sleep(DELAY);
                    dcm1 = distance.getDistance(DistanceUnit.CM);
                    telemetry.addData("Distance: ", dcm1);
                    telemetry.addData("Angle: ", i * 270 - 135);

                    if (dcm1 < distanceLowest) {
                        distanceLowest = dcm1;
                        angleLowest = i * 270 -135;
                        positionLowest = i;
                    }
                    telemetry.update();
                }
                for (double i = 0; i <= 1; i += 0.02) {
                    liadar.setPosition(i);
                    sleep(DELAY);
                    dcm1 = distance.getDistance(DistanceUnit.CM);
                    telemetry.addData("Distance: ", dcm1);
                    telemetry.addData("Angle: ", i * 270 - 135);
                    if (dcm1 < distanceLowest) {
                        distanceLowest = dcm1;
                        angleLowest = i * 270 -135;
                        positionLowest = i;
                    }
                    telemetry.update();
                }

                // Set position to closest element
                liadar.setPosition(positionLowest);
                telemetry.addData("Angle of Object: ", angleLowest);
                telemetry.addData("Lowest Distance: ", distanceLowest);
                telemetry.update();

                sleep(5000);

                //TODO:
                /*
                 * Optimize the sweeping radius of the lidar sensor to make it quicker
                 *
                 * Change the sweeping so that it only needs to sweep once to locate an object
                 *
                 * Create a class for using the lidar sensor so it is easily accessible through a common class
                 *
                 * Maybe utilize trigonometry to figure out the distance from the center of the screen to the object in pixels?
                 */


//                liadar.setPosition(-1);
//                sleep(100);
//
//                while (liadar.getPosition() != -1)
//                    sleep(1);
//
//                telemetry.addData("Distance", dcm1);
//                telemetry.update();

//                double dcm2 = distance.getDistance(DistanceUnit.CM);
//                sleep(100);



                //ethan is bad at Overwatch zzzzzzz

            }
        }
    }
}
