package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.roadrunner.trajectorysequence.TrajectorySequence;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled
@Autonomous
public class BasicLeftAutoSP extends LinearOpMode {

    enum Phase {
        SCAN,
        REPOSITION1,
        TO_POLE,
        SCORE,
        REPOSITION2,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int cycles = 3;
    int stackAugment = 840; // Augments the resting base position to reach cones on top of the stack
    int signalState = 1; // Uh I don't know what numbers are mapped to which signal and parking spot so please change

    boolean raising = false;
    boolean waiting = false;
    ElapsedTime timer = new ElapsedTime();

    Pose2d startPose = new Pose2d(-35, -64, Math.toRadians(90));


    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCore bot = new TurtleBotCore(this);
        OpenCVVersion4 cv = new OpenCVVersion4(this);
        // TODO: Uh I don't know what numbers are mapped to which signal and parking spot so please change
        int signalState = bot.coneColor(cv);

        drive.setPoseEstimate(startPose);

        Trajectory reposition1 = drive.trajectoryBuilder(startPose,false)
                .lineToLinearHeading(new Pose2d(-12,-60,Math.toRadians(90)))
                .build();

        Trajectory toFirstPole = drive.trajectoryBuilder(reposition1.end(),false)
                .lineToLinearHeading(new Pose2d(-9,-16,Math.toRadians(90)))
                .build();

//        Trajectory turn1 = drive.trajectoryBuilder(toFirstPole.end(),false)
//                .lineToLinearHeading(new Pose2d(-9.01,-16,Math.toRadians(135)))
//                .build();

        TrajectorySequence turn1 = drive.trajectorySequenceBuilder(toFirstPole.end())
                .turn(Math.toRadians(45))
                .build();

        TrajectorySequence turn2 = drive.trajectorySequenceBuilder(turn1.end())
                .turn(Math.toRadians(-45))
                .build();


//        Trajectory turn2 = drive.trajectoryBuilder(turn1.end(),false)
//                .lineToLinearHeading(new Pose2d(-9,-16,Math.toRadians(90)))
//                .build();

        Pose2d turnPose = new Pose2d(0,0,Math.toRadians(45));

        Trajectory reposition2 = drive.trajectoryBuilder(toFirstPole.end().plus(turnPose),false)
                .lineToLinearHeading(new Pose2d(-12,-60,Math.toRadians(90)))
                .build();

        Trajectory repositionToPark = drive.trajectoryBuilder(reposition2.end(),false)
                .lineToLinearHeading(startPose)
                .build();

        bot.claw(true);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        waitForStart();


        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMech(0,0);

            switch (currentPhase) {

                case SCAN:

                    currentPhase = Phase.REPOSITION1;
                    drive.followTrajectoryAsync(reposition1);


                    break;


                case REPOSITION1:

                    if (!drive.isBusy()) {
                        currentPhase = Phase.TO_POLE;
                        drive.followTrajectoryAsync(toFirstPole);
                        bot.extendArmAndSlide();
                    }
                    break;

                case TO_POLE:
                    if (!drive.isBusy() && !bot.shouldExtend && !waiting) {
                        waiting = true;
                        timer.reset();
                        drive.turnAsync(Math.toRadians(45));
                    }

                    if(waiting && timer.seconds() > 1 && !drive.isBusy()) {
                        currentPhase = Phase.SCORE;
                    }

                    break;

                case SCORE:
                    if(scored == 0) {
                        //drive.followTrajectorySequence(turn1);
                        bot.outtake();
                        scored++;
                        //drive.followTrajectorySequence(turn2);
                        //drive.turnAsync(Math.toRadians(-45));
                        bot.retractArmAndSlide();
                    }

                    if (scored == 1) {
                        currentPhase = Phase.REPOSITION2;
                        drive.followTrajectoryAsync(reposition2);


//                        if(signalState == 1) {
//                            drive.followTrajectoryAsync(park1A);
//                        }
//                        else if(signalState == 2) {
//                            drive.followTrajectoryAsync(park1B);
//                        }
//                        if(signalState == 3) {
//                            drive.followTrajectoryAsync(park1C);
//                        }
//                    }

//                    else {
//                        currentPhase = Phase.PARK;


                        //bot.retractArmAndSlide();
                    }

                    break;

                case REPOSITION2:
                    bot.retractArmAndSlide();
                    if(!drive.isBusy()) {
                        currentPhase = Phase.PARK;
                        drive.followTrajectoryAsync(repositionToPark);
                    }


                case PARK:
                    if(!drive.isBusy())
                        currentPhase = Phase.IDLE;
                    break;

                case IDLE:

                    break;

            }
        }











    }
}


