package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.vision.OpenCV;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled
@TeleOp
public class ConeSleeveTest extends LinearOpMode {
    int position;
    int orange = 0;
    int pink = 0;
    int green = 0;
    int error = 0;

    @Override
    public void runOpMode() {
        OpenCVVersion4 cv = new OpenCVVersion4(this);

        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {

                for (int i = 0; i < 100; i++) {
                    position = cv.iconPos();

                    if (position == 1) {
                        orange++;
                        //orange
                    } else if (position == 2) {
                        pink++;
                        //pink
                    } else if (position == 3) {
                        green++;
                        //green
                    } else {
                        error++;
                        //doesn't see a color
                    }
                }

                if (error > orange && error > green && error > pink) {
                    telemetry.addData("Highest Icon Position Number:", position);
                    telemetry.addLine("NOT SEEING ANYTHING");
                    //telemetry.update();
                } else if (orange > green && orange > pink) {
                    telemetry.addData("Highest Icon Position Number:", position);
                    telemetry.addLine("ORANGE");
                    //telemetry.update();
                } else if (green > orange && green > pink) {
                    telemetry.addData("Highest Icon Position Number:", position);
                    telemetry.addLine("GREEN");
                    //telemetry.update();
                } else if (pink > orange && pink > green) {
                    telemetry.addData("Highest Icon Position Number:", position);
                    telemetry.addLine("PINK");
                    //telemetry.update();
                }

                //telemetry.addData("Orange Count:", orange);
                //telemetry.addData("Pink Count:", pink);
                //telemetry.addData("Green Count:", green);
                //telemetry.addData("Error Count:", error);
                telemetry.update();

                sleep(5000);
            }
            //telemetry.addLine("OVER");
            //telemetry.update();
        }
    }
}
