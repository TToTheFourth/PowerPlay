package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;

@Disabled
@Autonomous
public class SlideRep extends LinearOpMode {

    @Override
    public void runOpMode() throws InterruptedException {
        ForWhatWeHaveNow bot = new ForWhatWeHaveNow(this);
        bot.startGyro();

        waitForStart();
        bot.slide(0.5, 30);
    }
}
