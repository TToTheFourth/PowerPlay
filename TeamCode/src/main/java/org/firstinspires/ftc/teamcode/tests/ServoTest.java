package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

@Disabled
@TeleOp
public class ServoTest extends LinearOpMode {

    private Servo servo;

    @Override
    public void runOpMode() {
        servo = hardwareMap.get(Servo.class, "liadarServo");

        waitForStart();

        while (opModeIsActive()) {
            if (gamepad1.a) {
                servo.setPosition(0.97);
            } else if (gamepad1.b) {
                servo.setPosition(0.33);
            }
            telemetry.addData("servo position: ", servo.getPosition());
            telemetry.update();
        }
    }
}