package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.vision.OpenCV;


@Disabled
public class OpenCVTest extends LinearOpMode {
    int position;

    @Override
    public void runOpMode() {
        OpenCV cam = new OpenCV(this);

        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {
                position = cam.iconPos();

                telemetry.addData("Icon Position Number:", position);
                telemetry.update();
            }
        }
    }
}
