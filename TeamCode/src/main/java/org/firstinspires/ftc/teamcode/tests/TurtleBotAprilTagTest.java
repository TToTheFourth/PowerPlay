package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;

@Disabled
@Autonomous
public class TurtleBotAprilTagTest extends LinearOpMode {
    TurtleBotCore bot;
    AprilTagsVersion1 aprilTag;

    @Override
    public void runOpMode() throws InterruptedException {
        bot = new TurtleBotCore(this);
        aprilTag = new AprilTagsVersion1(this);

        waitForStart();

        if (opModeIsActive()) {
            while(opModeIsActive()) {
                bot.scanAprilTagTime(aprilTag, 3);
            }
        }
    }
}
