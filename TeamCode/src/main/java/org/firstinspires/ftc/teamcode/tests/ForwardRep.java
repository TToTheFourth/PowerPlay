package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;

@Disabled
@Autonomous
public class ForwardRep extends LinearOpMode {

    @Override
    public void runOpMode() throws InterruptedException {
        ForWhatWeHaveNow bot = new ForWhatWeHaveNow(this);
        bot.startGyro();

        waitForStart();
        bot.goForward(0.5, 30);
    }
}
