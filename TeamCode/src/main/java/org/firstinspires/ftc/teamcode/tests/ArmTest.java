package org.firstinspires.ftc.teamcode.tests;

import static org.firstinspires.ftc.teamcode.Constants.MAXIMUM_EXTENSION;
import static org.firstinspires.ftc.teamcode.Constants.MINIMUM_RETRACTION;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

@Disabled
@TeleOp
public class ArmTest extends LinearOpMode {
    private DcMotor leftLinearSlide;
    private DcMotor rightLinearSlide;
    private DcMotor arm;

    private DcMotor backLeftMotor;
    private DcMotor frontLeftMotor;
    private DcMotor frontRightMotor;
    private DcMotor backRightMotor;

    private Servo grabber1;
    private Servo grabber2;

    private double integralSum;
    private double kP = 1.0;
    private double kI = 0;
    private double kD = 0;

    private double lastError;
    ElapsedTime timer = new ElapsedTime();

    @Override
    public void runOpMode() throws InterruptedException {
        //Linear Slides and arm
        leftLinearSlide = hardwareMap.get(DcMotor.class, "leftLinear");
        rightLinearSlide = hardwareMap.get(DcMotor.class, "rightLinear");
        arm = hardwareMap.get(DcMotor.class, "arm");

        // Drive train init
        backLeftMotor = hardwareMap.get(DcMotor.class, "motor0");
        frontLeftMotor = hardwareMap.get(DcMotor.class, "motor1");
        frontRightMotor = hardwareMap.get(DcMotor.class, "motor2");
        backRightMotor = hardwareMap.get(DcMotor.class, "motor3");


        arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftLinearSlide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinearSlide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftLinearSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightLinearSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftLinearSlide.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightLinearSlide.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);


        //Grabber init. Grabber 1 is the left and 2 on the right from intake side.
        grabber1 = hardwareMap.get(Servo.class, "grabber1");
        grabber2 = hardwareMap.get(Servo.class, "grabber2");

        double rightX_G1;
        double rightY_G1;
        double leftX_G1;


        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {

                rightY_G1 = -gamepad1.left_stick_y;
                rightX_G1 = -gamepad1.right_stick_x;
                leftX_G1 = -gamepad1.left_stick_x;

                double frontLeft = (rightX_G1 + rightY_G1 - leftX_G1);
                double backLeft = (rightX_G1 + rightY_G1 + leftX_G1);
                double backRight = (rightX_G1 - rightY_G1 + leftX_G1);
                double frontRight = (rightX_G1 - rightY_G1 - leftX_G1);

                frontLeftMotor.setPower(frontLeft);
                backLeftMotor.setPower(backLeft);
                backRightMotor.setPower(backRight);
                frontRightMotor.setPower(frontRight);

                float rightTriggerPower = gamepad1.right_trigger;
                float leftTriggerPower = gamepad1.left_trigger;

//                if (rightTriggerPower > 0) {
//                    leftLinearSlide.setPower(rightTriggerPower);
//                    rightLinearSlide.setPower(rightTriggerPower * -1);
//                } else if (leftTriggerPower > 0) {
//                    leftLinearSlide.setPower(leftTriggerPower * -1);
//                    rightLinearSlide.setPower(leftTriggerPower);
//                } else {
//                    leftLinearSlide.setPower(0);
//                    rightLinearSlide.setPower(0);
//                }

                if (gamepad1.dpad_up) {
                    arm.setPower(0.5);
                } else if (gamepad1.dpad_down) {
                    arm.setPower(-0.5);
                } else {
                    arm.setPower(0);
                }

                if (gamepad1.b) {
                    // open
                    grabber1.setPosition(0.6);
                    grabber2.setPosition(0.7);
                } else if (gamepad1.a) {
                    // close
                    grabber1.setPosition(0.75);
                    grabber2.setPosition(0.55);

                }

                int pos = leftLinearSlide.getCurrentPosition() * -1;
                telemetry.addData("Position: ", pos);
                telemetry.update();
                if (gamepad1.a) {
                    if (pos > MINIMUM_RETRACTION) {
                        leftLinearSlide.setPower(1);
                        rightLinearSlide.setPower(-1);


                    } else {
                        leftLinearSlide.setPower(0);
                        rightLinearSlide.setPower(0);
                    }
                } else if (gamepad1.y) {
                    double power = tickPID(MAXIMUM_EXTENSION, pos, 1);
                    leftLinearSlide.setPower(-power);
                    rightLinearSlide.setPower(power);
                } else {
                    leftLinearSlide.setPower(0);
                    rightLinearSlide.setPower(0);
                }

                /*
                if (gamepad1.b) {
                    if (pos < MAXIMUM_EXTENSION / 4) {
                        leftLinearSlide.setPower(-1);
                        rightLinearSlide.setPower(1);
                    } else if (pos > MAXIMUM_EXTENSION / 4) {
                        leftLinearSlide.setPower(1);
                        rightLinearSlide.setPower(-1);
                    } else {
                        leftLinearSlide.setPower(0);
                        rightLinearSlide.setPower(0);
                    }
                } else
                 */
            }
        }

        frontLeftMotor.setPower(0);
        backLeftMotor.setPower(0);
        frontRightMotor.setPower(0);
        backRightMotor.setPower(0);

    }

    public double PIDController(double goal, double state) {
        double error = goal - state;
        integralSum += error * timer.seconds();
        double derivative = (error - lastError) / timer.seconds();
        lastError = error;

        double output = (error * kP) + (integralSum * kI) + (derivative * kD);
        return output;
    }

    public double simplePIDController(double goal, double state) {
        double power = 1;
        if (state > goal - 200) {
            power = (state * (-1/200)) + 15;
        } else if (state >= goal) {
            power = 0;
        }

        return power;
    }

    public double tickPID(int goal, int state, double power) {
        double output;
        int difference = Math.abs(goal) - Math.abs(state);

        if (difference <= 300) {
            output = power * difference / 300;
        } else {
            output = power;
        }

        return output;
    }
}
