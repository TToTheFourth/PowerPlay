package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.TurtleBotCoreV2;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;

@Disabled
@Autonomous
public class RightAutoJustRRTrajectories extends LinearOpMode {

    enum Phase {
        SCAN,
        TO_POLE,
        SCORE,
        TO_STACK,
        INTAKE,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int cycles = 5;

    Pose2d startPose = new Pose2d(-35, -60, Math.toRadians(-90));



    @Override
    public void runOpMode() throws InterruptedException {



        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCoreV2 bot = new TurtleBotCoreV2(this);
        AprilTagsVersion1 tag = new AprilTagsVersion1(this);


        drive.setPoseEstimate(startPose);

        Trajectory toPoleFromStart = drive.trajectoryBuilder(startPose,true)
                .splineToSplineHeading(new Pose2d(-35,-20,Math.toRadians(-90)),Math.toRadians(90))
                .splineToSplineHeading(new Pose2d(-34,-8,Math.toRadians(-145)),  Math.toRadians(90))
                .build();

        Trajectory toStack = drive.trajectoryBuilder(toPoleFromStart.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-60,-12,Math.toRadians(180)),Math.toRadians(180))
                .build();

        Trajectory toPoleNormal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-34,-8,Math.toRadians(-145)),  Math.toRadians(30))
                .build();

        Trajectory toPoleFinal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-25,-12,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-10,-16,Math.toRadians(145)),  Math.toRadians(-30))
                .build();

        Trajectory park1 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-57,-12,Math.toRadians(180)),Math.toRadians(180))
                .build();

        Trajectory park2 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .lineToLinearHeading(new Pose2d(-34, -12, Math.toRadians(180)))
                .build();

        Trajectory park3 = drive.trajectoryBuilder(toPoleFinal.end(),false)
                .lineToLinearHeading(new Pose2d(-12, -14, Math.toRadians(180)))
                .build();



        int preSensed = bot.scanAprilTagTime(tag, 1);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        waitForStart();

        int sensed = bot.scanAprilTagTime(tag, 1);

        bot.extendSlides();

        while (opModeIsActive()) {
            drive.update();

            telemetry.addData("signal state", sensed);
            telemetry.update();


            switch (currentPhase) {

                case SCAN:

                    currentPhase = Phase.TO_POLE;
                    drive.followTrajectoryAsync(toPoleFromStart);

                    break;


                case TO_POLE:

                    if (!drive.isBusy()) {
                        currentPhase = Phase.SCORE;
                    }

                    break;

                case SCORE:

                    scored++;

                    if(scored == cycles + 1) {
                        currentPhase = Phase.PARK;
                        if(sensed == Constants.APRILTAG_ID_ONE) {
                            drive.followTrajectoryAsync(park1);
                        }

                        if(sensed == Constants.APRILTAG_ID_TWO) {
                            drive.followTrajectoryAsync(park2);
                        }

                        if(sensed == Constants.APRILTAG_ID_THREE) {
                            drive.followTrajectoryAsync(park3);
                        }

                    }

                    if(scored < cycles + 1) {
                        currentPhase = Phase.TO_STACK;
                        drive.followTrajectoryAsync(toStack);
                    }



                    break;


                case TO_STACK:

                    if(!drive.isBusy()) {
                        currentPhase = Phase.INTAKE;
                    }

                    break;

                case INTAKE:


                    currentPhase = Phase.TO_POLE;

                    if(scored == cycles && sensed == Constants.APRILTAG_ID_THREE) {
                        drive.followTrajectoryAsync(toPoleFinal);
                    }
                    else {
                        drive.followTrajectoryAsync(toPoleNormal);
                    }



                    break;


                case PARK:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                    }

                    break;

                case IDLE:

                    break;

            }
        }
    }
}