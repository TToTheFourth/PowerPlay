package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.subsystems.Arm;

@Disabled
@TeleOp
public class SubsystemTest extends LinearOpMode {

    @Override
    public void runOpMode() throws InterruptedException {
        Arm arm = new Arm(this);

        waitForStart();

        if (opModeIsActive()) {
            while(opModeIsActive()) {
                arm.runLinearSlides(1, Arm.Direction.UP);
                sleep(1000);
                arm.stopLinearSlides();
                sleep(5000);


            }
        }
    }
}
