package org.firstinspires.ftc.teamcode.tests;

import java.util.ArrayList;

public class SweepData {
    public double[] position;
    public double[] angle;
    public double[] distance;

    public void setData(ArrayList<Double> angleArr, ArrayList<Double> positionArr, ArrayList<Double> distanceArr) {
        angle = new double[angleArr.size()];
        for(int i = 0; i < angleArr.size(); i++) {
            angle[i] = angleArr.get(i);
        }

        position = new double[positionArr.size()];
        for(int i = 0; i < positionArr.size(); i++) {
            position[i] = positionArr.get(i);
        }

        distance = new double[distanceArr.size()];
        for(int i = 0; i < distanceArr.size(); i++) {
            distance[i] = distanceArr.get(i);
        }
    }

    @Override
    public SweepData clone() {
        SweepData c = new SweepData();

        c.position = new double[this.position.length];
        System.arraycopy(this.position, 0, c.position, 0, c.position.length);

        c.angle = new double[this.angle.length];
        System.arraycopy(this.angle, 0, c.angle, 0, c.angle.length);

        c.distance = new double[this.distance.length];
        System.arraycopy(this.distance, 0, c.distance, 0, c.distance.length);

        return c;
    }
}
