package org.firstinspires.ftc.teamcode;

import com.acmerobotics.roadrunner.control.PIDCoefficients;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;

import org.opencv.core.Scalar;

public final class Constants {
    public static final int CAMERA_WIDTH = 640;
    public static final int CAMERA_HEIGHT = 480;

    //Scalar constants for colors
    public static final Scalar RED = new Scalar(255, 0, 0);
    public static final Scalar GREEN = new Scalar(0, 255, 0);
    public static final Scalar BLUE = new Scalar(0, 0, 255);

    /**
     * Workshop:
     * Low: new Scalar (17,163,85)
     * High: new Scalar (30,255,252)
     *
     * Workshop 2:
     * Low: new Scalar (0,67,63)
     * High: new Scalar (28,184,243)
     */
    public static final Scalar YELLOW_LOW = new Scalar (0,67,63);
    public static final Scalar YELLOW_HIGH = new Scalar (28,184,243);

    //Delay in ms
    public static final int LIDAR_DELAY = 100;

    //Measured in ticks
    public static final int MINIMUM_RETRACTION = 0;
    public static final int MAXIMUM_EXTENSION = 3925;

    /*
     * Data Table values:
     * 1.) -3925
     * 2.) -3911
     * 3.) -3938
     *
     * Avg: -3925
     *
     * High Pole: -3215
     *
     * Middle Pole: -1601
     *
     * Low Pole: 0
     */

    public static int LOW_POLE = 0;
    public static int MID_POLE = 1751;
    public static int HIGH_POLE = 3365;

    public static final int ARM_HIGH = 1436; //615 original power
    public static final int ARM_LOW = -150;
    public static final int ARM_AHEAD = 500;

    public static final int ARM_EXTENDED = -1300; // - 1250 old position
    public static final int ARM_RESTING = 150;
    public static final int ARM_RETRACTED = 300;

    public static final double grabber1Closed = 0.60;
    public static final double grabber2Closed = 0.85;

    public static final double grabber1Open = 0.85;
    public static final double grabber2Open = 0.50;

    public static final double grabber1OpenIntaking = 0.85;
    public static final double grabber2OpenIntaking = 0.50;

    public static final double grabber1OpenSlidesUp = 0.69;
    public static final double grabber2OpenSlidesUp = 0.65;

    public static final int CENTER_CAMERA = (CAMERA_WIDTH * 3)/4;

    public static final int APRILTAG_ID_ONE = 14;
    public static final int APRILTAG_ID_TWO = 55;
    public static final int APRILTAG_ID_THREE = 5;

    public static final PIDCoefficients SLIDE_COEFFICIENTS = new PIDCoefficients(0,0,0);
    public static final PIDCoefficients ARM_COEFFICIENTS = new PIDCoefficients(0,0,0);

    public static final double SPOOL_RADIUS = 0.0; // Inches

    public static final double SPOOL_MOTOR_TICS_PER_REV = 0;
    public static final double ARM_MOTOR_TICS_PER_REV = 0;
    public static final double STARTING_ANGLE = 0;

    public static final double SLIDE_GRAVITY = 0.0; // Acceleration of gravity in Inches/Sec^2
    public static final double ARM_GRAVITY = 0.0; // Angular acceleration? In/Sec^2

    public static final double STACK_AUGMENT = 1.2; // The additional height each cone on the stack adds (in inches)
}
