package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.TurtleBotCoreV2;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;

@Disabled
@Autonomous
public class LeftAutoJustRRV5 extends LinearOpMode {

    enum Phase {
        SCAN,
        TO_POLE,
        SCORE,
        TO_STACK,
        INTAKE,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int cycles = 1;
    int dunk = 0;

    Pose2d startPose = new Pose2d(-35, -60, Math.toRadians(-90));



    @Override
    public void runOpMode() throws InterruptedException {



        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCore bot = new TurtleBotCore(this);
        AprilTagsVersion1 tag = new AprilTagsVersion1(this);


        drive.setPoseEstimate(startPose);

        Trajectory toPoleFromStart = drive.trajectoryBuilder(startPose,true)
                .splineToSplineHeading(new Pose2d(-35,-20,Math.toRadians(-90)),Math.toRadians(90))
                .splineToSplineHeading(new Pose2d(-34,-8,Math.toRadians(-145)),  Math.toRadians(90))
                .addTemporalMarker(1, () -> bot.extendArmAndSlide())
                .build();

        Trajectory toStack = drive.trajectoryBuilder(toPoleFromStart.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-60,-12,Math.toRadians(180)),Math.toRadians(180))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .addTemporalMarker(0.5, () -> bot.claw(true))
                .addTemporalMarker(0.5, () -> dunk = 0)
                .build();

        Trajectory toPoleNormal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-34,-8,Math.toRadians(-145)),  Math.toRadians(30))
                .addTemporalMarker(1, () -> bot.extendArmAndSlide())
                .addSpatialMarker(new Vector2d(-34,-8), () -> dunk = -300)
                .build();

        Trajectory toPoleFinal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-25,-12,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-10,-16,Math.toRadians(145)),  Math.toRadians(-30))
                .addTemporalMarker(1, () -> bot.extendArmAndSlide())
                .addSpatialMarker(new Vector2d(-10,-16), () -> dunk = -300)
                .build();

        Trajectory park1 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-57,-12,Math.toRadians(180)),Math.toRadians(180))
                .addTemporalMarker(0.5, () -> bot.claw(true))
                .addTemporalMarker(0.5, () -> dunk = 0)

                .build();

        Trajectory park2 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .lineToLinearHeading(new Pose2d(-34, -12, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.claw(true))
                .addTemporalMarker(0.5, () -> dunk = 0)
                .build();

        Trajectory park3 = drive.trajectoryBuilder(toPoleFinal.end(),false)
                .lineToLinearHeading(new Pose2d(-12, -14, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.claw(true))
                .addTemporalMarker(0.5, () -> dunk = 0)
                .build();



        int preSensed = bot.scanAprilTagTime(tag, 1);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        bot.claw(true);

        waitForStart();

        int sensed = bot.scanAprilTagTime(tag, 1);

        bot.claw(true);



        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMech(200 - (scored * 40), dunk);

            telemetry.addData("signal state", sensed);
            telemetry.update();


            switch (currentPhase) {

                case SCAN:

                    currentPhase = Phase.TO_POLE;
                    drive.followTrajectoryAsync(toPoleFromStart);

                    break;


                case TO_POLE:

                    if (!drive.isBusy()) {
                        currentPhase = Phase.SCORE;
                    }

                    break;

                case SCORE:

                    scored++;
                    bot.outtake();

                    if(scored == cycles + 1) {
                        currentPhase = Phase.PARK;
                        if(sensed == Constants.APRILTAG_ID_ONE) {
                            drive.followTrajectoryAsync(park1);
                        }

                        if(sensed == Constants.APRILTAG_ID_TWO) {
                            drive.followTrajectoryAsync(park2);
                        }

                        if(sensed == Constants.APRILTAG_ID_THREE) {
                            drive.followTrajectoryAsync(park3);
                        }

                    }

                    if(scored < cycles + 1) {
                        currentPhase = Phase.TO_STACK;
                        drive.followTrajectoryAsync(toStack);
                    }



                    break;


                case TO_STACK:

                    if(!drive.isBusy()) {
                        currentPhase = Phase.INTAKE;
                    }

                    break;

                case INTAKE:

                    bot.claw(true);

                    currentPhase = Phase.TO_POLE;

                    if(scored == cycles && sensed == Constants.APRILTAG_ID_THREE) {
                        drive.followTrajectoryAsync(toPoleFinal);
                    }
                    else {
                        drive.followTrajectoryAsync(toPoleNormal);
                    }



                    break;


                case PARK:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                        bot.retractArmAndSlide();
                    }

                    break;

                case IDLE:

                    bot.updateScoringMech(0,0);



                    break;

            }
        }
    }
}