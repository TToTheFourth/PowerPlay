package org.firstinspires.ftc.teamcode.autos;

import static org.firstinspires.ftc.teamcode.Constants.HIGH_POLE;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;
import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;
import org.openftc.apriltag.AprilTagDetection;

import java.util.ArrayList;

@Disabled
@Autonomous
public class AprilTagTest extends LinearOpMode {
    TurtleBotCore bot;
    AprilTagsVersion1 april;
    ForWhatWeHaveNow rep;

    @Override
    public void runOpMode() throws InterruptedException {
        bot = new TurtleBotCore(this);
        april = new AprilTagsVersion1(this);
        rep = new ForWhatWeHaveNow(this);

        waitForStart();

//        if (opModeIsActive()) {
//        int scanned = bot.scanAprilTag(april);
            //int scanned = bot.scanAprilTagTime(april, 5);
//                telemetry.addData("Scanned: ", scanned);
            //telemetry.update();
//        }

        while (opModeIsActive()) {
            //doStats(april);
            botCore(bot, april);
        }
    }

    public void botCore(TurtleBotCore bot, AprilTagsVersion1 aprilTagDetector) {
        bot.scanAprilTagTime(aprilTagDetector, 3);
    }

    public int doStats(AprilTagsVersion1 aprilTagDetector) {

        int ones = 0;
        int twos = 0;
        int threes = 0;
        int dcount = 0;

        ElapsedTime timer = new ElapsedTime();
        timer.reset();

        while(opModeIsActive() && dcount < 50 && timer.seconds() < 5) {
            // Note getDetectionsUpdate returns null if no new frames have been
            // processed since the last get
            ArrayList<AprilTagDetection> detections = aprilTagDetector.getDetectionsUpdate();

            if (detections != null && detections.size() > 0) {
                // Only increment the count if a new frame was processed
                dcount++;

                // Assume only one tag is seen - if more than one then there is a problem
                AprilTagDetection detect = detections.get(0);
                int detectID = detect.id;

                if (detectID == Constants.APRILTAG_ID_ONE) {
                    ones++;
                } else if (detectID == Constants.APRILTAG_ID_TWO) {
                    twos++;
                } else if (detectID == 5) { // need to update APRILTAG_ID_THREE to be 5
                    threes++;
                }
            }
        }

        // Which tag and associated position was seen the most?
        int best = 3;
        if(ones > twos && ones > threes) {
            best = 1;
        } else if (twos > ones && twos > threes) {
            best = 2;
        }

        // optional telemetry
        telemetry.addData("Count", dcount);
        telemetry.addData("One", ones);
        telemetry.addData("Two", twos);
        telemetry.addData("Three", threes);
        telemetry.addData("Best", best);
        telemetry.update();

        // return the most seen position
        return best;
    }


    /*
    public int process(AprilTagsVersion1 aprilTagDetector) {
        ArrayList<AprilTagDetection> detections = aprilTagDetector.getLatestDetections();

        int detectID = -1;
        String size = "";
        int pos = -1;

        if (detections == null) {
            size = "Null";
        } else if (detections.size() == 0) {
            size = "0";
        } else {
            size = "" + detections.size();
            AprilTagDetection detect = detections.get(0);
            detectID = detect.id;

            if (detectID == Constants.APRILTAG_ID_ONE) {
                pos = 1;
            } else if (detectID == Constants.APRILTAG_ID_TWO) {
                pos = 2;
            } else if (detectID == 5) {
                pos = 3;
            }
        }

        telemetry.addData("Size", size);
        telemetry.addData("Detection", detectID);
        telemetry.addData("Position", pos);
        telemetry.update();

        return pos;
    }
     */

}
