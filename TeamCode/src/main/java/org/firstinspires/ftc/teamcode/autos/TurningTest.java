package org.firstinspires.ftc.teamcode.autos;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;

@Disabled
@Autonomous
public class TurningTest extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        ForWhatWeHaveNow rep = new ForWhatWeHaveNow(this);
        rep.startGyro();

        waitForStart();
        rep.turnRight(45,0.5);
    }
}
