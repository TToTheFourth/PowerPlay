package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled
@Autonomous
public class LeftAutoRRV4 extends LinearOpMode {
    public static final int CAMERA_WIDTH = 640;
    public static final int CAMERA_HEIGHT = 480;

    enum Phase {
        SCAN,
        REPOSITION1,
        REPOSITION2,
        TO_POLE,
        SENSING,
        SCORE,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int signalState = 1; // Uh I don't know what numbers are mapped to which signal and parking spot so please change

    boolean raising = false;
    boolean waiting = false;
    ElapsedTime timer = new ElapsedTime();

    Pose2d startPose = new Pose2d(-35, -64, Math.toRadians(90));



    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCore bot = new TurtleBotCore(this);
        AprilTagsVersion1 april = new AprilTagsVersion1(this);
        // TODO: Uh I don't know what numbers are mapped to which signal and parking spot so please change


        drive.setPoseEstimate(startPose);

        Trajectory reposition1 = drive.trajectoryBuilder(startPose, false)
                .lineToLinearHeading(new Pose2d(-12, -60, Math.toRadians(90)))
                .build();

        Trajectory reposition2 = drive.trajectoryBuilder(reposition1.end(), false)
                .lineToLinearHeading(new Pose2d(-12, -16, Math.toRadians(90)))
                .build();

        Trajectory toFirstPole = drive.trajectoryBuilder(reposition2.end().plus(new Pose2d(0, 0, Math.toRadians(45))), false)
                .lineToLinearHeading(new Pose2d(-8, -17, Math.toRadians(135)))
                .build();

        Trajectory park1A = drive.trajectoryBuilder(toFirstPole.end(), false)
                .lineToLinearHeading(new Pose2d(-12, -16, Math.toRadians(180)))
                .addTemporalMarker(1.5, () -> bot.retractArmAndSlide())
                .build();
/**
 Trajectory park2A = drive.trajectoryBuilder(park1A.end(),false)
 .strafeLeft(24)
 .build();
 */
        Trajectory park1B = drive.trajectoryBuilder(toFirstPole.end(), false)
                .lineToLinearHeading(new Pose2d(-36, -16, Math.toRadians(180)))
                .addTemporalMarker(1.5, () -> bot.retractArmAndSlide())
                .build();
/**
 Trajectory park2B = drive.trajectoryBuilder(park1B.end(),false)
 .strafeLeft(24)
 .build();
 */
        Trajectory park1C = drive.trajectoryBuilder(toFirstPole.end(), false)
                .lineToLinearHeading(new Pose2d(-58, -16, Math.toRadians(180)))
                .addTemporalMarker(1.5, () -> bot.retractArmAndSlide())
                .build();
/**
 Trajectory park2C = drive.trajectoryBuilder(park1C.end(),false)
 .strafeLeft(24)
 .build();
 */

        bot.claw(true);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        waitForStart();

        if (opModeIsActive()) {
            signalState = bot.scanAprilTagTime(april, 3);
            while (opModeIsActive()) {
                drive.update();

                telemetry.addData("signal state", signalState);
                telemetry.update();


                switch (currentPhase) {

                    case SCAN:

                        currentPhase = Phase.REPOSITION1;
                        drive.followTrajectoryAsync(reposition1);

                        break;

                    case REPOSITION1:

                        if (!drive.isBusy()) {
                            currentPhase = Phase.REPOSITION2;
                            drive.followTrajectoryAsync(reposition2);
                        }

                        break;


                    case REPOSITION2:

                        if (!drive.isBusy()) {
                            currentPhase = Phase.TO_POLE;
                            drive.turnAsync(Math.toRadians(45));
                            bot.extendArmAndSlide();
                        }
                        break;


                    case TO_POLE:
                        if (!drive.isBusy() && !bot.shouldExtend && !waiting) {
                            drive.followTrajectoryAsync(toFirstPole);
                            waiting = true;

                        }

                        if (waiting && !drive.isBusy()) {
                            waiting = false;
                            currentPhase = Phase.SENSING;
                        }

                        break;

                    case SENSING:
//                    rx: x position of the middle of the rectangle
//                    mx: the center of the screen
//                    d: short distance measured by lidar
//                    tgt: target distance
//                    public void poleCenter(double rx, double mx, double d, double tgt) {
//                    if (rx > mx) {
//                        //slide right
//                    } else if (mx > rx) {
//                        //slide left
//                    } else if (Math.abs(rx - mx) < 2.5) {
//                        if (d > tgt) {
//                            //move forward
//                        } else if (d < tgt) {
//                            //move backwards
//                        } else if (d > 2.5 || d < 2.5) {
//                            //drop
//                        }
//                    }
//                }


                        break;

                    case SCORE:
                        if (!waiting) {

                            bot.outtake();
                            waiting = true;
                            timer.reset();
                            //drive.turnAsync(Math.toRadians(45));
                        } else if (waiting && !drive.isBusy() && timer.seconds() > 0.1) {

                            waiting = false;
                            currentPhase = Phase.PARK;
                            if (signalState == Constants.APRILTAG_ID_THREE) {
                                drive.followTrajectoryAsync(park1A);
                            } else if (signalState == Constants.APRILTAG_ID_TWO) {
                                drive.followTrajectoryAsync(park1B);
                            }
                            if (signalState == Constants.APRILTAG_ID_ONE) {
                                drive.followTrajectoryAsync(park1C);
                            }
                            //bot.retractArmAndSlide();
                        }

                        break;


                    case PARK:
                        if (!drive.isBusy()) {
                            currentPhase = Phase.IDLE;

                             //if(signalState == 1) {
                             //drive.followTrajectoryAsync(park2A);
                             //}
                             //else if(signalState == 2) {
                             //drive.followTrajectoryAsync(park2B);
                             //}
                             //if(signalState == 3) {
                             //drive.followTrajectoryAsync(park2C);
                             //}
                        }

                        break;

// case PARK2:
// if (!drive.isBusy()) {
// currentPhase = Phase.IDLE;
// }
// break;
                    case IDLE:

                        break;

                }

            }
        }
    }
}