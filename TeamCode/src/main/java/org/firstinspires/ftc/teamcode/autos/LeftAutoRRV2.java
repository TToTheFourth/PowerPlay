package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled
@Autonomous
public class LeftAutoRRV2 extends LinearOpMode {

    enum Phase {
        SCAN,
        REPOSITION1,
        TO_POLE,
        SCORE,
        INTAKE,
        TO_STACK,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int cycles = 0;
    int stackAugment = 840; // Augments the resting base position to reach cones on top of the stack
    int signalState = 1; // Uh I don't know what numbers are mapped to which signal and parking spot so please change

    boolean raising = false;
    boolean waiting = false;
    ElapsedTime timer = new ElapsedTime();

    Pose2d startPose = new Pose2d(-35, -64, Math.toRadians(90));


    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCore bot = new TurtleBotCore(this);
        OpenCVVersion4 cv = new OpenCVVersion4(this);
        // TODO: Uh I don't know what numbers are mapped to which signal and parking spot so please change
        int signalState = bot.coneColor(cv);

        drive.setPoseEstimate(startPose);

        Trajectory reposition1 = drive.trajectoryBuilder(startPose,false)
                .lineToLinearHeading(new Pose2d(-12,-60,Math.toRadians(90)))
                .build();

        Trajectory toFirstPole = drive.trajectoryBuilder(reposition1.end(),false)
                .lineToLinearHeading(new Pose2d(-9,-16,Math.toRadians(90)))
                .build();

        Trajectory repositionToStack = drive.trajectoryBuilder(toFirstPole.end().plus(new Pose2d(0,0,Math.toRadians(90))),false)
                .lineToLinearHeading(new Pose2d(-57,-12,Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .build();

        Trajectory toSecondPole = drive.trajectoryBuilder(repositionToStack.end(), true)
                .lineToLinearHeading(new Pose2d(-35,-12,Math.toRadians(180)))
                .build();

        Trajectory toStack = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(-61, -12, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .build();

        Trajectory park1A = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(-10, -12, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .build();
/**
        Trajectory park2A = drive.trajectoryBuilder(park1A.end(),false)
                .strafeLeft(24)
                .build();
*/
        Trajectory park1B = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(-34, -12, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .build();
/**
        Trajectory park2B = drive.trajectoryBuilder(park1B.end(),false)
                .strafeLeft(24)
                .build();
*/
        Trajectory park1C = drive.trajectoryBuilder(toSecondPole.end(),false)
                .lineToLinearHeading(new Pose2d(-58, -12, Math.toRadians(180)))
                .addTemporalMarker(0.5, () -> bot.retractArmAndSlide())
                .build();
/**
        Trajectory park2C = drive.trajectoryBuilder(park1C.end(),false)
                .strafeLeft(24)
                .build();
*/

        bot.claw(true);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        waitForStart();

        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMech(0,0);


            switch (currentPhase) {

                case SCAN:

                    currentPhase = Phase.REPOSITION1;
                    drive.followTrajectoryAsync(reposition1);

                    break;


                case REPOSITION1:

                    if (!drive.isBusy()) {
                        currentPhase = Phase.TO_POLE;
                        drive.followTrajectoryAsync(toFirstPole);
                        bot.extendArmAndSlide();
                    }
                    break;

                case TO_POLE:
                    if (!drive.isBusy() && !bot.shouldExtend && !waiting) {
                        waiting = true;
                        drive.turnAsync(Math.toRadians(45));
                    }

                    if(waiting && !drive.isBusy()) {
                        waiting = false;
                        currentPhase = Phase.SCORE;
                    }

                    break;

                case SCORE:
                    if(!waiting) {
                        bot.outtake();
                        scored++;
                    }

                    if (scored == 1 && scored != cycles && !waiting) {
                        waiting = true;
                        drive.turnAsync(Math.toRadians(45));
                        //bot.retractArmAndSlide();
                    }

                    else if(scored == 1 && scored != cycles && waiting && !drive.isBusy()) {
                        waiting = false;
                        currentPhase = Phase.TO_STACK;
                        drive.followTrajectoryAsync(repositionToStack);
                    }

                    else if (scored != cycles && !waiting) {
                        waiting = true;
                        drive.turnAsync(Math.toRadians(-45));
                        //bot.retractArmAndSlide();
                    }

                    else if (scored !=cycles && waiting && !drive.isBusy()) {
                        waiting = false;
                        stackAugment = stackAugment - 175;
                        currentPhase = Phase.TO_STACK;
                        drive.followTrajectoryAsync(toStack);
                    }

                    else if (scored == cycles && !waiting) {
                        waiting = true;
                        drive.turnAsync(Math.toRadians(-45));
                    }

                    else if (scored == cycles && waiting && !drive.isBusy()) {
                        waiting = false;
                        currentPhase = Phase.PARK;
                        if(signalState == 1) {
                            drive.followTrajectoryAsync(park1A);
                        }
                        else if(signalState == 2) {
                            drive.followTrajectoryAsync(park1B);
                        }
                        if(signalState == 3) {
                            drive.followTrajectoryAsync(park1C);
                        }
                        //bot.retractArmAndSlide();
                    }

                    break;


                case TO_STACK:

                    if (!drive.isBusy() && !waiting) {
                        waiting = true;
                        timer.reset();
                    }

                    if(waiting && timer.seconds() > 1) {
                        currentPhase = Phase.INTAKE;
                    }

                case INTAKE:

                    drive.setWeightedDrivePower(new Pose2d(0.3,0,0));

                    if (bot.coneLoaded && !raising) {
                        raising = true;
                        bot.extendArmAndSlide();
                        timer.reset();
                    }

                    if (raising && timer.seconds() > 0.5) {
                        currentPhase = Phase.TO_POLE;
                        drive.followTrajectoryAsync(toSecondPole);
                        raising = false;
                    }


                    break;





                case PARK:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                        /**
                        if(signalState == 1) {
                            drive.followTrajectoryAsync(park2A);
                        }
                        else if(signalState == 2) {
                            drive.followTrajectoryAsync(park2B);
                        }
                        if(signalState == 3) {
                            drive.followTrajectoryAsync(park2C);
                        } */
                    }
                    break;
/**
                case PARK2:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                    }
                    break;
*/
                case IDLE:

                    break;

            }
        }











    }
}


