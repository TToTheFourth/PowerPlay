package org.firstinspires.ftc.teamcode.tests;

import static org.firstinspires.ftc.teamcode.Constants.CAMERA_HEIGHT;
import static org.firstinspires.ftc.teamcode.Constants.CAMERA_WIDTH;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;
import org.openftc.apriltag.AprilTagDetection;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;

import java.util.ArrayList;

@TeleOp
@Disabled
public class AprilTagsTest extends LinearOpMode
{
    OpenCvCamera camera;
    AprilTagsVersion1 aprilTagDetector;
    ArrayList<AprilTagDetection> detections;

    @Override
    public void runOpMode()
    {
        aprilTagDetector = new AprilTagsVersion1(this);
        camera = aprilTagDetector.getCamera();
        int noDetection = 0;
        int detection = 0;

        waitForStart();

        telemetry.setMsTransmissionInterval(50);

        while (opModeIsActive())
        {
            detections = aprilTagDetector.getDetectionsUpdate();

            // If there's been a new frame...
            if(detections != null)
            {
                if(detections.size() == 0)
                {
                    noDetection++;
                }
                else
                {
                    detection++;
                }
            }

            sleep(20);
        }
    }
}