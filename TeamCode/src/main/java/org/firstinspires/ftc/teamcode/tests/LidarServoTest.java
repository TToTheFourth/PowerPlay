package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

@Disabled
@TeleOp
public class LidarServoTest extends LinearOpMode {
    private Servo servo;

    @Override
    public void runOpMode() {
        servo = hardwareMap.get(Servo.class, "liadarServo");
        servo.scaleRange(0.18, 0.7);

        waitForStart();

        while (opModeIsActive()) {
            servo.setPosition(1);
            telemetry.addData("Servo position: ", servo.getPosition());
            telemetry.update();
            sleep(4000);
            servo.setPosition(0);
            sleep(4000);
        }
    }
}