package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.TurtleBotCore;

@Disabled
@TeleOp
public class RetractTest extends LinearOpMode {


    TurtleBotCore bot;
    int dunk = 0;
    int augment = 0;

    @Override
    public void runOpMode() throws InterruptedException {
        waitForStart();

        bot = new TurtleBotCore(this);

        if (opModeIsActive()) {
            while (opModeIsActive()) {

                telemetry.addData("level", bot.getLevel());
                telemetry.addData("power", bot.getSlidePower());
                telemetry.update();

                bot.updateScoringMech(augment,dunk);

                if(gamepad1.x) {
                   augment = 500;
                }
                else {
                    augment = 0;
                }

                if(gamepad1.y) {
                    dunk = 300;
                }
                else {
                    dunk = 0;
                }

                if(gamepad1.a) {
                    bot.extendArmAndSlide();
                }

                if(gamepad1.b) {
                    bot.retractArmAndSlide();
                }



            }
        }
    }
}
