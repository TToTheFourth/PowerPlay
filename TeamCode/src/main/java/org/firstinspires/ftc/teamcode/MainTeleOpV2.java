package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Constants.ARM_HIGH;
import static org.firstinspires.ftc.teamcode.Constants.ARM_LOW;
import static org.firstinspires.ftc.teamcode.Constants.HIGH_POLE;
import static org.firstinspires.ftc.teamcode.Constants.LOW_POLE;
import static org.firstinspires.ftc.teamcode.Constants.MID_POLE;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Open;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Open;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.tests.LiadarTestCH;

@TeleOp
public class MainTeleOpV2 extends LinearOpMode {




    enum CONTROL_MODE {
        Manual,
        Automated
    }

    CONTROL_MODE controlMode = CONTROL_MODE.Automated;
    boolean previousX2 = false;
    boolean toggleX2 = false;
    boolean bumperInputReceived = true;
    boolean dpadInputReceived = true;
    int speedMultiplier = 1;

    @Override
    public void runOpMode() throws InterruptedException {

        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCoreV3 bot = new TurtleBotCoreV3(this);

        waitForStart();

        while (opModeIsActive()) {

            if(gamepad2.x && !previousX2) {
                toggleX2 = !toggleX2;
            }
            previousX2 = gamepad2.x;

            if(toggleX2) {
                controlMode = CONTROL_MODE.Manual;
            }
            else {
                controlMode = CONTROL_MODE.Automated;
            }

            switch (controlMode) {
                case Manual:

                    if(gamepad2.left_bumper) {
                        bot.closeClaw();
                    }

                    if(gamepad2.right_bumper) {
                        bot.openClaw();
                    }

                    double armPower = gamepad2.right_stick_y;
                    double slidePower = gamepad2.left_stick_y;
                    bot.updateScoringMechManual(armPower, slidePower);


                    break;

                case Automated:

                    bot.setDunk((gamepad1.right_trigger * 400) - (gamepad1.left_trigger * 400));


                    if (gamepad1.right_bumper && bumperInputReceived) {
                        bot.raiseStackAugmentOneLevel();
                        bumperInputReceived = false;
                    }

                    if (gamepad1.left_bumper && bumperInputReceived) {
                        bot.lowerStackAugmentOneLevel();
                        bumperInputReceived = false;
                    }

                    if (!gamepad1.left_bumper && !gamepad1.right_bumper) {
                        bumperInputReceived = true;
                    }

                    if (gamepad1.y) {
                        bot.setStackAugmentFive();
                    }

                    if (gamepad1.x) {
                        bot.setStackAugmentZero();
                    }


                    if (gamepad2.dpad_up && dpadInputReceived) {
                        bot.extendSlideOneLevel();
                        dpadInputReceived = false;
                    }

                    if (gamepad2.dpad_down && dpadInputReceived) {
                        bot.retractSlideOneLevel();
                        dpadInputReceived = false;
                    }

                    if (!gamepad2.dpad_up && !gamepad2.dpad_down) {
                        dpadInputReceived = true;
                    }

                    if (gamepad2.b) {
                        bot.extendArm();
                    }

                    if(gamepad2.y) {
                        bot.retractArm();
                    }

                    if(gamepad2.left_bumper) {
                        bot.closeClaw();
                    }

                    if(gamepad2.right_bumper) {
                        bot.openClaw();
                    }

                    if(gamepad2.a) {
                        bot.extendArmAndSlide(3);
                    }

                    if(gamepad2.right_trigger > 0.1) {
                        bot.retractArmAndSlide();
                    }


                    bot.updateScoringMechAutomated(1,1);




                    break;
            }


            Pose2d drivePower = new Pose2d(0,0,0);
            if(gamepad1.dpad_left) {
                drivePower = new Pose2d(0,0.35,-0.2 * gamepad1.right_stick_x);
            }
            else if(gamepad1.dpad_right) {
                drivePower = new Pose2d(0,-0.35,-0.2 * gamepad1.right_stick_x);
            }
            else if(gamepad1.dpad_up) {
                drivePower = new Pose2d(0.35,0,-0.2 * gamepad1.right_stick_x);
            }
            else if (gamepad1.dpad_down) {
                drivePower = new Pose2d(-0.35,0,-0.2 * gamepad1.right_stick_x);
            }
            else {
                drivePower = new Pose2d(
                        -gamepad1.left_stick_y * speedMultiplier,
                        -gamepad1.left_stick_x * speedMultiplier,
                        -gamepad1.right_stick_x * speedMultiplier
                );
            }

            drive.setWeightedDrivePower(drivePower);
            drive.update();


        }

        bot.setIdle();
        drive.setWeightedDrivePower(new Pose2d(0,0,0));
        drive.update();

    }

}