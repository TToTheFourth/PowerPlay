package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import java.util.ArrayList;


public class LiadarTestActual implements Runnable {
    final int DELAY = 100;

    Servo liadar;
    DistanceSensor distance;

    double dcm1;
    double positionLowest = Double.MAX_VALUE;
    double angleLowest = Double.MAX_VALUE;
    double distanceLowest = Double.MAX_VALUE;
    boolean doScan = true;
    double lowLidar = 0.18;
    double highLidar = 0.6;
    double stepSize = 0.02;
    boolean sweepComplete = true;
    SweepData sweepData;
    Object sweepDataLock;

    public LiadarTestActual(Servo l, DistanceSensor d) {
        liadar = l;
        distance = d;
        sweepData = new SweepData();
        sweepDataLock = new Object();

    }

    public void stop() {
        doScan = false;
    }
    public void scan() throws InterruptedException {
        doScan = true;
        ArrayList<Double> angleArr = new ArrayList<>();
        ArrayList<Double> positionArr = new ArrayList<>();
        ArrayList<Double> distanceArr = new ArrayList<>();

        while (doScan) {
            sweepComplete = true;
            double nextLowest = Double.MAX_VALUE;
            double nextAngle = 0.0;
            double nextPos = 0.0;
            angleArr.clear();
            positionArr.clear();
            distanceArr.clear();
            for (double i = highLidar; i >= lowLidar && doScan; i -= stepSize) {
                sweepComplete = false;
                liadar.setPosition(i);
                Thread.sleep(DELAY);
                dcm1 = distance.getDistance(DistanceUnit.CM);
                double pos = liadar.getPosition();
                positionArr.add(pos);
                angleArr.add(getAngle(pos));
                distanceArr.add(dcm1);
                if (dcm1 < distanceLowest) {
                    distanceLowest = dcm1;
                    angleLowest = getAngle(pos);
                    positionLowest = pos;
                    nextLowest = dcm1;
                    nextAngle = getAngle(pos);;
                    nextPos = pos;
                } else if(dcm1 < nextLowest) {
                    nextLowest = dcm1;
                    nextAngle = getAngle(pos);
                    nextPos = pos;
                }
            }
            sweepComplete = true;
            distanceLowest = nextLowest;
            angleLowest = nextAngle;
            positionLowest = nextPos;
            nextLowest = Double.MAX_VALUE;
            synchronized (sweepDataLock) {
                sweepData.setData(angleArr, positionArr, distanceArr);
            }
            angleArr.clear();
            positionArr.clear();
            distanceArr.clear();
            for (double i = lowLidar; i <= highLidar && doScan; i += stepSize) {
                sweepComplete = false;
                liadar.setPosition(i);
                Thread.sleep(DELAY);
                dcm1 = distance.getDistance(DistanceUnit.CM);
                double pos = liadar.getPosition();
                positionArr.add(pos);
                angleArr.add(getAngle(pos));
                distanceArr.add(dcm1);
                if (dcm1 < distanceLowest) {
                    distanceLowest = dcm1;
                    angleLowest = getAngle(pos);
                    positionLowest = pos;
                    nextLowest = dcm1;
                    nextAngle = getAngle(pos);
                    nextPos = pos;
                } else if(dcm1 < nextLowest) {
                    nextLowest = dcm1;
                    nextAngle = getAngle(pos);
                    nextPos = pos;
                }
            }
            sweepComplete = true;
            distanceLowest = nextLowest;
            angleLowest = nextAngle;
            positionLowest = nextPos;
            synchronized (sweepDataLock) {
                sweepData.setData(angleArr, positionArr, distanceArr);
            }
        }
    }

    private double getAngle(double pos) {
        return pos * 270 - 135;
    }

    public double getDistanceLowest() {
        return distanceLowest;
    }

    public double getAngleLowest() {
        return angleLowest;
    }

    public double getPositionLowest() {
        return positionLowest;
    }

    public boolean isSweepComplete() {
        return sweepComplete;
    }

    public double setPosition (double pos) {
        liadar.setPosition(pos);
        return distance.getDistance(DistanceUnit.CM);
    }

    public SweepData getSweepData() {
        SweepData data = null;

        synchronized (sweepDataLock) {
            data = sweepData.clone();
        }

        return data;
    }

    public double getLowLidar() {
        return lowLidar;
    }

    public void setLowLidar(double lowLidar) {
        this.lowLidar = lowLidar;
    }

    public double getHighLidar() {
        return highLidar;
    }

    public void setHighLidar(double highLidar) {
        this.highLidar = highLidar;
    }

    public double getStepSize() {
        return stepSize;
    }

    public void setStepSize(double stepSize) {
        this.stepSize = stepSize;
    }

    @Override
    public void run() {
        try {
            scan();
        } catch (InterruptedException iex) {}
    }
}
