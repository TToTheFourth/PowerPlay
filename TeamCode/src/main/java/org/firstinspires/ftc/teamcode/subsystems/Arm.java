package org.firstinspires.ftc.teamcode.subsystems;

import static org.firstinspires.ftc.teamcode.Constants.MAXIMUM_EXTENSION;
import static org.firstinspires.ftc.teamcode.Constants.MINIMUM_RETRACTION;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

public class Arm {
    private DcMotor leftLinearSlide;
    private DcMotor rightLinearSlide;
    private DcMotor arm;
    private Servo grabber1;
    private Servo grabber2;

    public Arm(LinearOpMode om) {
        leftLinearSlide = om.hardwareMap.get(DcMotor.class, "leftLinear");
        rightLinearSlide = om.hardwareMap.get(DcMotor.class, "rightLinear");

        //Set to brake mode on zero power
        leftLinearSlide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinearSlide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        //Reset encoders
        leftLinearSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightLinearSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        //Running using encoder
        leftLinearSlide.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightLinearSlide.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        arm = om.hardwareMap.get(DcMotor.class, "arm");
        arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        grabber1 = om.hardwareMap.get(Servo.class, "grabber1");
        grabber2 = om.hardwareMap.get(Servo.class, "grabber2");
    }

    public enum Direction {
        UP,
        DOWN
    }

    public enum Grabber {
        OPEN,
        CLOSE
    }

    public void runLinearSlides(float power, Direction direction) {
        switch (direction) {
            case UP:
                leftLinearSlide.setPower(power * -1);
                rightLinearSlide.setPower(power);
                break;

            case DOWN:
                leftLinearSlide.setPower(power);
                rightLinearSlide.setPower(power * -1);
                break;

            default:
                leftLinearSlide.setPower(0);
                rightLinearSlide.setPower(0);
                break;
        }
    }

    public void stopLinearSlides() {
        leftLinearSlide.setPower(0);
        rightLinearSlide.setPower(0);
    }

    public enum Position {
        EXTENDED,
        RETRACTED,
        MIDDLE,
    }

    public void extend(Position position) {
        int goal;

        /*
         * Data Table values:
         * 1.) -3925
         * 2.) -3911
         * 3.) -3938
         *
         * Avg: -3925
         */
        switch (position) {
            case EXTENDED:
                goal = MAXIMUM_EXTENSION;
                break;
            case MIDDLE:
                goal = MAXIMUM_EXTENSION/2;
                break;
            case RETRACTED:
                goal = MINIMUM_RETRACTION;
                break;
            default:
                goal = 0;
                break;
        }

        int pos = leftLinearSlide.getCurrentPosition() * -1;


            if (pos < MAXIMUM_EXTENSION/2) {
                leftLinearSlide.setPower(-1);
                rightLinearSlide.setPower(1);
            } else if (pos > MAXIMUM_EXTENSION/2){
                leftLinearSlide.setPower(1);
                rightLinearSlide.setPower(-1);
            } else {
                leftLinearSlide.setPower(0);
                rightLinearSlide.setPower(0);
            }
    } //Autonomous

    public void grab(Grabber grab) {
        switch (grab) {
            case OPEN:
                grabber1.setPosition(0);
                grabber2.setPosition(0);
                break;

            case CLOSE:
                grabber1.setPosition(1);
                grabber2.setPosition(1);
                break;
        }
    }
}