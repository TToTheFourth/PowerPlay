package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Constants.ARM_EXTENDED;
import static org.firstinspires.ftc.teamcode.Constants.ARM_HIGH;
import static org.firstinspires.ftc.teamcode.Constants.ARM_LOW;
import static org.firstinspires.ftc.teamcode.Constants.ARM_RESTING;
import static org.firstinspires.ftc.teamcode.Constants.ARM_RETRACTED;
import static org.firstinspires.ftc.teamcode.Constants.HIGH_POLE;
import static org.firstinspires.ftc.teamcode.Constants.LOW_POLE;
import static org.firstinspires.ftc.teamcode.Constants.MID_POLE;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Open;
import static org.firstinspires.ftc.teamcode.Constants.grabber1OpenIntaking;
import static org.firstinspires.ftc.teamcode.Constants.grabber1OpenSlidesUp;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Open;
import static org.firstinspires.ftc.teamcode.Constants.grabber2OpenIntaking;
import static org.firstinspires.ftc.teamcode.Constants.grabber2OpenSlidesUp;

import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Const;
import org.firstinspires.ftc.teamcode.tests.LiadarTestCH;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;
import org.openftc.apriltag.AprilTagDetection;

import java.util.ArrayList;

public class TurtleBotCoreV3 {

    private LinearOpMode opMode;


    private DcMotor rightLinear;
    private DcMotor leftLinear;
    private DcMotor arm;

    private Servo grabber1;
    private Servo grabber2;

    private RevColorSensorV3 color;

    private ElapsedTime aprilTagScannerTime;
    private ElapsedTime sequenceTimer;
    boolean shouldExtend = false;
    boolean shouldRetract = false;


    private enum SEQUENCE {
        EXTEND,
        EXTENDING_CLAW_1,
        EXTENDING_SLIDES_2,
        EXTENDING_ARM_3,
        EXTENDING_SLIDES_4,
        RETRACT,
        RETRACTING_CLAW_1,
        RETRACTING_ARM_2,
        RETRACTING_SLIDES_3,
        IDLE
    }


    SEQUENCE currentSequence = SEQUENCE.IDLE;
    int slideLevel = 0;
    int armLevel = 0;
    int currentSlidePos = 0;
    int currentArmPos = 0;
    int extendLevel = 3;
    double stackAugment = 0;
    double dunk = 0;
    double clearAugment = 0;
    boolean clawClosed = true;


    public TurtleBotCoreV3(LinearOpMode om) {
        this.opMode = om;

        color = om.hardwareMap.get(RevColorSensorV3.class, "color");

        rightLinear = om.hardwareMap.get(DcMotor.class, "rightLinear");
        leftLinear = om.hardwareMap.get(DcMotor.class, "leftLinear");
        arm = om.hardwareMap.get(DcMotor.class, "arm");

        //Grabber init. Grabber 1 is the left and 2 on the right from intake side.
        grabber1 = om.hardwareMap.get(Servo.class, "grabber1");
        grabber2 = om.hardwareMap.get(Servo.class, "grabber2");


        leftLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        leftLinear.setDirection(DcMotorSimple.Direction.REVERSE);
        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        arm.setDirection(DcMotorSimple.Direction.REVERSE);

        aprilTagScannerTime = new ElapsedTime();
        sequenceTimer = new ElapsedTime();



    }

    public boolean mechIsBusy() {
        if(currentSequence == SEQUENCE.IDLE) {
            return true;
        }
        else {
            return false;
        }
    }

    public void setDunk(double setDunk) {
        dunk = setDunk;
    }

    public void raiseStackAugmentOneLevel() {
        if(stackAugment < 5) {
            stackAugment++;
        }
    }

    public void lowerStackAugmentOneLevel() {
        if(stackAugment > 0) {
            stackAugment--;
        }
    }

    public void setStackAugmentZero() {
        stackAugment = 0;
    }

    public void setStackAugmentFive() {
        stackAugment = 5;
    }

    public void clearAugmentUp() {
        clearAugment = 800;
    }

    public void clearAugmentDown() {
        clearAugment = 0;
    }

    public void extendArm() {
        if (currentSequence == SEQUENCE.IDLE && currentSlidePos > MID_POLE - 50) {
            clawClosed = true;
            armLevel = 1;
        }
    }

    public void retractArm() {
        if(currentSequence == SEQUENCE.IDLE && currentSlidePos > MID_POLE - 50) {
            clawClosed = true;
            armLevel = -1;
        }
    }

    public void extendSlideOneLevel() {
        if (currentSequence == SEQUENCE.IDLE && slideLevel < 3) {
            slideLevel++;
        }
    }

    public void retractSlideOneLevel() {
        if(currentSequence == SEQUENCE.IDLE && slideLevel > 0) {
            slideLevel--;
        }
    }



    public void openClaw() {
        clawClosed = false;
    }

    public void closeClaw() {
        clawClosed = true;
    }

    public void extendArmAndSlide(int level) {
        currentSequence = SEQUENCE.EXTEND;
        extendLevel = level;
    }

    public void retractArmAndSlide() {
        currentSequence = SEQUENCE.RETRACT;
    }

    public void updateScoringMechManual(double armPower, double slidePower) {

        currentSlidePos = leftLinear.getCurrentPosition();
        currentArmPos = arm.getCurrentPosition();

        arm.setPower(armPower);
        leftLinear.setPower(slidePower);
        rightLinear.setPower(slidePower);

        if(clawClosed) {
            grabber1.setPosition(grabber1Closed);
            grabber2.setPosition(grabber2Closed);
        }
        else if(currentArmPos < (ARM_EXTENDED + ARM_RESTING)/2) {
            grabber1.setPosition(grabber1OpenSlidesUp);
            grabber2.setPosition(grabber2OpenSlidesUp);
        }
        else {
            grabber1.setPosition(grabber1OpenIntaking);
            grabber2.setPosition(grabber2OpenIntaking);
        }

    }

    public void armBrakeOff() {
        arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
    }
    public void armBrakeOn() {arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);}

    public void updateScoringMechAutomated(double armPowerScalar, double slidePowerScalar) {

        // Initialize input variables
        double targetSlidePos = 0;
        double targetArmPos = 0;
        currentSlidePos = leftLinear.getCurrentPosition();
        currentArmPos = arm.getCurrentPosition();


        if(clawClosed) {
            grabber1.setPosition(grabber1Closed);
            grabber2.setPosition(grabber2Closed);
        }
        else if(currentArmPos < (ARM_EXTENDED + ARM_RESTING)/2) {
            grabber1.setPosition(grabber1OpenSlidesUp);
            grabber2.setPosition(grabber2OpenSlidesUp);
        }
        else {
            grabber1.setPosition(grabber1OpenIntaking);
            grabber2.setPosition(grabber2OpenIntaking);
        }

        switch(currentSequence) {
            case IDLE:

                break;

            case EXTEND:

                sequenceTimer.reset();
                clawClosed = true;
                currentSequence = SEQUENCE.EXTENDING_CLAW_1;

                break;

            case EXTENDING_CLAW_1:

                if(sequenceTimer.seconds() > 0.1) {
                    currentSequence = SEQUENCE.EXTENDING_SLIDES_2;
                    if(extendLevel == 3) {
                        slideLevel = 3;
                    }

                    else {
                        slideLevel = 2;
                    }
                }

                break;

            case EXTENDING_SLIDES_2:

                if(currentSlidePos > MID_POLE) {
                    currentSequence = SEQUENCE.EXTENDING_ARM_3;
                    armLevel = 1;
                }

                break;

            case EXTENDING_ARM_3:

                if(extendLevel == 3 && currentSlidePos > HIGH_POLE - 100) {
                    currentSequence = SEQUENCE.IDLE;
                }
                if (extendLevel != 3 && currentSlidePos > MID_POLE - 100) {
                    currentSequence = SEQUENCE.EXTENDING_SLIDES_4;
                    slideLevel = extendLevel;
                }

                break;

            case EXTENDING_SLIDES_4:

                if(extendLevel == 2 && currentSlidePos < MID_POLE + 100) {
                    currentSequence = SEQUENCE.IDLE;
                }
                if(extendLevel == 1 && currentSlidePos < LOW_POLE + 100) {
                    currentSequence = SEQUENCE.IDLE;
                }

                break;



            case RETRACT:

                sequenceTimer.reset();
                clawClosed = true;
                // slideLevel = 2;
                currentSequence = SEQUENCE.RETRACTING_CLAW_1;

                break;

            case RETRACTING_CLAW_1:

                if(sequenceTimer.seconds() > 0.1) {
                    currentSequence = SEQUENCE.RETRACTING_ARM_2;
                    armLevel = -1;
                }

                break;

            case RETRACTING_ARM_2:

                if(currentArmPos > 0) {
                    currentSequence = SEQUENCE.RETRACTING_SLIDES_3;
                    slideLevel = 0;
                }

                break;

            case RETRACTING_SLIDES_3:

                if(currentSlidePos < LOW_POLE + 100 + (stackAugment * 125)) {
                    currentSequence = SEQUENCE.IDLE;
                    clawClosed = false;
                    armLevel = 0;
                }

                break;

        }


        if(slideLevel == 3) {
            targetSlidePos = HIGH_POLE - dunk;
        }

        if(slideLevel == 2) {
            targetSlidePos = MID_POLE - dunk;
        }

        if(slideLevel == 1) {
            if(armLevel != 1) {
                targetSlidePos = LOW_POLE + (stackAugment * 125) + clearAugment;
            }
            else {
                targetSlidePos = LOW_POLE - dunk;
            }

        }

        if(slideLevel == 0) {
            targetSlidePos = LOW_POLE + (stackAugment * 125) + clearAugment;
        }

        if(armLevel == 1) {


            if(slideLevel == 1) {
                targetArmPos = ARM_EXTENDED + (0.3 * dunk);
            }
            else {
                targetArmPos = ARM_EXTENDED;
            }

        }

        if(armLevel == 0) {
            targetArmPos = ARM_RESTING;

            if(slideLevel == 0 && stackAugment == 0) {
                armBrakeOff();
                armPowerScalar = 0;
            }
            else {
                armBrakeOn();
            }

        }

        if(armLevel == -1) {
            targetArmPos = ARM_RETRACTED;
        }

        setSlidePower(targetSlidePos, slidePowerScalar);
        setArmPower(targetArmPos, armPowerScalar);


    }

    public void resetArmEncoder() {
        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    public void resetSlideEncoder() {
        leftLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    public void setArmPower(double targetPos, double powerScalar) {

        int currentPos = arm.getCurrentPosition();
        double difference = currentPos - targetPos;
        double power = 0;

        if(Math.abs(difference) > 600) {
            power = - powerScalar * (Math.abs(difference) / difference);
        }

        else {
            power = -powerScalar * (difference / 600);
        }

        arm.setPower(power);
    }

    public void setSlidePower(double targetPos, double powerScalar) {

        int currentPos = leftLinear.getCurrentPosition();
        double difference = currentPos - targetPos;
        double power = 0;

        if(Math.abs(difference) > 300) {
            power = - powerScalar * (Math.abs(difference) / difference);
        }

        else {
            power = -powerScalar * (difference / 300);
        }

        leftLinear.setPower(power);
        rightLinear.setPower(power);
    }

    public void setIdle() {
        arm.setPower(0);
        leftLinear.setPower(0);
        rightLinear.setPower(0);
    }









    /**
     * VISION CODE SECTION BELOW
     *
     * Scans a specified amount of times using AprilTag
     * @param aprilTagDetector
     * @return The ID of the detected Apriltag
     */
    public int scanAprilTag(AprilTagsVersion1 aprilTagDetector) {
        ArrayList<AprilTagDetection> detections;
        int detectionTypeOne = 0;
        int detectionTypeTwo = 0;
        int detectionTypeThree = 0;
        int incorrectDetections = 0;
        int noDetections = 0;

        detections = aprilTagDetector.getDetectionsUpdate();

        if (detections != null) {
            for (int i = 0; i < 100; i++) { // May need to change based off of time
                if (detections.size() > 0) {
                    for (AprilTagDetection detect : detections) {
                        int detectID = detect.id;

                        if (detectID == Constants.APRILTAG_ID_ONE) {
                            detectionTypeOne++;
                        } else if (detectID == Constants.APRILTAG_ID_TWO) {
                            detectionTypeTwo++;
                        } else if (detectID == Constants.APRILTAG_ID_THREE) {
                            detectionTypeThree++;
                        } else {
                            incorrectDetections++;
                        }
                    }
                } else {
                    noDetections++; //
                }
            }

            // Code then decides which one to return
            int returningDetection;
            int totalDetects = detectionTypeOne + detectionTypeTwo + detectionTypeThree + incorrectDetections;
            double certainty;

            if (detectionTypeOne > detectionTypeTwo && detectionTypeOne > detectionTypeThree) {
                returningDetection = Constants.APRILTAG_ID_ONE;
                certainty = Math.round((double) detectionTypeOne / totalDetects) * 100;
            } else if (detectionTypeTwo > detectionTypeOne && detectionTypeTwo > detectionTypeThree) {
                returningDetection = Constants.APRILTAG_ID_TWO;
                certainty = Math.round((double) detectionTypeTwo / totalDetects) * 100;
            } else {
                returningDetection = Constants.APRILTAG_ID_THREE;
                certainty = Math.round((double) detectionTypeThree / totalDetects) * 100;
            }

            double error = Math.round((double) incorrectDetections / totalDetects) * 100;

            // Not needed. Like to have for testing
            opMode.telemetry.addLine(String.format("Detection: %d", returningDetection));
            opMode.telemetry.addLine(String.format("Certainty: %f", certainty));
            opMode.telemetry.addLine(String.format("Error: %f", error));
            opMode.telemetry.addLine(String.format("No scans: %d", noDetections));
            opMode.telemetry.update();

            return returningDetection;
        } else {
            return -1;
        }
    }

    /**
     * Scans for a specified amount of time in seconds
     * @param aprilTagDetector
     * @return The ID of the detected Apriltag
     */
    public int scanAprilTagTime(AprilTagsVersion1 aprilTagDetector, int time) {
        ArrayList<AprilTagDetection> detections;
        int detectionTypeOne = 0;
        int detectionTypeTwo = 0;
        int detectionTypeThree = 0;
        int incorrectDetections = 0;
        int noDetections = 0;


        aprilTagScannerTime.reset();

        while (opMode.opModeIsActive() && aprilTagScannerTime.seconds() < time) { // May need to change based off of time
            detections = aprilTagDetector.getDetectionsUpdate();
            if (detections != null) {
                if (detections.size() > 0) {
                    for (AprilTagDetection detect : detections) {
                        int detectID = detect.id;

                        if (detectID == Constants.APRILTAG_ID_ONE) {
                            detectionTypeOne++;
                        } else if (detectID == Constants.APRILTAG_ID_TWO) {
                            detectionTypeTwo++;
                        } else if (detectID == Constants.APRILTAG_ID_THREE) {
                            detectionTypeThree++;
                        } else {
                            incorrectDetections++;
                        }
                    }
                } else {
                    noDetections++; //
                }
            }
        }

        // Code then decides which one to return
        int returningDetection;
        int totalDetects = detectionTypeOne + detectionTypeTwo + detectionTypeThree + incorrectDetections;
        double certainty;

        if (detectionTypeOne > detectionTypeTwo && detectionTypeOne > detectionTypeThree) {
            returningDetection = Constants.APRILTAG_ID_ONE;
            certainty = Math.round((double) detectionTypeOne / totalDetects) * 100;
        } else if (detectionTypeTwo > detectionTypeOne && detectionTypeTwo > detectionTypeThree) {
            returningDetection = Constants.APRILTAG_ID_TWO;
            certainty = Math.round((double) detectionTypeTwo / totalDetects) * 100;
        } else {
            returningDetection = Constants.APRILTAG_ID_THREE;
            certainty = Math.round((double) detectionTypeThree / totalDetects) * 100;
        }

        double error = Math.round((double) incorrectDetections / totalDetects) * 100;

        // Not needed. Like to have for testing
//            opMode.telemetry.addLine(String.format("Detection: %d", returningDetection));
//            opMode.telemetry.addLine(String.format("Certainty: %f", certainty));
//            opMode.telemetry.addLine(String.format("Error: %f", error));
//            opMode.telemetry.addLine(String.format("No scans: %d", noDetections));
//            opMode.telemetry.update();
        opMode.telemetry.addLine("Detection: " + returningDetection);
        opMode.telemetry.addLine("Certainty: " + certainty + "%");
        opMode.telemetry.addLine("Error: " + error + "%");
        opMode.telemetry.addLine("No scans: " + noDetections);
        opMode.telemetry.update();

        return returningDetection;
    }

}