package org.firstinspires.ftc.teamcode.tests;

import static org.firstinspires.ftc.teamcode.Constants.CENTER_CAMERA;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.tests.LiadarTestCH;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion3;
import org.opencv.core.RotatedRect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@Disabled
@TeleOp
public class LiadarCameraIntegration extends LinearOpMode {
    private LiadarTestCH liadar;
    private OpenCVVersion3 cv;

    private DistanceSensor distance;
    private Servo lidarServo;
    private ForWhatWeHaveNow rep;

    @Override
    public void runOpMode() throws InterruptedException {
        lidarServo = hardwareMap.get(Servo.class, "liadarServo");
        lidarServo.scaleRange(0.33,0.97);
        distance = hardwareMap.get(DistanceSensor.class, "distance");
        cv = new OpenCVVersion3(this, true, false);
        rep = new ForWhatWeHaveNow(this);

        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);

        waitForStart();

        ArrayList<Distance> distances = new ArrayList<Distance>();
        int DELAY = 50;

        if (opModeIsActive()) {
            while (opModeIsActive()) {
                if (gamepad1.a) {
                    liadar.setPosition(0.5);
                    int index = 0;
                    double meanDifferences = 0;
                    for(double i = 0.5; i >= 0; i+=-0.02) {
                        double d = distance.getDistance(DistanceUnit.INCH);
                        distances.add(new Distance(d, i));

                        sleep(DELAY);

                        if(distances.size() > 1) {
                            double diffs = distances.get(index-1).getDistance() - distances.get(index).getDistance();
                            if (diffs > meanDifferences)
                                break;
                            else
                                meanDifferences = (meanDifferences + diffs)/2;
                        }

                        index++;
                    }
                }
            }
        }
    }

    public double cmToInches(double cm) {
        double inch = 0.3937 * cm;
        return inch;
    }

    public static double median (ArrayList<Distance> distances) {
        double medianDistance;

        if (distances.size() % 2 == 0) {
            medianDistance = distances.get(distances.size()/2).getDistance()
                    + distances.get(distances.size()/2-1).getDistance();

            return medianDistance;
        }

        return distances.get(distances.size()/2).getDistance();
    }
}