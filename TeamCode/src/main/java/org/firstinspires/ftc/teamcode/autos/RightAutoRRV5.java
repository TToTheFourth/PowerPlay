package org.firstinspires.ftc.teamcode.autos;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.TurtleBotCoreV2;
import org.firstinspires.ftc.teamcode.roadrunner.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;

@Disabled
@Autonomous
public class RightAutoRRV5 extends LinearOpMode {

    enum Phase {
        SCAN,
        TO_POLE,
        SCORE,
        TO_STACK,
        INTAKE,
        PARK,
        IDLE
    }

    Phase currentPhase = Phase.SCAN;

    int scored = 0;
    int cycles = 0;
    double slideAugment = 0;
    ElapsedTime timer = new ElapsedTime();
    boolean waiting = false;

    Pose2d startPose = new Pose2d(35, -68, Math.toRadians(-90));



    @Override
    public void runOpMode() throws InterruptedException {



        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        TurtleBotCoreV2 bot = new TurtleBotCoreV2(this);
        AprilTagsVersion1 tag = new AprilTagsVersion1(this);

        drive.setPoseEstimate(startPose);

        Trajectory toPoleFromStart = drive.trajectoryBuilder(startPose,true)
                .splineToSplineHeading(new Pose2d(-35,-20,Math.toRadians(-90)),Math.toRadians(90))
                .splineToSplineHeading(new Pose2d(-34,-8,Math.toRadians(-145)),  Math.toRadians(90))
                .addTemporalMarker(1, () -> bot.extendSlides())
                .addTemporalMarker(1.5, () -> bot.extendArm())
                .build();

        Trajectory toStack = drive.trajectoryBuilder(toPoleFromStart.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-60,-12,Math.toRadians(180)),Math.toRadians(180))
                .addTemporalMarker(0.3, () -> bot.closeClaw())
                .addTemporalMarker(0.5, () -> bot.retractArm())
                .addTemporalMarker(0.5, () -> bot.retractSlides())
                .build();

        Trajectory toPoleNormal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-34,-8,Math.toRadians(-145)),  Math.toRadians(30))
                .addTemporalMarker(0.2, () -> bot.extendSlides())
                .addTemporalMarker(1, () -> bot.extendArm())
                .build();

        Trajectory toPoleFinal = drive.trajectoryBuilder(toStack.end(),true)
                .splineToSplineHeading(new Pose2d(-25,-12,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-10,-16,Math.toRadians(145)),  Math.toRadians(-30))
                .addTemporalMarker(0.7, () -> bot.extendSlides())
                .addTemporalMarker(1.2, () -> bot.extendArm())
                .build();

        Trajectory park1 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-57,-12,Math.toRadians(180)),Math.toRadians(180))
                .addTemporalMarker(0.3, () -> bot.closeClaw())
                .addTemporalMarker(0.5, () -> bot.retractArm())
                .addTemporalMarker(0.5, () -> bot.retractSlides())
                .build();

        Trajectory park2 = drive.trajectoryBuilder(toPoleNormal.end(),false)
                .lineToLinearHeading(new Pose2d(-34, -12, Math.toRadians(180)))
                .addTemporalMarker(0.3, () -> bot.closeClaw())
                .addTemporalMarker(0.5, () -> bot.retractArm())
                .addTemporalMarker(0.5, () -> bot.retractSlides())
                .build();

        Trajectory park3 = drive.trajectoryBuilder(toPoleFinal.end(),false)
                .splineToSplineHeading(new Pose2d(-25,-12,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-10,-16,Math.toRadians(145)),  Math.toRadians(-30))
                .addTemporalMarker(0.3, () -> bot.closeClaw())
                .addTemporalMarker(0.5, () -> bot.retractArm())
                .addTemporalMarker(0.5, () -> bot.retractSlides())
                .build();



        int preSensed = bot.scanAprilTagTime(tag, 1);

        telemetry.addData("Status", "Ready for start");
        telemetry.update();

        waitForStart();

        int sensed = bot.scanAprilTagTime(tag, 1);
        bot.closeClaw();

        while (opModeIsActive()) {
            drive.update();
            bot.updateScoringMech(slideAugment);

            telemetry.addData("signal state", sensed);
            telemetry.update();


            switch (currentPhase) {

                case SCAN:

                    currentPhase = Phase.TO_POLE;
                    drive.followTrajectoryAsync(toPoleFromStart);

                    break;


                case TO_POLE:

                    if (!drive.isBusy()) {
                        currentPhase = Phase.SCORE;
                    }

                    break;

                case SCORE:

                    if(!waiting) {
                        timer.reset();
                        waiting = true;
                        slideAugment = -2;
                    }

                    else if(timer.seconds() > 0.2)  {
                        bot.openClaw(true);
                    }

                    else if(timer.seconds() > 0.5)  {
                        slideAugment = 0;
                        scored++;
                        waiting = false;

                        if(scored == cycles + 1) {
                            currentPhase = Phase.PARK;
                            if(sensed == Constants.APRILTAG_ID_ONE) {
                                drive.followTrajectoryAsync(park1);
                            }

                            if(sensed == Constants.APRILTAG_ID_TWO) {
                                drive.followTrajectoryAsync(park2);
                            }

                            if(sensed == Constants.APRILTAG_ID_THREE) {
                                drive.followTrajectoryAsync(park3);
                            }

                        }

                        if(scored < cycles + 1) {
                            currentPhase = Phase.TO_STACK;
                            slideAugment = (5 - scored) * Constants.STACK_AUGMENT;
                            drive.followTrajectoryAsync(toStack);
                        }

                    }

                    break;


                case TO_STACK:

                    if(!drive.isBusy()) {
                        currentPhase = Phase.INTAKE;
                    }

                    break;

                case INTAKE:

                    if(!waiting) {
                        bot.closeClaw();
                        timer.reset();
                        waiting = true;

                    }

                    else if(timer.seconds() > 0.2)  {
                        slideAugment = 5;
                    }

                    else if(timer.seconds() > 0.7)  {
                        waiting = false;
                        currentPhase = Phase.TO_POLE;

                        if(scored == cycles && sensed == Constants.APRILTAG_ID_THREE) {
                            drive.followTrajectoryAsync(toPoleFinal);
                        }
                        else {
                            drive.followTrajectoryAsync(toPoleNormal);
                        }

                    }



                    break;


                case PARK:
                    if (!drive.isBusy()) {
                        currentPhase = Phase.IDLE;
                    }

                    break;

                case IDLE:

                    break;

            }
        }











    }
}


