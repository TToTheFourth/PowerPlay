package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Constants.LOW_POLE;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Closed;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cRangeSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.Gyro;
import org.firstinspires.ftc.teamcode.Gyro2;

import java.util.Timer;

public class ForWhatWeHaveNow {

    private static final double MAX_ANGLE = 5.0;
    private static final double ANGLE_ADJ_PERC = 0.2;
    private static final double ANGLE_ADJ_SPEED = 0.2;
    int one = 0;
    int two = 0;
    int three = 0;
    final int CUTOFF_SECONDS = 6;

    private DcMotor backLeftMotor;
    private DcMotor frontLeftMotor;
    private DcMotor frontRightMotor;
    private DcMotor backRightMotor;
    private Servo grabber1;
    private Servo grabber2;

    private Gyro gyro;
    private Gyro2 miniGyro;
    private LinearOpMode opMode;

    private DcMotor leftLinear;
    private DcMotor rightLinear;
    private DcMotor arm;

    //private java.util.Timer timeKeeper = new java.util.Timer();
    private Timer timer4;
    private ElapsedTime driveTimer;

    public ForWhatWeHaveNow (LinearOpMode om) {
        this.opMode = om;

        backLeftMotor = opMode.hardwareMap.get(DcMotor.class, "motor0");
        frontLeftMotor = opMode.hardwareMap.get(DcMotor.class, "motor1");
        frontRightMotor = opMode.hardwareMap.get(DcMotor.class, "motor2");
        backRightMotor = opMode.hardwareMap.get(DcMotor.class, "motor3");
        BNO055IMU imu = opMode.hardwareMap.get(BNO055IMU.class, "imu");
        arm = opMode.hardwareMap.get(DcMotor.class, "arm");
        gyro = new Gyro(imu, opMode);
        driveTimer = new ElapsedTime();
        driveTimer.reset();

        leftLinear = opMode.hardwareMap.get(DcMotor.class, "leftLinear");
        rightLinear = opMode.hardwareMap.get(DcMotor.class, "rightLinear");
        grabber1 = opMode.hardwareMap.get(Servo.class, "grabber1");
        grabber2 = opMode.hardwareMap.get(Servo.class, "grabber2");
        //stoneServo = opMode.hardwareMap.get(Servo.class, "stoneServo");


        //stops movement of robot quickly.
        backLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    public void startGyro(){
        gyro.StartGyro();
        miniGyro = gyro.getMiniGyro();
    }

    public double ramp(double power, long startTime) {
        // ramp for 0.75 seconds
        long t = System.currentTimeMillis() - startTime;
        if (t >= 750) {
            return power;
        } else {
            return power / 750 * t;
        }
    }



    public void turnRight(double degrees, double power) {
        //
        gyro.resetWithDirection(Gyro.RIGHT);
        // tells the gyro we are turning right

        // start the motors turning right
        double rightY_G1 = 0.0;
        double rightX_G1 = -1.0 * power;
        double leftX_G1 = 0.0;

        frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
        backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
        backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
        frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
        // connects the motors to the correct variables

        // loop until the robot turns :) degrees
        double d = -1 * degrees;
        while (opMode.opModeIsActive()) {
            if (gyro.getAngle() <= d) {
                break;
            }
        }
        // gets angle turn

        frontLeftMotor.setPower(0.0);
        backLeftMotor.setPower(0.0);
        backRightMotor.setPower(0.0);
        frontRightMotor.setPower(0.0);
        // stops the motors
    }

    public void turnLeft(double degrees, double power) {
        //gyro.resetWithDirection(Gyro.LEFT);
        miniGyro.reset();
        // tells gyro we are going left

        // start the motors turning left
        double rightY_G1 = 0.0;
        double rightX_G1 = 1.0 * power;
        double leftX_G1 = 0.0;

        frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
        backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
        backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
        frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
        // sets the motors to the correct variables

        // loop until the robot turns :) degrees
        double d = degrees;
        while (opMode.opModeIsActive()) {
            if (Math.abs(miniGyro.getAngle()) >= d) {
                break;
            }
        }
        // gets degrees

        frontLeftMotor.setPower(0.0);
        backLeftMotor.setPower(0.0);
        backRightMotor.setPower(0.0);
        frontRightMotor.setPower(0.0);
        // stops motors
    }

    /**
     * THIS METHOD IS NOT FINAL. I AM STILL TRYING TO FIND A SOLUTION TO 'turnUntil'
     * @author Ethan
     * @param direction is either 'l' (left), 'r' (right), and anything else if nil. My lazy solution to my problems.
     */
    public void turnForever(char direction) {
        double motorDirection;

        // Modifies the gyro
        if (direction == 'l'){
//            gyro.resetWithDirection(Gyro.LEFT);
            motorDirection = -0.5;
        } else if (direction == 'r'){
//            gyro.resetWithDirection(Gyro.RIGHT);
            motorDirection = 0.5;
        } else {
            //Completely stops
            frontLeftMotor.setPower(0.0);
            backLeftMotor.setPower(0.0);
            backRightMotor.setPower(0.0);
            frontRightMotor.setPower(0.0);
            return;
        }

        // start the motors turning right if motor direction is 1, and left if it is -1
        double rightY_G1 = 0.0;
        double rightX_G1 = -1.0 * motorDirection;
        double leftX_G1 = 0.0;

        //Set the power in the correct direction
        frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
        backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
        backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
        frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
    }

    public void slide (double power, double distance) {

        // sets power
        // left is negative

        double rightY_G1 = 0;
        double rightX_G1 = 0;
        double leftY_G1 = 0;
        double leftX_G1 = -power;

        frontRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        // sets encoder

        if (miniGyro != null) {
            miniGyro.reset();
        }
        long ticks = ticksToInchesSlide(distance);
        long start = System.currentTimeMillis();
        driveTimer.reset();
        while (opMode.opModeIsActive()) {
            if (driveTimer.seconds() > CUTOFF_SECONDS) {
                break;
            }

            int rotations = frontRightMotor.getCurrentPosition();
            if (rotations<0) {
                rotations = rotations * -1;
            }
            if (rotations >= ticks) {
                break;
            }

            // Get the current heading; anything other than 0 is off course
            // this will return positive angle if drifting CCW
            // this will return negative angle if drifting CW
            double angle = miniGyro.getAngle();

            // Correct rightX_G1 [-1.0,1.0] to adjust turn
            // if rightX_G1 < 0 then robot will turn left
            // if rightX_G1 > 0 then robot will turn right
            rightX_G1 = -1.0 * angle * 0.022;
            leftX_G1 = ramp(-power, start);

            frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
            backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
            backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
            frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
        }

        // sets the inches to ticks so the motors understand
        frontLeftMotor.setPower(0);
        backLeftMotor.setPower(0);
        backRightMotor.setPower(0);
        frontRightMotor.setPower(0);
    }

    public void slideHold (double power, double distance, int slideTicks) {

        // sets power
        // left is negative

        double rightY_G1 = 0;
        double rightX_G1 = 0;
        double leftY_G1 = 0;
        double leftX_G1 = -power;

        frontRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        // sets encoder

        if (miniGyro != null) {
            miniGyro.reset();
        }
        long ticks = ticksToInchesSlide(distance);
        long start = System.currentTimeMillis();

        driveTimer.reset();
        while (opMode.opModeIsActive()) {
            if (driveTimer.seconds() > CUTOFF_SECONDS) {
                break;
            }
            closeClaw();
            int rotations = frontRightMotor.getCurrentPosition();
            if (rotations<0) {
                rotations = rotations * -1;
            }
            if (rotations >= ticks) {
                break;
            }
            // TODO: or break if distance < ??

            // Get the current heading; anything other than 0 is off course
            // this will return positive angle if drifting CCW
            // this will return negative angle if drifting CW
            double angle = miniGyro.getAngle();

            // Correct rightX_G1 [-1.0,1.0] to adjust turn
            // if rightX_G1 < 0 then robot will turn left
            // if rightX_G1 > 0 then robot will turn right
            rightX_G1 = -1.0 * angle * 0.022;
            leftX_G1 = ramp(-power, start);

            frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
            backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
            backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
            frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));

            double lp = ticRamp(slideTicks, leftLinear.getCurrentPosition(), 1);
            leftLinear.setPower(-lp);
            rightLinear.setPower(lp);

            double ap = ticRamp(Constants.ARM_HIGH, arm.getCurrentPosition(), 1);
            arm.setPower(ap);
        }

        // sets the inches to ticks so the motors understand
        frontLeftMotor.setPower(0);
        backLeftMotor.setPower(0);
        backRightMotor.setPower(0);
        frontRightMotor.setPower(0);
    }

    public void goForwardOld(double power, double distance){
        backRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        // sets the encoders

        double rightY_G1 = 1.0 * power;
        double rightX_G1 = 0.0;
        double leftX_G1 = 0.0;
        // sets power

        frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
        backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
        backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
        frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
        // sets the correct variables to the motors

        if (miniGyro != null) {
            miniGyro.reset();
        }

        long ticks = ticksToInchesForward(distance);
        long start = System.currentTimeMillis();
        while (opMode.opModeIsActive()) {
            int rotations = backRightMotor.getCurrentPosition();
            if (rotations<0) {
                rotations = rotations * -1;
            }
            if (rotations >= ticks) {
                break;
            }

            if (miniGyro.getAngle() > MAX_ANGLE) {
                turnRight(ANGLE_ADJ_PERC * miniGyro.getAngle(), ANGLE_ADJ_SPEED);
                frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
                backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
                backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
                frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
            }else if (miniGyro.getAngle() < -MAX_ANGLE){
                turnLeft (-ANGLE_ADJ_PERC * miniGyro.getAngle(), ANGLE_ADJ_SPEED);
                frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
                backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
                backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
                frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
            }
            // makes inches transfer to ticks
        }

        frontLeftMotor.setPower(0.0);
        backLeftMotor.setPower(0.0);
        backRightMotor.setPower(0.0);
        frontRightMotor.setPower(0.0);
        // sets motors to zero
    }

    public void goForward(double power, double distance){
        // sets the encoders
        frontLeftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontLeftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        double rightY_G1 = 1.0 * power;
        double rightX_G1 = 0.0;
        double leftX_G1 = 0.0;

        if (miniGyro != null) {
            miniGyro.reset();
        }

        long ticks = ticksToInchesForward(distance);
        long start = System.currentTimeMillis();
        driveTimer.reset();

        while (opMode.opModeIsActive()) {
            if (driveTimer.seconds() > CUTOFF_SECONDS) {
                break;
            }
            int rotations = frontLeftMotor.getCurrentPosition();
            if (rotations<0) {
                rotations = rotations * -1;
            }
            if (rotations >= ticks) {
                break;
            }

            // Get the current heading; anything other than 0 is off course
            // this will return positive angle if drifting CCW
            // this will return negative angle if drifting CW
            double angle = miniGyro.getAngle();

            // Correct rightX_G1 [-1.0,1.0] to adjust turn
            // if rightX_G1 < 0 then robot will turn left
            // if rightX_G1 > 0 then robot will turn right
            rightX_G1 = -1.0 * angle * 0.022;
            rightY_G1 = ramp(power, start);

            frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
            backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
            backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
            frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
        }

        stopMotor();
        // sets motors to zero
    }

    public void goForwardHold(double power, double distance, int slideTicks){
        // sets the encoders
        frontLeftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontLeftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        double rightY_G1 = 1.0 * power;
        double rightX_G1 = 0.0;
        double leftX_G1 = 0.0;

        if (miniGyro != null) {
            miniGyro.reset();
        }

        long ticks = ticksToInchesForward(distance);
        long start = System.currentTimeMillis();

        driveTimer.reset();

        while (opMode.opModeIsActive()) {
            if (driveTimer.seconds() > CUTOFF_SECONDS) {
                break;
            }

            closeClaw();
            int rotations = frontLeftMotor.getCurrentPosition();
            if (rotations<0) {
                rotations = rotations * -1;
            }
            if (rotations >= ticks) {
                break;
            }

            // Get the current heading; anything other than 0 is off course
            // this will return positive angle if drifting CCW
            // this will return negative angle if drifting CW
            double angle = miniGyro.getAngle();

            // Correct rightX_G1 [-1.0,1.0] to adjust turn
            // if rightX_G1 < 0 then robot will turn left
            // if rightX_G1 > 0 then robot will turn right
            rightX_G1 = -1.0 * angle * 0.022;
            rightY_G1 = ramp(power, start);

            double lp = ticRamp(slideTicks, leftLinear.getCurrentPosition(), 1);
            leftLinear.setPower(-lp);
            rightLinear.setPower(lp);

            frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
            backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
            backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
            frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
        }

        stopMotor();
        // sets motors to zero
    }

    public void goForwardNoGyro(double power, double distance){
        double rightY_G1 = 1.0 * power;
        double rightX_G1 = 0.0;
        double leftX_G1 = 0.0;
        // sets power

        // sets the correct variables to the motors

        backRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        // sets the encoders

        long ticks = ticksToInchesForward(distance);
        long start = System.currentTimeMillis();

        while (opMode.opModeIsActive()) {
            int rotations = backRightMotor.getCurrentPosition();
            if (rotations<0) {
                rotations = rotations * -1;
            }
            if (rotations >= ticks) {
                break;
            }

            rightY_G1 = ramp(power, start);
            frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
            backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
            backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
            frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));
        }

        stopMotor();
        // sets motors to zero
    }

    public void stopMotor(){
        frontLeftMotor.setPower(0.0);
        backLeftMotor.setPower(0.0);
        backRightMotor.setPower(0.0);
        frontRightMotor.setPower(0.0);
    }
    public long ticksToInchesForward(double inches) {
        return (long) (inches * 1610); //1901.75
        // ticks forward formula
    }
    public long ticksToInchesSlide(double inches) {
        return (long) (inches * 1610); //1925.33
        // tick to slide inches formula 74.6
    }
    public long inchesToTime(double inches, double power) {
        return (long) (0.0384 * inches * 500.0 / power);
        // inches to time formula
    }

//    public void dropSweep() {
//        miniSweep.setPosition(-1);
//        opMode.sleep(500);
//    }

    public void goForwardRamp(double power, double distance){
        backRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        double rightY_G1 = 1.0 * power;
        double rightX_G1 = 0.0;
        double leftX_G1 = 0.0;

        long ticks = ticksToInchesForward(distance);

        // TODO: figure out the increment value based on the target speed
        float increement = 0.0f;

        // TODO: set the rightY_G1 start speed to a low value, but not so low the motors won't turn

        if (miniGyro != null) {
            miniGyro.reset();
        }
        while (opMode.opModeIsActive()) {
            int rotations = backRightMotor.getCurrentPosition();
            if (rotations<0) {
                rotations = rotations * -1;
            }
            if (rotations >= ticks) {
                break;
            }

            // TODO: if a certain number of ticks has occured then increment rightY_G1 until desired speed

            // TODO: if we are approaching the target distance then start to decrement rightY_G1 speed

            // Get the current heading; anything other than 0 is off course
            // this will return positive angle if drifting CCW
            // this will return negative angle if drifting CW
            double angle = miniGyro.getAngle();

            // Correct rightX_G1 [-1.0,1.0] to adjust turn
            // if rightX_G1 < 0 then robot will turn left
            // if rightX_G1 > 0 then robot will turn right
            rightX_G1 = -1.0 * angle * 0.022;

            frontLeftMotor.setPower((rightX_G1 + rightY_G1 - leftX_G1));
            backLeftMotor.setPower((rightX_G1 + rightY_G1 + leftX_G1));
            backRightMotor.setPower((rightX_G1 - rightY_G1 + leftX_G1));
            frontRightMotor.setPower((rightX_G1 - rightY_G1 - leftX_G1));

        }

        // sets motors to zero
        stopMotor();
    }

    public void moveToPos(int goal) {
        double direction = 1.0;
        if (Math.abs(leftLinear.getCurrentPosition()) > goal)
            direction = -1.0;

        double power = ticRamp(goal, leftLinear.getCurrentPosition(), direction);
        leftLinear.setPower(power);
        rightLinear.setPower(-power);
    }

    public void goUp(int ticks){


        while(opMode.opModeIsActive()) {
            if(Math.abs(leftLinear.getCurrentPosition()) >= ticks -100){
                break;
            }
            double power = ticRamp(ticks, leftLinear.getCurrentPosition(), 1);
            leftLinear.setPower(-power);
            rightLinear.setPower(power);

            closeClaw();
        }
    }

    public void goDown(int ticks){


        while(opMode.opModeIsActive()) {

            boolean slidebottom;


            if(Math.abs(leftLinear.getCurrentPosition()) <= 100) {
                break;
            }

            double power = -0.5;
            leftLinear.setPower(-power);
            rightLinear.setPower(power);

            if(arm.getCurrentPosition() <= Constants.ARM_LOW - 100){
                arm.setPower(0);
            }
            else {
                arm.setPower(-0.5);
            }
        }
        leftLinear.setPower(0);
        rightLinear.setPower(0);
        arm.setPower(0);
    }

    public void goDownHold(int ticks){


        while(opMode.opModeIsActive()) {
            closeClaw();

            if(Math.abs(leftLinear.getCurrentPosition()) <= ticks) {
                break;
            }

            double power = -0.5;
            leftLinear.setPower(-power);
            rightLinear.setPower(power);

//            double armPower = ticRamp(Constants.ARM_HIGH - 200, arm.getCurrentPosition(), 1);
//            arm.setPower(armPower);
        }
    }

    public void raiseArm(){
        while(opMode.opModeIsActive()) {
            if(Math.abs(arm.getCurrentPosition()) >= Constants.ARM_HIGH -300){
                break;
            }
            double power = ticRamp(Constants.ARM_HIGH - 200, arm.getCurrentPosition(), 1);
            arm.setPower(power);
        }
    }

    public void lowerArm(){
        while(opMode.opModeIsActive()) {
            if(arm.getCurrentPosition() <= Constants.ARM_LOW -100){
                break;
            }
            double power = -0.5;
            arm.setPower(power);
        }
        arm.setPower(0);
    }





    public void openClaw(){
        grabber1.setPosition(Constants.grabber1Open);
        grabber2.setPosition(Constants.grabber2Open);
    }

    public void closeClaw(){
        grabber1.setPosition(Constants.grabber1Closed);
        grabber2.setPosition(Constants.grabber2Closed);
    }

    private double ticRamp(int goal, int cur, double power) {
        double val = 0.0;

        if(Math.abs(goal) - Math.abs(cur) <= 300) {
            val = power * (Math.abs(goal) - Math.abs(cur)) / 300;
        } else {
            val = power;
        }

        return val;
    }
}