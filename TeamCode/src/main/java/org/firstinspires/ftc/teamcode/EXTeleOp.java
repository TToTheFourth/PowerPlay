package org.firstinspires.ftc.teamcode;

import static org.firstinspires.ftc.teamcode.Constants.ARM_HIGH;
import static org.firstinspires.ftc.teamcode.Constants.ARM_LOW;
import static org.firstinspires.ftc.teamcode.Constants.HIGH_POLE;
import static org.firstinspires.ftc.teamcode.Constants.LOW_POLE;
import static org.firstinspires.ftc.teamcode.Constants.MID_POLE;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber1Open;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Closed;
import static org.firstinspires.ftc.teamcode.Constants.grabber2Open;

import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.tests.LiadarTestCH;

@TeleOp
@Disabled
public class EXTeleOp extends LinearOpMode {

    private DcMotor backLeftMotor;
    private DcMotor frontLeftMotor;
    private DcMotor frontRightMotor;
    private DcMotor backRightMotor;

    private DcMotor intake;

    private DcMotor rightLinear;
    private DcMotor leftLinear;
    private DcMotor arm;

    private Servo grabber1;
    private Servo grabber2;
    private Servo flag;

    private RevColorSensorV3 color;

    private ElapsedTime clawTime;


    @Override
    public void runOpMode() throws InterruptedException {
        // Drive train init
        backLeftMotor = hardwareMap.get(DcMotor.class, "motor0");
        frontLeftMotor = hardwareMap.get(DcMotor.class, "motor1");
        frontRightMotor = hardwareMap.get(DcMotor.class, "motor2");
        backRightMotor = hardwareMap.get(DcMotor.class, "motor3");
        color = hardwareMap.get(RevColorSensorV3.class, "color");

        //Intake init
        intake = hardwareMap.get(DcMotor.class, "intake");

        //Linear slide init. Left and right looking from intake side
        rightLinear = hardwareMap.get(DcMotor.class, "rightLinear");
        leftLinear = hardwareMap.get(DcMotor.class, "leftLinear");

        //Arm init. Part of Linear slide init
        arm = hardwareMap.get(DcMotor.class, "arm");

        //Grabber init. Grabber 1 is the left and 2 on the right from intake side.
        grabber1 = hardwareMap.get(Servo.class, "grabber1");
        grabber2 = hardwareMap.get(Servo.class, "grabber2");
        flag = hardwareMap.get(Servo.class, "flag");

        Servo liadarServo = hardwareMap.get(Servo.class, "liadarServo");
        DistanceSensor distance = hardwareMap.get(DistanceSensor.class, "distance");
        LiadarTestCH lidar = new LiadarTestCH(liadarServo, distance);

        clawTime = new ElapsedTime();

        //telemetry.addData("Status", "Initialized");
        //telemetry.update();

        claw(false);
        waitForStart();

        //Thread thread = new Thread(lidar);
        //thread.start();

        leftLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        frontLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRightMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        intake.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontLeftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        frontRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
//        backLeftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
//        backRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        double rightX_G1;
        double rightY_G1;
        double leftX_G1;
        int goal = 0;
        int level = 0; // 0 is all the way down, 1 is low pole, 2 is mid pole, 3 is high pole
        int armPosBozo = -1; // -1 is resting/intaking position, 0 is middle, and 1 is extended out the back
        boolean retracting = false;
        boolean inputReceived = true;
        boolean lowering = false;
        boolean resetting = false;

        while (opModeIsActive()) {

            if (gamepad1.left_trigger > 0.1) {
                rightY_G1 = 0.35;
                rightX_G1 = -0.2 * gamepad1.right_stick_x;
                leftX_G1 = 0;
            }
            else if (gamepad1.right_trigger > 0.1) {
                rightY_G1 = -0.35;
                rightX_G1 = -0.2 * gamepad1.right_stick_x;
                leftX_G1 = 0;
            }
            else if (gamepad1.a) {
                rightY_G1 = 0.8 * gamepad1.left_stick_y;
                rightX_G1 = - 0.4 * gamepad1.right_stick_x;
                leftX_G1 = 0.7 * gamepad1.left_stick_x;

            }

            else {
                rightY_G1 = 0.8 * gamepad1.left_stick_y;
                rightX_G1 = - 0.8 * gamepad1.right_stick_x;
                leftX_G1 = 0.7 * gamepad1.left_stick_x;
            }



            double frontLeft = (rightX_G1 + rightY_G1 - leftX_G1);
            double backLeft = (rightX_G1 + rightY_G1 + leftX_G1);
            double backRight = (rightX_G1 - rightY_G1 + leftX_G1);
            double frontRight = (rightX_G1 - rightY_G1 - leftX_G1);

            frontLeftMotor.setPower(frontLeft);
            backLeftMotor.setPower(backLeft);
            backRightMotor.setPower(backRight);
            frontRightMotor.setPower(frontRight);

            //Forward -22821 for 12 inches
            //Slide 208 for 12 inches

            //Forward 1079
            //Slide -23104
            int pos = leftLinear.getCurrentPosition();
            int armPos = arm.getCurrentPosition();
            boolean slidebottom;


            if(Math.abs(leftLinear.getCurrentPosition()) <= 100) {
                slidebottom = true;
            } else {
                slidebottom = false;
            }

            if(gamepad1.dpad_up) {
                HIGH_POLE = 3500;
                MID_POLE = 1950;
                LOW_POLE = 125;
            }
            else if(gamepad1.dpad_down) {
                HIGH_POLE = 3100;
                MID_POLE = 1550;
                LOW_POLE = -50;
            }
            else {
                HIGH_POLE = 3365;
                MID_POLE = 1751;
                LOW_POLE = 0;
            }








            // Moves the target level up and down based on dpad input, with a shortcut to the top for a
            if (gamepad2.dpad_up && inputReceived == true && level != 3) {
                // Increments the level up one for every up press
                level++;
                inputReceived = false;
            }

            if (gamepad2.dpad_down && inputReceived == true && level != 0) {
                // Increments the level down one for every down press
                level--;
                inputReceived = false;
            }

            if (!gamepad2.dpad_up && !gamepad2.dpad_down) {
                // Resets for a new input once you let off the button
                inputReceived = true;
            }

            if(gamepad2.a) {
                level = 3;
            }

            if(Math.abs(gamepad2.left_stick_y) > 0.1) {
                resetting = true;
                leftLinear.setPower(-0.1 * gamepad2.left_stick_y);
                rightLinear.setPower(0.1 * gamepad2.left_stick_y);
            }

            if(resetting && gamepad2.y) {
                leftLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                leftLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                rightLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                rightLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                resetting = false;

            }


            // Just in case somebody decides to try and swing the arm before extending
            if (Math.abs(pos) > 1550) {
                if(gamepad2.b) {
                    armPosBozo = 1;
                }

                if(gamepad2.x) {
                    armPosBozo = -1;
                    level = 0;
                    claw(true);
                }
            }

            if(gamepad2.right_bumper) {
                claw(false);
            }

            // Retraction code

            if(gamepad2.left_trigger > 0.1 && level != 0 && !retracting) {
                claw(false);

            }

            if(gamepad2.right_trigger > 0.1 && level != 0 && !retracting) {
                claw(true);
                retracting = true;
                clawTime.reset();

            }

            if (retracting && clawTime.seconds() > 0 && Math.abs(pos) > 1550) {
                claw(true);
                armPosBozo = -1;
                level = 0;
                retracting = false;
                lowering = true;
            }

            if (lowering && slidebottom) {
                claw(false);
                lowering = false;
            }


            if (gamepad2.left_bumper || color.getRawLightDetected() > 300) {
                // close
                claw(true);
                if(color.getRawLightDetected() > 300) {
                    flag.setPosition(0.45);
                }

            }





/**
            if (gamepad2.dpad_left)
                goal = 0;
            else if (gamepad2.dpad_right)
                goal = 1; */
            /**
            if (gamepad2.a) {
                if(slidebottom) {
                    leftLinear.setPower(0.0);
                    rightLinear.setPower(0.0);
                    arm.setPower(0);
                    claw(false);

                } else {
                    claw(true);

                    double armPower;
                    leftLinear.setPower(0.5);
                    rightLinear.setPower(-0.5);
                    if (goal == 0) { //Pressing left dpad puts it behind
                        armPower = ticRamp((ARM_LOW-600), armPos, -1);
                    } else if (goal == 1) { //Pressing right dpad puts it ahead
                        armPower = ticRamp((ARM_AHEAD+500), armPos, 1);
                    } else {
                        armPower = ticRamp((ARM_LOW-600), armPos, -1);
                    }
                    arm.setPower(armPower);
                }
            }*/

            if (level == 2 && !resetting){
                double power = armTicRamp(MID_POLE, pos, 1);
                leftLinear.setPower(-power);
                rightLinear.setPower(power);
            }

            else if (level == 3 && !resetting) {
                double power = armTicRamp(HIGH_POLE, pos, 1);
                leftLinear.setPower(-power);
                rightLinear.setPower(power);
            }

            else if (level ==1 && !resetting) {
                double power = armTicRamp(LOW_POLE, pos, 1);
                leftLinear.setPower(-power);
                rightLinear.setPower(power);
            }

            else if (level == 0 && armPos < 300 && !resetting) {
                double power = armTicRamp(LOW_POLE, pos, 1);
                leftLinear.setPower(-power);
                rightLinear.setPower(power);
            }

            else {
                leftLinear.setPower(0);
                rightLinear.setPower(0);
            }


            if (armPosBozo == 1 && (level > 1 || armPos > 500)) {
                double armPower = armTicRamp(ARM_HIGH, armPos, 1);
                telemetry.addData("Ticks: ", armPos);
                arm.setPower(armPower);
            } else if (armPosBozo == -1) {
                double armPower = armTicRamp((ARM_LOW), armPos, 1);
                telemetry.addData("Ticks: ", armPos);
                arm.setPower(armPower);
            } else {
                arm.setPower(0);
            }


//            double power;
//
//            if(gamepad2.x) {
//                goal = HIGH_POLE;
//            } else if (gamepad2.y) {
//                goal = MID_POLE;
//            } else if (gamepad2.a) {
//                goal = LOW_POLE;
//            }
//
//            if (gamepad2.b) {
//                leftLinear.setPower(ticRamp(goal, pos, -1));
//                rightLinear.setPower(ticRamp(goal, pos, 1));
//            } else {
//                if (slidebottom) {
//                    leftLinear.setPower(0);
//                    rightLinear.setPower(0);
//                }
//            }




/**
            if (gamepad2.left_bumper) {
                claw(false);

//                clawTime.reset();

//                if (clawTime.seconds() > 1)


            } */



//            if (gamepad2.right_bumper || color.getRawLightDetected() > 300) {
//                // close
//                grabber1.setPosition(0.65);
//                grabber2.setPosition(0.80);
//                if(color.getRawLightDetected() > 300) {
//                    flag.setPosition(0.45);
//                }
//
//            } else {
//                grabber1.setPosition(0.90);
//                grabber2.setPosition(0.50);
//                flag.setPosition(0.8);
//            }

//            if (gamepad2.a) {
//                intake.setPower(1);
//            } else if (gamepad2.b) {
//                intake.setPower(-1);
//            } else if (gamepad2.y) {
//                intake.setPower(-0.5);
//            } else if (gamepad2.x) {
//                intake.setPower(0.5);
//            } else {
//                intake.setPower(0);
//            }

//            telemetry.addData("Target Power Left Back", backLeft);
//            telemetry.addData("Target Power Right Back", backRight);
//            telemetry.addData("Target Power Left Front", frontLeft);
//            telemetry.addData("Target Power Right Front", frontRight);
            //telemetry.addData("Forward Pod: ", frontLeftMotor.getCurrentPosition());
            //telemetry.addData("Slide Pod: ", frontRightMotor.getCurrentPosition());
           // telemetry.addData("rawLight", color.getRawLightDetected());

            telemetry.addData("level", level);
            telemetry.addData("armPosBozo", armPosBozo);


            //telemetry.addData("light",color.getLightDetected());
            telemetry.update();
        }


        frontLeftMotor.setPower(0);
        backLeftMotor.setPower(0);
        frontRightMotor.setPower(0);
        backRightMotor.setPower(0);
        intake.setPower(0);
        lidar.stop();
    }

    private double slideTicRamp(int goal, int cur, double power) {
        double val = 0.0;

        if(Math.abs(Math.abs(goal) - Math.abs(cur)) <= 300) {


            val = power * (Math.abs(goal) - Math.abs(cur)) / 300;
        } else if (Math.abs(goal) > Math.abs(cur)) {
            val = power;
        }
        else if (Math.abs(goal) <= Math.abs(cur)) {
            val = -power;
        }

        return val;
    }

    private double armTicRamp(int goal, int cur, double power) {
        double val = 0.0;

        if(Math.abs(goal - cur) <= 600) {


            val = power * (goal - cur) / 600;
        } else if (Math.abs(goal) > Math.abs(cur)) {
            val = power;
        }
        else if (Math.abs(goal) <= Math.abs(cur)) {
            val = -power;
        }

        return val;
    }

    private void claw (boolean closed) {
        if (closed) {
            grabber1.setPosition(grabber1Closed);
            grabber2.setPosition(grabber2Closed);

        }

        else {
            grabber1.setPosition(grabber1Open);
            grabber2.setPosition(grabber2Open);
            flag.setPosition(0.8);
        }

    }
}