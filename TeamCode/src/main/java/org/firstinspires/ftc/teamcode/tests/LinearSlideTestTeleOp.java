package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@Disabled
@TeleOp
public class LinearSlideTestTeleOp extends LinearOpMode {

    private DcMotor linearLeft;
    private DcMotor linearRight;
    @Override
    public void runOpMode() {
        linearLeft = hardwareMap.get(DcMotor.class, "linearL");
        linearRight = hardwareMap.get(DcMotor.class, "linearR");

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        waitForStart();

        double power;

        while (opModeIsActive()) {
            power = -gamepad1.left_stick_y;

            linearLeft.setPower(power);
            linearRight.setPower(power);

            telemetry.addData("Target Power Left Back", power);
        }

        linearRight.setPower(0);
        linearLeft.setPower(0);
    }
}