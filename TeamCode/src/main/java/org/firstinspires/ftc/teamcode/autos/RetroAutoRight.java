package org.firstinspires.ftc.teamcode.autos;

import static org.firstinspires.ftc.teamcode.Constants.APRILTAG_ID_ONE;
import static org.firstinspires.ftc.teamcode.Constants.APRILTAG_ID_THREE;
import static org.firstinspires.ftc.teamcode.Constants.HIGH_POLE;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.ForWhatWeHaveNow;
import org.firstinspires.ftc.teamcode.TurtleBotCore;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;

import java.util.Timer;

@Autonomous
public class RetroAutoRight extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        ForWhatWeHaveNow bot = new ForWhatWeHaveNow(this);
        Servo lidarServo = hardwareMap.get(Servo.class, "liadarServo");
        lidarServo.scaleRange(0.33,0.97);
        DistanceSensor distance = hardwareMap.get(DistanceSensor.class, "distance");
        TurtleBotCore turtle = new TurtleBotCore(this);
        AprilTagsVersion1 tag = new AprilTagsVersion1(this);


        bot.startGyro();

        waitForStart();

        bot.closeClaw();

        lidarServo.setPosition(0.5);

        int sensed = turtle.scanAprilTagTime(tag, 1);
        telemetry.addData("Sensed: ", sensed);
        telemetry.update();

        tag.stopCameraStream();

//        bot.goForward(- 0.5, 3); //61
//        sleep(3000);

//        double dis = distance.getDistance(DistanceUnit.INCH);
//        telemetry.addLine("Distance: " + dis);
//        telemetry.update();

        lidarServo.setPosition(0.05);


        //TODO: Create auto adjustment for Lidar using wall
//        sleep(700);

//        double wall = distance.getDistance(DistanceUnit.INCH);
//        telemetry.addLine("Left Wall: " + wall);
//        telemetry.update();
//        sleep(3000);
//        if (wall > 31) {
//            bot.slide(- 0.5, wall-30);
//        } else if (wall < 31) {
//            bot.slide(0.5, 30-wall);
//        }

        //go forward 58 inches
        bot.goForward(- 0.5, 55); //61

//        lidarServo.setPosition(0.5);
//        sleep(600);

//        dis = distance.getDistance(DistanceUnit.INCH);
//        telemetry.addLine("Distance: " + dis);
//        telemetry.update();

        //turn left 90 degrees m
        bot.turnRight(83, 0.3);

        /*
        //raise lift
        bot.goUp(Constants.HIGH_POLE-500);
        bot.raiseArm();

        //slide to the left 6 inches
        bot.slideHold(0.5,6,Constants.HIGH_POLE-500);

//        lidarServo.setPosition(0.5);

        bot.closeClaw();

        bot.goForwardHold(0.5,2,Constants.HIGH_POLE-500);

        sleep(1000);
        //drop
        bot.goDownHold(HIGH_POLE-650);
        bot.openClaw();
        sleep(650);

        bot.goUp(HIGH_POLE-500);

        sleep(1000);



        bot.goForwardHold(-0.5,2,Constants.HIGH_POLE-500);



        // Hold the slide and the arm at the current position for 1 second
        // To give time for the claw to open and the cone to fall

//        long startTime = System.currentTimeMillis();
//        while (opModeIsActive()){
//            bot.goUp(Constants.HIGH_POLE+200);
//            bot.raiseArm();
//            if(System.currentTimeMillis() - startTime > 1000){
//                break;
//            }
//        }

        //slide to the right 6 inches
        bot.slideHold(-0.3,14,Constants.HIGH_POLE-500);

        bot.closeClaw();
        //holding while claw is closing
//        while (opModeIsActive()){
//            bot.goUp(Constants.HIGH_POLE);
//            bot.raiseArm();
//            if(System.currentTimeMillis() - startTime > 1000){
//                break;
//            }
//        }
        //reset the arm
        bot.lowerArm();
        bot.goDown(300);
        //go to parking area and park
        */

        lidarServo.setPosition(0);
        if (sensed == APRILTAG_ID_ONE) {
            bot.goForward(0.5,24);
        } else if (sensed == APRILTAG_ID_THREE) {
            bot.goForward(-0.5,16);
        }

    }
}
