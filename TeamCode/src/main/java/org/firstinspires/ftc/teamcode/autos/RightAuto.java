package org.firstinspires.ftc.teamcode.autos;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.RepBot;
import org.firstinspires.ftc.teamcode.vision.OpenCVVersion4;

@Disabled
@Autonomous
public class RightAuto extends LinearOpMode {
    int dist = 0;

    @Override
    public void runOpMode() throws InterruptedException {
        RepBot bot = new RepBot(this);
        OpenCVVersion4 cv = new OpenCVVersion4(this);
        bot.startGyro();

        waitForStart();

        int pos = bot.coneColor(cv);

        bot.slide(0.5, 24);
        bot.goForward(0.5, 50);
        //pick up cone
        bot.slide(-0.25, 6);
        bot.goForward(0.5, 20);

        //cone cycles
        for (int i = 0; i < 2; i++) {
            bot.slide(-0.5, 32.5);
            //pick up and place cone on pole
            bot.slide(0.5, 32.5);
            bot.turnRight(90, 0.3);
            //grab new block
            bot.turnLeft(90, 0.3);
        }

        bot.slide(-0.5, 35);

        if (pos == 2) {
            dist = 20;
        } else if (pos == 3) {
            dist = 50;
        }

        bot.goForward(-0.5, dist);
    }
}
