package org.firstinspires.ftc.teamcode;

import com.acmerobotics.roadrunner.control.PIDCoefficients;
import com.acmerobotics.roadrunner.control.PIDFController;
import com.acmerobotics.roadrunner.profile.MotionProfile;
import com.acmerobotics.roadrunner.profile.MotionProfileGenerator;
import com.acmerobotics.roadrunner.profile.MotionState;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Const;
import org.firstinspires.ftc.teamcode.tests.LiadarTestCH;
import org.firstinspires.ftc.teamcode.vision.AprilTagsVersion1;
import org.openftc.apriltag.AprilTagDetection;

import java.util.ArrayList;

public class TurtleBotCoreV2 {

    // The opMode (u an opp)
    private LinearOpMode opMode;

    // Declarations

    // Motors
    private DcMotor rightLinear;
    private DcMotor leftLinear;
    private DcMotor arm;

    // Servos
    private Servo grabber1;
    private Servo grabber2;

    // Sensors
    private RevColorSensorV3 color;

    // Misc
    private ElapsedTime slideMotionProfileTimer;
    private ElapsedTime armMotionProfileTimer;
    private PIDFController slideController;
    private PIDFController slideControllerMP;
    private PIDFController armController;

    private enum SLIDE_STATE {
        RUNNING_MOTION_PROFILE,
        ACTIVE,
        IDLE
    }

    private enum ARM_STATE {
        RUNNING_MOTION_PROFILE,
        ACTIVE,
        IDLE
    }

    private MotionState zero = new MotionState(0, 0, 0);
    private MotionState one = new MotionState(5, 0, 0);
    private MotionState two = new MotionState(12, 0, 0);
    private MotionState three = new MotionState(20, 0, 0);
    private MotionState extended = new MotionState(115,0,0);
    private MotionState retracted = new MotionState(-45,0,0);

    private MotionProfile zeroToOne = MotionProfileGenerator.generateSimpleMotionProfile(
            zero,
            one,
            25,
            40,
            100
    );

    private MotionProfile zeroToTwo = MotionProfileGenerator.generateSimpleMotionProfile(
            zero,
            two,
            25,
            40,
            100
    );

    private MotionProfile zeroToThree = MotionProfileGenerator.generateSimpleMotionProfile(
            zero,
            three,
            25,
            40,
            100
    );

    private MotionProfile oneToZero = MotionProfileGenerator.generateSimpleMotionProfile(
            one,
            zero,
            25,
            40,
            100
    );

    private MotionProfile oneToTwo = MotionProfileGenerator.generateSimpleMotionProfile(
            one,
            two,
            25,
            40,
            100
    );

    private MotionProfile oneToThree = MotionProfileGenerator.generateSimpleMotionProfile(
            one,
            three,
            25,
            40,
            100
    );

    private MotionProfile twoToOne = MotionProfileGenerator.generateSimpleMotionProfile(
            two,
            one,
            25,
            40,
            100
    );

    private MotionProfile twoToZero = MotionProfileGenerator.generateSimpleMotionProfile(
            two,
            zero,
            25,
            40,
            100
    );

    private MotionProfile twoToThree = MotionProfileGenerator.generateSimpleMotionProfile(
            two,
            three,
            25,
            40,
            100
    );

    private MotionProfile threeToOne = MotionProfileGenerator.generateSimpleMotionProfile(
            three,
            one,
            25,
            40,
            100
    );

    private MotionProfile threeToTwo = MotionProfileGenerator.generateSimpleMotionProfile(
            three,
            two,
            25,
            40,
            100
    );

    private MotionProfile threeToZero = MotionProfileGenerator.generateSimpleMotionProfile(
            three,
            zero,
            25,
            40,
            100
    );


    private MotionProfile idle = MotionProfileGenerator.generateSimpleMotionProfile(
            new MotionState(0, 0, 0),
            new MotionState(1, 0, 0),
            25,
            40,
            100
    );

    private MotionProfile extend = MotionProfileGenerator.generateSimpleMotionProfile(
            retracted,
            extended,
            25,
            40,
            100
    );

    private MotionProfile retract = MotionProfileGenerator.generateSimpleMotionProfile(
            extended,
            retracted,
            25,
            40,
            100
    );

    private ElapsedTime aprilTagScannerTime;

    private MotionProfile currentSlideProfile = idle;
    private MotionProfile currentArmProfile = idle;

    public static SLIDE_STATE slideState = SLIDE_STATE.IDLE;
    public static ARM_STATE armState = ARM_STATE.IDLE;

    // Variables
    private PIDCoefficients SLIDE_COEFFICIENTS = Constants.SLIDE_COEFFICIENTS;
    private PIDCoefficients ARM_COEFFICIENTS = Constants.ARM_COEFFICIENTS;
    private double SPOOL_RADIUS = Constants.SPOOL_RADIUS;
    private double SPOOL_MOTOR_TICS_PER_REV = Constants.SPOOL_MOTOR_TICS_PER_REV;
    private double ARM_MOTOR_TICS_PER_REV = Constants.ARM_MOTOR_TICS_PER_REV;
    private double STARTING_ANGLE = Constants.STARTING_ANGLE;
    private double slideTargetPos = 0;
    private double slideCurrentPos = 0;
    private double armTargetPos = 0;
    private double armCurrentPos = 0;
    private double SLIDE_GRAVITY = Constants.SLIDE_GRAVITY;
    private double ARM_GRAVITY = Constants.ARM_GRAVITY;

    public TurtleBotCoreV2(LinearOpMode om) {
        // The meaning of life

        // Opp
        this.opMode = om;

        // Motors
        rightLinear = om.hardwareMap.get(DcMotor.class, "rightLinear");
        leftLinear = om.hardwareMap.get(DcMotor.class, "leftLinear");
        arm = om.hardwareMap.get(DcMotor.class, "arm");

        // Servos
        grabber1 = om.hardwareMap.get(Servo.class, "grabber1"); // Left side (from intake perspective)
        grabber2 = om.hardwareMap.get(Servo.class, "grabber2"); // Right side (from intake perspective)

        // Sensors
        color = om.hardwareMap.get(RevColorSensorV3.class, "color");

        // Misc
        slideMotionProfileTimer = new ElapsedTime();
        armMotionProfileTimer = new ElapsedTime();
        aprilTagScannerTime = new ElapsedTime();
        slideController = new PIDFController(SLIDE_COEFFICIENTS);
        slideControllerMP = new PIDFController(SLIDE_COEFFICIENTS);
        armController = new PIDFController(ARM_COEFFICIENTS);

        // Config
        leftLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftLinear.setDirection(DcMotorSimple.Direction.REVERSE);
        leftLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightLinear.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLinear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightLinear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

    }

    public void updateScoringMech(double slideAugment) {

        slideCurrentPos = getSlidePos();
        armCurrentPos = getArmPos();


        switch (slideState) {

            case IDLE:

                setSlidePower(0);

                break;


            case ACTIVE:

                slideController.setTargetPosition(slideTargetPos);
                double correction = slideController.update(slideCurrentPos);
                setSlidePower(correction);

                break;

            case RUNNING_MOTION_PROFILE:

                MotionState state = currentSlideProfile.get(slideMotionProfileTimer.seconds());
                slideControllerMP.setTargetPosition(state.getX());
                slideControllerMP.setTargetVelocity(state.getV());
                slideControllerMP.setTargetAcceleration(state.getA() + SLIDE_GRAVITY);
                double correction2 = slideControllerMP.update(slideCurrentPos);
                setSlidePower(correction2);

                if(state.getX() == currentSlideProfile.end().getX()) {
                    slideTargetPos = currentSlideProfile.end().getX();
                    slideState = SLIDE_STATE.ACTIVE;
                }

                break;


        }

        switch (armState) {

            case IDLE:

                arm.setPower(0);

                break;


            case ACTIVE:

                armController.setTargetPosition(armTargetPos + slideAugment);
                double correction = armController.update(armCurrentPos);
                arm.setPower(correction);

                break;

            case RUNNING_MOTION_PROFILE:

                MotionState state = currentArmProfile.get(armMotionProfileTimer.seconds());
                slideControllerMP.setTargetPosition(state.getX());
                slideControllerMP.setTargetVelocity(state.getV());
                slideControllerMP.setTargetAcceleration(state.getA() + (ARM_GRAVITY * Math.sin(getArmPos())));
                double correction2 = slideControllerMP.update(slideCurrentPos);
                setSlidePower(correction2);

                if(state.getX() == currentArmProfile.end().getX()) {
                    armTargetPos = currentArmProfile.end().getX();
                    armState = ARM_STATE.ACTIVE;
                }

                break;


        }


    }

    public void setSlideModeIdle() {
        slideState = SLIDE_STATE.IDLE;
    }

    public void runSlideModeActive(int targetPos) {
        slideTargetPos = targetPos;
        slideState = SLIDE_STATE.ACTIVE;
    }

    public void runSlideProfile(int currentLevel, int targetLevel) {
        slideState = SLIDE_STATE.RUNNING_MOTION_PROFILE;
        if (currentLevel == 0 && targetLevel == 1) {
            currentSlideProfile = zeroToOne;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 0 && targetLevel == 2) {
            currentSlideProfile = zeroToTwo;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 0 && targetLevel == 3) {
            currentSlideProfile = zeroToThree;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 1 && targetLevel == 0) {
            currentSlideProfile = oneToZero;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 1 && targetLevel == 2) {
            currentSlideProfile = oneToTwo;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 1 && targetLevel == 3) {
            currentSlideProfile = oneToThree;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 2 && targetLevel == 1) {
            currentSlideProfile = twoToOne;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 2 && targetLevel == 0) {
            currentSlideProfile = twoToZero;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 2 && targetLevel == 3) {
            currentSlideProfile = twoToThree;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 3 && targetLevel == 1) {
            currentSlideProfile = threeToOne;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 3 && targetLevel == 2) {
            currentSlideProfile = threeToTwo;
            slideMotionProfileTimer.reset();
        } else if (currentLevel == 3 && targetLevel == 0) {
            currentSlideProfile = threeToZero;
            slideMotionProfileTimer.reset();
        } else {
            slideState = SLIDE_STATE.IDLE;
        }
    }

    public void runArmProfile (int currentState, int targetState) {
        // A state of 1 represents resting position, a state of -1 represents extended out the back

        armState = ARM_STATE.RUNNING_MOTION_PROFILE;
        if (currentState == 1 && targetState == -1) {
            currentArmProfile = extend;
            armMotionProfileTimer.reset();
        } else if (currentState == -1 && targetState == 1) {
            currentArmProfile = retract;
            armMotionProfileTimer.reset();
        } else {
            armState = ARM_STATE.IDLE;
        }
    }

    public void setSlidePower(double power) {
        leftLinear.setPower(power);
        rightLinear.setPower(power);
    }

    public double getSlidePos() {
        double pos = ((leftLinear.getCurrentPosition() * 2 * Math.PI * SPOOL_RADIUS) / SPOOL_MOTOR_TICS_PER_REV);
        return pos;
    }

    public double getArmPos() {
        // In degrees
        double pos = (STARTING_ANGLE + (360 * (arm.getCurrentPosition() / ARM_MOTOR_TICS_PER_REV)));
        return pos;
    }

    public SLIDE_STATE getSlideState() {
        return slideState;
    }

    public ARM_STATE getArmState() {
        return armState;
    }

    public void extendSlides() {
        runSlideProfile(0,3);
    }
    public void extendArm() {
        runArmProfile(1,-1);
    }
    public void retractSlides() {
        runSlideProfile(3,0);
    }
    public void retractArm() {
        runArmProfile(-1,1);
    }

    public void openClaw(boolean slidesUp){

        if (slidesUp) {
            grabber1.setPosition(Constants.grabber1OpenSlidesUp);
            grabber2.setPosition(Constants.grabber2OpenSlidesUp);
        }

        else {
            grabber1.setPosition(Constants.grabber1OpenIntaking);
            grabber2.setPosition(Constants.grabber2OpenIntaking);
        }

    }

    public void closeClaw(){
        grabber1.setPosition(Constants.grabber1Closed);
        grabber2.setPosition(Constants.grabber2Closed);
    }

    /**
     * Scans a specified amount of times using AprilTag
     * @param aprilTagDetector
     * @return The ID of the detected Apriltag
     */
    public int scanAprilTag(AprilTagsVersion1 aprilTagDetector) {
        ArrayList<AprilTagDetection> detections;
        int detectionTypeOne = 0;
        int detectionTypeTwo = 0;
        int detectionTypeThree = 0;
        int incorrectDetections = 0;
        int noDetections = 0;

        detections = aprilTagDetector.getDetectionsUpdate();

        if (detections != null) {
            for (int i = 0; i < 100; i++) { // May need to change based off of time
                if (detections.size() > 0) {
                    for (AprilTagDetection detect : detections) {
                        int detectID = detect.id;

                        if (detectID == Constants.APRILTAG_ID_ONE) {
                            detectionTypeOne++;
                        } else if (detectID == Constants.APRILTAG_ID_TWO) {
                            detectionTypeTwo++;
                        } else if (detectID == Constants.APRILTAG_ID_THREE) {
                            detectionTypeThree++;
                        } else {
                            incorrectDetections++;
                        }
                    }
                } else {
                    noDetections++; //
                }
            }

            // Code then decides which one to return
            int returningDetection;
            int totalDetects = detectionTypeOne + detectionTypeTwo + detectionTypeThree + incorrectDetections;
            double certainty;

            if (detectionTypeOne > detectionTypeTwo && detectionTypeOne > detectionTypeThree) {
                returningDetection = Constants.APRILTAG_ID_ONE;
                certainty = Math.round((double) detectionTypeOne / totalDetects) * 100;
            } else if (detectionTypeTwo > detectionTypeOne && detectionTypeTwo > detectionTypeThree) {
                returningDetection = Constants.APRILTAG_ID_TWO;
                certainty = Math.round((double) detectionTypeTwo / totalDetects) * 100;
            } else {
                returningDetection = Constants.APRILTAG_ID_THREE;
                certainty = Math.round((double) detectionTypeThree / totalDetects) * 100;
            }

            double error = Math.round((double) incorrectDetections / totalDetects) * 100;

            // Not needed. Like to have for testing
            opMode.telemetry.addLine(String.format("Detection: %d", returningDetection));
            opMode.telemetry.addLine(String.format("Certainty: %f", certainty));
            opMode.telemetry.addLine(String.format("Error: %f", error));
            opMode.telemetry.addLine(String.format("No scans: %d", noDetections));
            opMode.telemetry.update();

            return returningDetection;
        } else {
            return -1;
        }
    }

    /**
     * Scans for a specified amount of time in seconds
     * @param aprilTagDetector
     * @return The ID of the detected Apriltag
     */
    public int scanAprilTagTime(AprilTagsVersion1 aprilTagDetector, int time) {
        ArrayList<AprilTagDetection> detections;
        int detectionTypeOne = 0;
        int detectionTypeTwo = 0;
        int detectionTypeThree = 0;
        int incorrectDetections = 0;
        int noDetections = 0;


        aprilTagScannerTime.reset();

        while (opMode.opModeIsActive() && aprilTagScannerTime.seconds() < time) { // May need to change based off of time
            detections = aprilTagDetector.getDetectionsUpdate();
            if (detections != null) {
                if (detections.size() > 0) {
                    for (AprilTagDetection detect : detections) {
                        int detectID = detect.id;

                        if (detectID == Constants.APRILTAG_ID_ONE) {
                            detectionTypeOne++;
                        } else if (detectID == Constants.APRILTAG_ID_TWO) {
                            detectionTypeTwo++;
                        } else if (detectID == Constants.APRILTAG_ID_THREE) {
                            detectionTypeThree++;
                        } else {
                            incorrectDetections++;
                        }
                    }
                } else {
                    noDetections++; //
                }
            }
        }

        // Code then decides which one to return
        int returningDetection;
        int totalDetects = detectionTypeOne + detectionTypeTwo + detectionTypeThree + incorrectDetections;
        double certainty;

        if (detectionTypeOne > detectionTypeTwo && detectionTypeOne > detectionTypeThree) {
            returningDetection = Constants.APRILTAG_ID_ONE;
            certainty = Math.round((double) detectionTypeOne / totalDetects) * 100;
        } else if (detectionTypeTwo > detectionTypeOne && detectionTypeTwo > detectionTypeThree) {
            returningDetection = Constants.APRILTAG_ID_TWO;
            certainty = Math.round((double) detectionTypeTwo / totalDetects) * 100;
        } else {
            returningDetection = Constants.APRILTAG_ID_THREE;
            certainty = Math.round((double) detectionTypeThree / totalDetects) * 100;
        }

        double error = Math.round((double) incorrectDetections / totalDetects) * 100;

        // Not needed. Like to have for testing
//            opMode.telemetry.addLine(String.format("Detection: %d", returningDetection));
//            opMode.telemetry.addLine(String.format("Certainty: %f", certainty));
//            opMode.telemetry.addLine(String.format("Error: %f", error));
//            opMode.telemetry.addLine(String.format("No scans: %d", noDetections));
//            opMode.telemetry.update();
        opMode.telemetry.addLine("Detection: " + returningDetection);
        opMode.telemetry.addLine("Certainty: " + certainty + "%");
        opMode.telemetry.addLine("Error: " + error + "%");
        opMode.telemetry.addLine("No scans: " + noDetections);
        opMode.telemetry.update();

        return returningDetection;
    }


}
