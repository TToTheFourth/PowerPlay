package com.example.meepmeeptesting;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.noahbres.meepmeep.MeepMeep;
import com.noahbres.meepmeep.roadrunner.DefaultBotBuilder;
import com.noahbres.meepmeep.roadrunner.entity.RoadRunnerBotEntity;

public class MeepMeepTest {
    public static void main(String[] args) {
        double coneScoreTime = 0.5;
        double intakeTime = 2;
        MeepMeep meepMeep = new MeepMeep(800);

        RoadRunnerBotEntity myBot = new DefaultBotBuilder(meepMeep)
                // Set bot constraints: maxVel, maxAccel, maxAngVel, maxAngAccel, track width
                .setDimensions(15,15)
                .setConstraints(60, 60, Math.toRadians(180), Math.toRadians(180), 15)
                .followTrajectorySequence(drive ->
                        drive.trajectorySequenceBuilder(new Pose2d(59, -16, Math.toRadians(35)))

                                .setReversed(true)
                                .lineToLinearHeading(new Pose2d(-61.5,-16,Math.toRadians(180)))
                                .splineToSplineHeading(new Pose2d(-23,-16,Math.toRadians(180)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(-8.5,-23.5,Math.toRadians(145)),  Math.toRadians(-35))


                                //.lineToLinearHeading(new Pose2d(61.5,-16,Math.toRadians(0)))

                                //.setReversed(true)
                //.splineToSplineHeading(new Pose2d(45,-16,Math.toRadians(0)),Math.toRadians(180))
                //.splineToSplineHeading(new Pose2d(34,-23.5,Math.toRadians(35)),  Math.toRadians(-150))
/**
                                .setReversed(true)

                                .lineToLinearHeading(new Pose2d(-35,-53,Math.toRadians(180)))

                .lineToLinearHeading(new Pose2d(-35,-47,Math.toRadians(180)))


                                .setReversed(false)
                .lineToLinearHeading(new Pose2d(-36,-16,Math.toRadians(180)))

                .lineToLinearHeading(new Pose2d(-61.5,-16,Math.toRadians(180)))


                                .setReversed(true)
                .lineToLinearHeading(new Pose2d(-58,-16,Math.toRadians(145)))

                                .setReversed(false)
                .lineToLinearHeading(new Pose2d(-61.5,-16,Math.toRadians(180)))

                                .setReversed(true)
                .splineToSplineHeading(new Pose2d(-45,-16,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-34,-23.5,Math.toRadians(145)),  Math.toRadians(-30))

                                .setReversed(false)
                .splineToSplineHeading(new Pose2d(-48,-16,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-61.5,-16,Math.toRadians(180)),Math.toRadians(180))

.setReversed(true)
                .splineToSplineHeading(new Pose2d(-25,-16,Math.toRadians(180)),Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(-10,-23.5,Math.toRadians(145)),  Math.toRadians(-30))



                                /**            .setReversed(false)
                .splineToSplineHeading(new Pose2d(-48,-16,Math.toRadians(180)),Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-61.5,-16,Math.toRadians(180)),Math.toRadians(180))
*//**
                                .setReversed(false)
                //.splineToSplineHeading(new Pose2d(-52,-16,Math.toRadians(180)),Math.toRadians(180))
               // .splineToSplineHeading(new Pose2d(-60,-16,Math.toRadians(180)),Math.toRadians(180))

                //.lineToLinearHeading(new Pose2d(-36, -16, Math.toRadians(180)))




               .lineToLinearHeading(new Pose2d(-12, -18, Math.toRadians(180)))

*/
/**
                                .setReversed(true)
                                .splineToSplineHeading(new Pose2d(36,-20,Math.toRadians(-90)),Math.toRadians(90))
                                .splineToSplineHeading(new Pose2d(32.5,-9.5,Math.toRadians(-35)),  Math.toRadians(140))


                                .setReversed(false)

                                .splineToSplineHeading(new Pose2d(48,-16,Math.toRadians(0)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(61.5,-16,Math.toRadians(0)),Math.toRadians(0))

                                .setReversed(true)

                                .splineToSplineHeading(new Pose2d(45,-16,Math.toRadians(0)),Math.toRadians(180))
                                .splineToSplineHeading(new Pose2d(32.5,-9.5,Math.toRadians(-35)),  Math.toRadians(150))

                                .setReversed(false)

                                .splineToSplineHeading(new Pose2d(52,-16,Math.toRadians(0)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(60,-16,Math.toRadians(0)),Math.toRadians(0))

                                .setReversed(true)

                                .splineToSplineHeading(new Pose2d(21,-18,Math.toRadians(0)),Math.toRadians(180))
                                .splineToSplineHeading(new Pose2d(8.5,-23.5,Math.toRadians(35)),  Math.toRadians(-150))
                                .setReversed(false)

                                .splineToSplineHeading(new Pose2d(52,-16,Math.toRadians(0)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(60,-16,Math.toRadians(0)),Math.toRadians(0))
*/
                                //.lineToLinearHeading(new Pose2d(36, -16, Math.toRadians(180)))

                                //.lineToLinearHeading(new Pose2d(12, -18, Math.toRadians(180)))


/**
                                .setReversed(true)

                                .lineToLinearHeading(new Pose2d(-35,-53,Math.toRadians(180)))
                                .lineToLinearHeading(new Pose2d(-35,-47,Math.toRadians(180)))

                                .waitSeconds(coneScoreTime)

                                .setReversed(false)
                                .lineToLinearHeading(new Pose2d(-35,-12,Math.toRadians(180)))
                                .lineToLinearHeading(new Pose2d(-57,-12,Math.toRadians(180)))
                                .waitSeconds(intakeTime)


                                .setReversed(true)
                                .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(-30,-8,Math.toRadians(-145)),  Math.toRadians(30))
                                .waitSeconds(coneScoreTime)

                                .setReversed(false)
                                .splineToSplineHeading(new Pose2d(-48,-16,Math.toRadians(180)),Math.toRadians(180))
                                .splineToSplineHeading(new Pose2d(-57,-16,Math.toRadians(180)),Math.toRadians(180))
                                .waitSeconds(intakeTime)




                                //.setReversed(true)
                                //.splineToSplineHeading(new Pose2d(-35,-20,Math.toRadians(-90)),Math.toRadians(-180))
                                //.splineToSplineHeading(new Pose2d(-33,-9,Math.toRadians(-135)),  Math.toRadians(-240))

                                // To mid pole

                                .setReversed(true)
                                .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(-34,-16,Math.toRadians(145)),  Math.toRadians(-30))
                                .waitSeconds(coneScoreTime)

                                .setReversed(false)
                                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                                .splineToSplineHeading(new Pose2d(-60,-12,Math.toRadians(180)),Math.toRadians(180))
                                .waitSeconds(intakeTime)


                               // To team high pole
                                .setReversed(true)
                                .splineToSplineHeading(new Pose2d(-25,-12,Math.toRadians(180)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(-10,-16,Math.toRadians(145)),  Math.toRadians(-30))
                                .waitSeconds(coneScoreTime)

                                .setReversed(false)
                                .splineToSplineHeading(new Pose2d(-25,-12,Math.toRadians(180)),Math.toRadians(180))
                                .splineToSplineHeading(new Pose2d(-60,-12,Math.toRadians(180)),Math.toRadians(180))
                                .waitSeconds(intakeTime)


                                // To team low pole
                                .setReversed(true)
                                .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(-34,-8,Math.toRadians(-145)),  Math.toRadians(30))
                                .waitSeconds(coneScoreTime)

                                .setReversed(false)
                                .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                                .splineToSplineHeading(new Pose2d(-60,-12,Math.toRadians(180)),Math.toRadians(180))
                                .waitSeconds(intakeTime)

                                // To low pole
                                .setReversed(true)
                                .lineToLinearHeading(new Pose2d(-58,-16,Math.toRadians(145)))
                                .waitSeconds(coneScoreTime)

                                .setReversed(false)
                                .lineToLinearHeading(new Pose2d(-60,-12,Math.toRadians(180)))
                                .waitSeconds(intakeTime)


                                // TODO: Parking for 1
/**
                                 .setReversed(true)
                                 .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                                 .splineToSplineHeading(new Pose2d(-30,-8,Math.toRadians(-145)),  Math.toRadians(30))
                                 .waitSeconds(coneScoreTime)

                                 .setReversed(false)
                                 .splineToSplineHeading(new Pose2d(-52,-12,Math.toRadians(180)),Math.toRadians(180))
                                 .splineToSplineHeading(new Pose2d(-57,-12,Math.toRadians(180)),Math.toRadians(180))
*/
                                // TODO: Parking for 2
/**
                                 .setReversed(true)
                                 .splineToSplineHeading(new Pose2d(-45,-12,Math.toRadians(180)),Math.toRadians(0))
                                 .splineToSplineHeading(new Pose2d(-30,-8,Math.toRadians(-145)),  Math.toRadians(30))
                                 .waitSeconds(coneScoreTime)

                                .setReversed(false)
                                 .lineToLinearHeading(new Pose2d(-34, -12, Math.toRadians(180)))
*/
/**

                                // TODO: Parking for 3

                                .setReversed(true)
                                .splineToSplineHeading(new Pose2d(-25,-16,Math.toRadians(180)),Math.toRadians(0))
                                .splineToSplineHeading(new Pose2d(-10,-20,Math.toRadians(145)),  Math.toRadians(-30))
                                .waitSeconds(coneScoreTime)

                                .lineToLinearHeading(new Pose2d(-12, -14, Math.toRadians(180)))
                                */


                                .build()
                );

        meepMeep.setBackground(MeepMeep.Background.FIELD_POWERPLAY_OFFICIAL)
                .setDarkMode(true)
                .setBackgroundAlpha(0.95f)
                .addEntity(myBot)
                .start();
    }
}